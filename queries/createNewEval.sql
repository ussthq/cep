USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[createNewEval]    Script Date: 1/21/16 11:41:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[createNewEval]
	-- Add the parameters for the stored procedure here
	@newYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Eval_Years (year)
		VALUES (@newYear);

	INSERT INTO Ministry_Evaluation_Score_Text (year,text,forscore,javatype)
		VALUES(@newYear,'New Eval Score Text for score 1',1,'org.salvationarmy.cep.model.MinistryEvaluationScoreText');

	INSERT INTO Ministry_Evaluation_Score_Text (year,text,forscore,javatype)
		VALUES(@newYear,'New Eval Score Text for score 2',2,'org.salvationarmy.cep.model.MinistryEvaluationScoreText');

	INSERT INTO Ministry_Evaluation_Score_Text (year,text,forscore,javatype)
		VALUES(@newYear,'New Eval Score Text for score 3',3,'org.salvationarmy.cep.model.MinistryEvaluationScoreText');

	INSERT INTO Ministry_Evaluation_Score_Text (year,text,forscore,javatype)
		VALUES(@newYear,'New Eval Score Text for score 4',4,'org.salvationarmy.cep.model.MinistryEvaluationScoreText');

	INSERT INTO Ministry_Evaluation_Score_Text (year,text,forscore,javatype)
		VALUES(@newYear,'New Eval Score Text for score 5',5,'org.salvationarmy.cep.model.MinistryEvaluationScoreText');
END

