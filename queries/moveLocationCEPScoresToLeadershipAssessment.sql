USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[moveLocationCEPScoresToLeadershipAssessment]    Script Date: 1/26/16 11:15:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[moveLocationCEPScoresToLeadershipAssessment] 
	-- Add the parameters for the stored procedure here
	@revId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @reviewYear int,
		@locationId int,
		@emscid int,
		@subcat varchar(max),
		@memScore float,
		@standScore float,
		@partScore float,
		@growthScore float,
		@minScore float;

	SELECT
		@locationId = locationid FROM Reviews WHERE revid=@revId;
	SELECT
		@reviewYear = [year] FROM Reviews WHERE revid=@revId;

	DECLARE scoreCursor CURSOR FOR
		SELECT [membership score],[standard score],[participation score],[growth score],[ministry score],[sub category] FROM CEPScores WHERE locationid=@locationid;
		OPEN scoreCursor;
			FETCH NEXT FROM scoreCursor INTO @memScore,@standScore,@partScore,@growthScore,@minScore,@subcat;
			WHILE @@FETCH_STATUS=0
			BEGIN
				-- GET the EMSCID for this score
				SELECT 
					@emscid = emscid FROM EM_Sub_Categories AS emsc 
						JOIN Essential_Ministries AS em 
						ON emsc.emid=em.emid 
						WHERE em.year=@reviewYear AND emsc.subcategory=@subcat;
				PRINT 'Got an EmscId of ' + convert(varchar,@emscid);
				
				IF @emscid IS NOT NULL
					BEGIN
						DECLARE @laid int;

						-- Determine if scores already exist
						SELECT
							@laid = laid FROM Leadership_Assessment
								WHERE revid=@revid AND emscid=@emscid;

						IF @laid IS NOT NULL
							BEGIN
								UPDATE Leadership_Assessment
									SET MembershipScore=@memScore,StandardScore=@standScore,ParticipationScore=@partScore,GrowthScore=@growthScore
									WHERE laid=@laid;
								PRINT 'Updated record in Leadership_Assessment with laid=' + convert(varchar,@laid) + ' for sub category ' + @subcat;
							END
						ELSE
							BEGIN
								INSERT INTO Leadership_Assessment(emscid,revid,score,MembershipScore,StandardScore,ParticipationScore,GrowthScore,MinistryScore)
									VALUES(@emscid,@revid,0,@memScore,@standScore,@partScore,@growthScore,@minScore);
								PRINT 'Added record to Leadership_Assessment with id=' + convert(varchar,SCOPE_IDENTITY()) + ' for sub category ' + @subcat;
							END
						SET @laid = NULL;
					END
				IF @emscid IS NULL
					BEGIN
						PRINT 'emscid for ' + @subcat + ' not found, no record added';
					END

				PRINT '******';
				
				FETCH NEXT FROM scoreCursor INTO @memScore,@standScore,@partScore,@growthScore,@minScore,@subcat;
			END
		CLOSE scoreCursor;
	DEALLOCATE scoreCursor;
END
