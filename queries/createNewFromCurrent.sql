USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[createNewFromCurrent]    Script Date: 1/21/16 11:43:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[createNewFromCurrent]
	-- Add the parameters for the stored procedure here
	@year int,
	@newYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Eval_Years (year)
		VALUES (@newYear);

	INSERT INTO Ministry_Evaluation_Score_Text(year,text,forscore,javatype)
		SELECT @newYear,text,forscore,javatype
		FROM Ministry_Evaluation_Score_Text
		WHERE year=@year;

	INSERT INTO Intro_And_Links(year,intro,javatype)
		SELECT @newYear,intro,javatype
		FROM Intro_And_Links
		WHERE year=@year;

	DECLARE @emid int,
			@name varchar(50),
			@javatype varchar(max),
			@sequence int,
			@emYear int,
			@newEmid int,
			@emDefinition varchar(max);

	DECLARE emCursor CURSOR
		FOR SELECT * FROM Essential_Ministries WHERE year=@year;
		OPEN emCursor;
			FETCH NEXT FROM emCursor INTO @emid,@name,@emYear,@sequence,@javatype,@emDefinition;
			WHILE @@FETCH_STATUS=0
			BEGIN
				INSERT INTO Essential_Ministries(name,year,sequence,javatype,definition)
					VALUES(@name,@newYear,@sequence,@javatype,@emDefinition);

				SET @newEmid=SCOPE_IDENTITY();

				INSERT INTO EM_Sub_Categories(emid,subcategory,sequence,duration,javatype)
					SELECT @newEmid,subcategory,sequence,duration,javatype
					FROM EM_Sub_Categories
					WHERE emid=@emid;

				INSERT INTO Impact_Questions(emid,question,sequence,javatype)
					SELECT @newEmid,question,sequence,javatype
					FROM Impact_Questions
					WHERE emid=@emid;

				FETCH NEXT FROM emCursor INTO @emid,@name,@emYear,@sequence,@javatype,@emDefinition;
			END

		CLOSE emCursor;
	DEALLOCATE emCursor;

END