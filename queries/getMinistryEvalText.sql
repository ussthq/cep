USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[getMinistryEvalText]    Script Date: 1/21/16 11:45:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[getMinistryEvalText] 
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Ministry_Evaluation_Score_Text WHERE year=@year ORDER BY forscore ASC;
END

