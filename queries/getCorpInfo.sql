USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[getCorpInfo]    Script Date: 1/21/16 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[getCorpInfo] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LocationID,div_id,unit_id,unit_name,unit_officer,unit_type
	 FROM [USS_CEP2.0].[dbo].[sqlunits]
	 WHERE unit_type='CRP'
		OR unit_type='OTP'
		OR unit_type='OTH'
		OR unit_type='HLC'
	 ORDER BY unit_name ASC;
END


