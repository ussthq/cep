USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[deepDeleteEMModels]    Script Date: 1/21/16 11:43:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[deepDeleteEMModels]
	-- Add the parameters for the stored procedure here
	@emid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	DELETE FROM EM_Sub_Categories WHERE emid=@emid;
	DELETE FROM Impact_Questions WHERE emid=@emid;
	DELETE FROM Essential_Ministries WHERE emid=@emid;

END


