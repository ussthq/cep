USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[getEssentialMinistries]    Script Date: 1/21/16 11:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[getEssentialMinistries]
	@Year varchar(50)
AS
BEGIN
	SELECT * FROM [dbo].[Essential_Ministries] WHERE year=@Year ORDER BY [sequence] ASC;
	RETURN;
END


