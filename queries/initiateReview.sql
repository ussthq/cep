USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[initiateReview]    Script Date: 1/26/16 11:25:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[initiateReview]
	-- Add the parameters for the stored procedure here
	@year int,
	@divid int,
	@locationid int,
	@unitid int,
	@state varchar(50),
	@officer varchar(MAX),
	@corpName varchar(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @officer = '' OR @officer = 'null'
		BEGIN
			SET @officer = NULL;
		END
	IF @state = '' OR @state = 'null'
		BEGIN
			SET @state = NULL
		END

	-- Make sure we don't create duplicates
	DECLARE @count int;
	SELECT @count = COUNT(locationid)
		FROM Reviews
		WHERE locationid=@locationid
			AND year=@year;

	-- If there are no other records for this location and this year, create a new review
	IF @count = 0
		BEGIN
			INSERT INTO Reviews(year,divid,locationid,unitid,state,officer,corpname,javatype,locked,submitted,signed)
				VALUES(@year,@divid,@locationid,@unitid,@state,@officer,@corpName,'org.salvationarmy.cep.model.Review',0,0,0)
		
			SELECT * from Reviews WHERE revid=SCOPE_IDENTITY();
		END
END


