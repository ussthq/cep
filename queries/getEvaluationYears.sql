USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[getEvaluationYears]    Script Date: 1/21/16 11:45:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[getEvaluationYears]
AS
BEGIN
	SELECT DISTINCT [year] from dbo.Essential_Ministries ORDER BY [year] DESC;
END


