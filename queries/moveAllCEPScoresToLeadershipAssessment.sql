USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[moveAllCEPScoresToLeadershipAssessment]    Script Date: 1/26/16 6:02:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[moveAllCEPScoresToLeadershipAssessment] --for updates
	-- Add the parameters for the stored procedure here
	@reviewYear int
	-- [moveAllCEPScoresToLeadershipAssessment] 2016
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @revid int,
		@locationid int,
		@emscid int,
		@subcat varchar(max),
		@memScore float,
		@standScore float,
		@partScore float,
		@growthScore float,
		@minScore float;
		
	-- Outer loop
	DECLARE revCursor CURSOR FOR
		SELECT revid,locationid FROM Reviews WHERE year=@reviewYear
		OPEN revCursor;
			FETCH NEXT FROM revCursor INTO @revid,@locationid;
			WHILE @@FETCH_STATUS=0
			BEGIN

				-- Inner loop
				DECLARE scoreCursor CURSOR FOR
					SELECT [membership score],[standard score],[participation score],[growth score],[ministry score],[sub category] FROM CEPScores WHERE locationid=@locationid;
					OPEN scoreCursor;
						FETCH NEXT FROM scoreCursor INTO @memScore,@standScore,@partScore,@growthScore,@minScore,@subcat;
						WHILE @@FETCH_STATUS=0
						BEGIN

							--PRINT 'memScore=' + convert(varchar,@memScore);
							--PRINT 'standScore=' + convert(varchar,@standScore);
							--PRINT 'partScore=' + convert(varchar,@partScore);
							--PRINT 'growthScore=' + convert(varchar,@growthScore);
							--PRINT 'minScore=' + convert(varchar,@minScore);
							--PRINT 'subCat=' + @subcat;

							SELECT 
								@emscid = emscid FROM EM_Sub_Categories AS emsc 
									JOIN Essential_Ministries AS em 
									ON emsc.emid=em.emid 
									WHERE em.year=@reviewYear AND emsc.subcategory=@subcat


							--PRINT 'emscid=' + convert(varchar,@emscid);
							--PRINT 'revid=' + convert(varchar,@revid);
							--PRINT 'locationid=' + convert(varchar,@locationid);

							IF @emscid IS NOT NULL
								BEGIN
									declare @laid int;

									SELECT
										@laid = laid FROM Leadership_Assessment
											WHERE revid=@revid AND emscid=@emscid;

									IF @laid IS NOT NULL
										BEGIN
											UPDATE Leadership_Assessment
												SET MembershipScore=@memScore,StandardScore=@standScore,ParticipationScore=@partScore,GrowthScore=@growthScore
												WHERE laid=@laid;
												PRINT 'Updated record in Leadership_Assessment with id=' + convert(varchar,@laid) + ' for sub category ' + @subcat;
										END
									ELSE
										BEGIN
											INSERT INTO Leadership_Assessment(emscid,revid,score,MembershipScore,StandardScore,ParticipationScore,GrowthScore,MinistryScore)
												VALUES(@emscid,@revid,0,@memScore,@standScore,@partScore,@growthScore,@minScore);
											PRINT 'Added record to Leadership_Assessment with id=' + convert(varchar,SCOPE_IDENTITY() + ' for sub category ' + @subcat);
										END
									SET @laid = NULL;
								END

							IF @emscid IS NULL
								BEGIN
									PRINT 'emscid for ' + @subcat + ' not found, no record added';
								END

							PRINT '******';
							FETCH NEXT FROM scoreCursor INTO @memScore,@standScore,@partScore,@growthScore,@minScore,@subcat;
						END
					CLOSE scoreCursor;
				DEALLOCATE scoreCursor; -- Close inner loop

				FETCH NEXT FROM revCursor INTO @revid,@locationid;
			END

		CLOSE revCursor;
	DEALLOCATE revCursor; -- Close outer loop

END


--test update
--select * from Leadership_Assessment as la
--join EM_Sub_Categories as emsc
--on la.emscid=emsc.emscid
--where la.revid=1700