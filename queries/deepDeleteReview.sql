USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[deepDeleteReview]    Script Date: 1/26/16 10:14:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[deepDeleteReview] 
	@revid as int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Delete records in this order
	-- 1. Planning Actions
	-- 2. Planning Objectives
	-- 3. Planning Goals
	-- 4. Ministry Evaluation Scores
	-- 5. Impact Question Answers
	-- 6. Review Updaters

	-- Delete Planning Actions and Planning Objectives
	DECLARE objCursor CURSOR FOR
		SELECT po.objid FROM Planning_Goals as pg
			JOIN Planning_Objectives as po
			on pg.goalid=po.goalid
			where pg.revid=@revid
		OPEN objCursor
			DECLARE @objid int;

			FETCH NEXT FROM objCursor INTO @objid;
			WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE FROM Planning_Actions WHERE objid=@objid;
				PRINT 'Deleted ' + Convert(varchar,@@ROWCOUNT) + ' Planning_Action rows for objid ' + convert(varchar, @objid);
				DELETE FROM Planning_Objectives WHERE objid=@objid;
				PRINT 'Deleted ' + Convert(varchar,@@ROWCOUNT) + ' Planning_Objectives rows for objid ' + convert(varchar, @objid);
				FETCH NEXT FROM objCursor INTO @objid;
			END
		CLOSE objCursor
	DEALLOCATE objCursor

	-- Delete Planning Goals
	DELETE FROM Planning_Goals WHERE revid = @revid;
	PRINT 'Deleted ' + Convert(varchar,@@ROWCOUNT) + ' Planning_Goals rows for revid ' + convert(varchar, @revid);

	-- Delete Ministry Evaluation Scores
	DELETE FROM Ministry_Evaluation_Scores WHERE revid=@revid;
	PRINT 'Deleted ' + Convert(varchar,@@ROWCOUNT) + ' Ministry_Evaluation_Scores rows for revid ' + convert(varchar, @revid);

	-- Delete Impact Question Answers
	DELETE FROM Impact_Question_Answers WHERE revid=@revid;
	PRINT 'Deleted ' + Convert(varchar,@@ROWCOUNT) + ' Impact_Question_Answers rows for revid ' + convert(varchar, @revid);

	-- Delete Review Updaters
	DELETE FROM Review_Updaters WHERE revid=@revid;
	PRINT 'Deleted ' + Convert(varchar,@@ROWCOUNT) + ' Review_Updaters rows for revid ' + convert(varchar, @revid);

	-- Delete Leadership Assessment Scores
	DELETE FROM Leadership_Assessment WHERE revid=@revid;
	PRINT 'Deleted ' + Convert(varchar,@@ROWCOUNT) + ' Leadership_Assessment rows for revid ' + convert(varchar, @revid);

	-- Finally Delete the review
	DELETE FROM Reviews WHERE revid=@revid;
	PRINT 'Deleted ' + Convert(varchar,@@ROWCOUNT) + ' Reviews rows for revid ' + convert(varchar, @revid);

END
