USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[getEMSubCategories]    Script Date: 1/21/16 11:44:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[getEMSubCategories] 
	-- Add the parameters for the stored procedure here
	@emid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM EM_Sub_Categories WHERE emid=@emid ORDER BY [sequence] ASC;
END


