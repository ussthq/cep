USE [USS_CEP2.00_TEST]
GO
/****** Object:  StoredProcedure [dbo].[getEMImpactQuestions]    Script Date: 1/21/16 11:44:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[getEMImpactQuestions] 
	@emid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Impact_Questions WHERE emid=@emid ORDER BY [sequence] ASC;
END


