package org.salvationarmy.sql;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.salvationarmy.exception.NoConfigurationException;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;

public class Sql implements Serializable {

	private static final long serialVersionUID = 1L;
	private SQLServerDataSource dataSource;
	private Connection connection;
	private Configuration config;
	private Statement statement;
	
	public Sql() {
		
	}
	
	public Sql(Configuration config) {
		setConfig(config);
	}

	public SQLServerDataSource getDataSource() throws NoConfigurationException {
		Configuration config = getConfig();
		if (config == null) {
			throw new NoConfigurationException("No Configuration Object is defined");
		}else if (null == dataSource) {
			dataSource = new SQLServerDataSource();
			dataSource.setDatabaseName(config.getSqlDatabase());
			dataSource.setPortNumber(config.getSqlPortNumber());
			dataSource.setUser(config.getSqlUserName());
			dataSource.setPassword(config.getSqlPassword());
			dataSource.setServerName(config.getSqlHostName());
			dataSource.setEncrypt(false);
		}
		return dataSource;
	}

	public void setDataSource(SQLServerDataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Connection getConnection() {
		if (null == connection) {
			try {
				SQLServerDataSource dataSource = getDataSource();
				connection = dataSource.getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (NoConfigurationException e) {
				e.printStackTrace();
			}
		}
		return connection;
	}
	

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Configuration getConfig() {
		if (null == config) {
			config = new Configuration();
		}
		return config;
	}

	public void setConfig(Configuration config) {
		this.config = config;
	}
	
	public Statement getStatement() throws SQLException {
		if (null == statement && null != getConnection()) {
			statement = getConnection().createStatement();
		}
		return statement;
	}
	
	public void setStatement(Statement statement) {
		this.statement = statement;
	}
	
	public ResultSet doQuery(String query) throws SQLException {
		ResultSet result = null;
		if (getConnection() != null && query != null) {
			Statement stmt = null;
			stmt = getStatement();
			result = stmt.executeQuery(query);
		}
		return result;
	}
	
	public Integer doUpdate(String query) throws SQLException {
		//System.out.println("Sql.doUpdate, query=" + query);
		Integer rowsAffected = 0;
		if (getConnection() != null && query != null) {
			Statement stmt = null;
			stmt = getStatement();
			rowsAffected = stmt.executeUpdate(query);
		}
		return rowsAffected;
	}
	
	public String getJdbcVersion() {
		Driver driver = new SQLServerDriver();
		Integer majorVer = driver.getMajorVersion();
		Integer minorVer = driver.getMinorVersion();
		return majorVer.toString() + "." + minorVer.toString(); 
	}
	
}
