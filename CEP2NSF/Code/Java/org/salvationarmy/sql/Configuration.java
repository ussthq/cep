package org.salvationarmy.sql;

import java.util.HashMap;
import java.util.Map;

import org.salvationarmy.exception.NoConfigurationException;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.Name;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

import com.google.gson.Gson;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Configuration {

	private String notesServerName;
	private String notesHostName;
	private String corpDetailPath;
	private String sqlHostName;
	private String sqlDatabase;
	private String sqlUserName;
	private String sqlPassword;
	private int sqlPortNumber;
	private ServerType serverType;
	private Name dominoServerName;
	private Document configDoc;
	
	public enum ServerType {
		LOCAL, DEVELOPMENT, STAGING, PRODUCTION;
	}
	
	public Configuration() {
		init();
	}
	
	public void init() {
		Document configDoc = getConfigDoc();
		Map<String, String> fieldMap = getFieldMap();
		try {
			setNotesServerName(configDoc.getItemValueString(fieldMap.get("notesServerName")));
			setNotesHostName(configDoc.getItemValueString(fieldMap.get("notesHostName")));
			setCorpDetailPath(configDoc.getItemValueString(fieldMap.get("corpDetailPath")));
			setSqlHostName(configDoc.getItemValueString(fieldMap.get("sqlHostName")));
			setSqlPortNumber(configDoc.getItemValueInteger(fieldMap.get("sqlPortNumber")));
			setSqlDatabase(configDoc.getItemValueString(fieldMap.get("sqlDatabase")));
			setSqlUserName(configDoc.getItemValueString(fieldMap.get("sqlUserName")));
			setSqlPassword(configDoc.getItemValueString(fieldMap.get("sqlPassword")));
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	private Map<String, String> getFieldMap() {
		ServerType serverType = getServerType();
		Map<String, String> fieldMap = new HashMap<String, String>();
		switch (serverType) {
			case LOCAL:
				fieldMap.put("notesServerName", "LocalNotesServerName");
				fieldMap.put("notesHostName", "LocalNotesHostName");
				fieldMap.put("corpDetailPath", "LocalCorpDetailPath");
				fieldMap.put("sqlHostName", "LocalSqlServerHostName");
				fieldMap.put("sqlPortNumber", "LocalSqlServerPortNumber");
				fieldMap.put("sqlDatabase", "LocalSqlDatabase");
				fieldMap.put("sqlUserName", "LocalSqlUsername");
				fieldMap.put("sqlPassword", "LocalSqlPassword");
				break;
			case DEVELOPMENT:
				fieldMap.put("notesServerName", "DevNotesServerName");
				fieldMap.put("notesHostName", "DevNotesHostName");
				fieldMap.put("corpDetailPath", "DevCorpDetailPath");
				fieldMap.put("sqlHostName", "DevSqlServerHostName");
				fieldMap.put("sqlPortNumber", "DevSqlServerPortNumber");
				fieldMap.put("sqlDatabase", "DevSqlDatabase");
				fieldMap.put("sqlUserName", "DevSqlUsername");
				fieldMap.put("sqlPassword", "DevSqlPassword");
				break;
			case STAGING:
				fieldMap.put("notesServerName", "StagingNotesServerName");
				fieldMap.put("notesHostName", "StagingNotesHostName");
				fieldMap.put("corpDetailPath", "StagingCorpDetailPath");
				fieldMap.put("sqlHostName", "StagingSqlServerHostName");
				fieldMap.put("sqlPortNumber", "StagingSqlServerPortNumber");
				fieldMap.put("sqlDatabase", "StagingSqlDatabase");
				fieldMap.put("sqlUserName", "StagingSqlUsername");
				fieldMap.put("sqlPassword", "StagingSqlPassword");
				break;
			case PRODUCTION:
				fieldMap.put("notesServerName", "NotesServerName");
				fieldMap.put("notesHostName", "NotesHostName");
				fieldMap.put("corpDetailPath", "CorpDetailPath");
				fieldMap.put("sqlHostName", "SqlServerHostName");
				fieldMap.put("sqlPortNumber", "SqlServerPortNumber");
				fieldMap.put("sqlDatabase", "SqlDatabase");
				fieldMap.put("sqlUserName", "SqlUsername");
				fieldMap.put("sqlPassword", "SqlPassword");
				break;
		}
		return fieldMap;
	}

	public String getNotesServerName() {
		return notesServerName;
	}

	public void setNotesServerName(String notesServerName) {
		this.notesServerName = notesServerName;
	}

	public String getSqlDatabase() {
		return sqlDatabase;
	}

	public void setSqlDatabase(String sqlDatabase) {
		this.sqlDatabase = sqlDatabase;
	}

	public String getSqlUserName() {
		return sqlUserName;
	}

	public void setSqlUserName(String sqlUserName) {
		this.sqlUserName = sqlUserName;
	}

	public String getSqlPassword() {
		return sqlPassword;
	}

	public void setSqlPassword(String sqlPassword) {
		this.sqlPassword = sqlPassword;
	}

	public String getSqlHostName() {
		return sqlHostName;
	}
	
	public void setSqlHostName(String sqlHostName) {
		this.sqlHostName = sqlHostName;
	}

	public int getSqlPortNumber() {
		return sqlPortNumber;
	}

	public void setSqlPortNumber(int sqlPortNumber) {
		this.sqlPortNumber = sqlPortNumber;
	}

	public String getCorpDetailPath() {
		return corpDetailPath;
	}
	
	public void setCorpDetailPath(String corpDetailPath) {
		this.corpDetailPath = corpDetailPath;
	}
	
	public String getNotesHostName() {
		return notesHostName;
	}

	public void setNotesHostName(String notesHostName) {
		this.notesHostName = notesHostName;
	}
	
	public ServerType getServerType() {
		if (null == serverType) {
			Name serverName = getDominoServerName();
			Document configDoc = getConfigDoc();
			try {
				String abbrServerName = serverName.getAbbreviated().toLowerCase();
				if (abbrServerName.equals(configDoc.getItemValueString("LocalNotesServerName").toLowerCase())) {
					serverType = ServerType.LOCAL;
				}else if (abbrServerName.equals(configDoc.getItemValueString("DevNotesServerName").toLowerCase())) {
					serverType = ServerType.DEVELOPMENT;
				}else if (abbrServerName.equals(configDoc.getItemValueString("NotesServerName").toLowerCase())) {
					serverType = ServerType.PRODUCTION;
				}else if (abbrServerName.equals(configDoc.getItemValueString("StagingNotesServerName").toLowerCase())) {
					serverType = ServerType.STAGING;
				}
			} catch (NotesException e) {
				e.printStackTrace();
				serverType = null;
			}
		}
		return serverType;
	}

	public void setServerType(ServerType serverType) {
		this.serverType = serverType;
	}

	public Name getDominoServerName() {
		if (null == dominoServerName) {
			Database db = ExtLibUtil.getCurrentDatabase();
			Session sess = ExtLibUtil.getCurrentSession();
			try {
				dominoServerName = sess.createName(db.getServer());
			} catch (NotesException e) {
				e.printStackTrace();
				dominoServerName = null;
			}
		}
		return dominoServerName;
	}

	public void setDominoServerName(Name dominoServerName) {
		this.dominoServerName = dominoServerName;
	}

	public Document getConfigDoc() {
		if (null == configDoc) {
			Database db = ExtLibUtil.getCurrentDatabase();
			try {
				View vw = db.getView("Config");
				configDoc = vw.getFirstDocument();
				if (configDoc == null) {
					throw new NoConfigurationException("No Configuration Document Found!");
				}
			}catch (NotesException e) {
				e.printStackTrace();
			} catch (NoConfigurationException nce) {
				nce.printStackTrace();
			}
		}
		return configDoc;
	}

	public void setConfigDoc(Document configDoc) {
		this.configDoc = configDoc;
	}
	
	public String toString() {
		Map<String, Object> configMap = new HashMap<String, Object>();
		configMap.put("configDoc", getConfigDoc());
		configMap.put("notesServerName", getNotesServerName());
		configMap.put("notesHostName", getNotesHostName());
		configMap.put("serverType", getServerType());
		configMap.put("sqlHostName", getSqlHostName());
		configMap.put("sqlPortNumber", getSqlPortNumber());
		configMap.put("sqlDatabase", getSqlDatabase());
		configMap.put("sqlUserName", getSqlUserName());
		configMap.put("sqlPassword", getSqlPassword());
		return configMap.toString();
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
