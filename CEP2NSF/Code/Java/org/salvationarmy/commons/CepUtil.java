package org.salvationarmy.commons;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.Name;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

import org.salvationarmy.cep.CepFactory;
import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.gson.DoubleDeserializer;
import org.salvationarmy.cep.gson.ReviewStateDeserializer;
import org.salvationarmy.cep.model.EMSubCategory;
import org.salvationarmy.cep.model.Review;
import org.salvationarmy.cep.model.Review.ReviewState;
import org.salvationarmy.exception.NoGroupFoundException;
import org.salvationarmy.exception.NoLocationSIDException;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.exception.NoReviewFoundException;
import org.salvationarmy.exception.NoUserFoundException;
import org.salvationarmy.sql.Configuration;
import org.salvationarmy.sql.Sql;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class CepUtil {
	
	public static String getCorpOfficer(String unitSID) throws NotesException {
		String officer = null;
		Session sess = ExtLibUtil.getCurrentSession();
		String server = ExtLibUtil.getCurrentDatabase().getServer();
		Database domDir = sess.getDatabase(server, "names.nsf");
		DocumentCollection docColl = domDir.FTSearch("FIELD locationSID CONTAINS " + unitSID);
		if (docColl.getCount() > 0) {
			Vector<String> officers = new Vector<String>();
			for (int i = 0; i < docColl.getCount(); i++) {
				Document doc = docColl.getNthDocument(i);
				officers.add(doc.getItemValueString("Fullname"));
			}
			// Ack, what a hack
			officer = officers.toString().replace("'", "''");
			officer = officer.replace("[", "");
			officer = officer.replace("]", "");
		}
		return officer;
	}
	
	@SuppressWarnings("unchecked")
	public static Vector getCorpOfficers(String unitSID) throws NotesException {
		Vector<String> officers = null;
		Session sess = ExtLibUtil.getCurrentSession();
		String server = ExtLibUtil.getCurrentDatabase().getServer();
		Database domDir = sess.getDatabase(server, "names.nsf");
		DocumentCollection docColl = domDir.FTSearch("FIELD locationSID CONTAINS " + unitSID);
		if (docColl.getCount() > 0) {
			officers = new Vector<String>();
			for (int i = 0; i < docColl.getCount(); i++) {
				Document doc = docColl.getNthDocument(i);
				officers.add(doc.getItemValueString("Fullname"));
			}
		}
		return officers;
	}
	
	@SuppressWarnings("unchecked")
	public static Vector getCorpManager(String divInitial) throws NotesException, NoGroupFoundException {
		Vector<String> managersList = null;
		if (!divInitial.equals(null) && !divInitial.isEmpty()) {
			String groupName = "CEPManagers" + divInitial;
			managersList = getGroupMembers(groupName);
		}
		return managersList;
	}
	
	@SuppressWarnings("unchecked")
	public static Vector getGroupMembers(String groupName) throws NotesException, NoGroupFoundException {
		Vector<String> memberList = null;
		if (!groupName.equals(null) && !groupName.isEmpty()) {
			Session sess = ExtLibUtil.getCurrentSession();
			String server = ExtLibUtil.getCurrentDatabase().getServer();
			Database dir = sess.getDatabase(server, "names.nsf");
			View vw = dir.getView("$Users");
			Document group = vw.getDocumentByKey(groupName);
			if (group != null) {
				memberList = group.getItemValue("members");
			}else {
				throw new NoGroupFoundException("Unable to find group with name " + groupName);
			}
		}
		return memberList;
	}
	
	public static String getLocationSID(String abbreviatedName) throws NotesException, NoUserFoundException {
		String locationSID = null;
		// This is for testing purposes only
		// TODO: REMOVE THIS AFTER DEVELOPMENT
		// DHQ ALM Statistician
		// abbreviatedName = "Yolanda Kyles/ALM/USS/SArmy";
		// User Lake Charles, LA
		// abbreviatedName = "Donna Watts/OFC/USS/SArmy";
		// User
		// abbreviatedName = "Christopher Bryant/OFC/USS/SArmy";
		// User Rockingham County (838)
		// abbreviatedName = "Paul Fuller/OFC/USS/SArmy";
		
		if (abbreviatedName != null) {
			Database dir = null;
			Session sess = ExtLibUtil.getCurrentSession();
			String server = ExtLibUtil.getCurrentDatabase().getServer();
			dir = sess.getDatabase(server, "names.nsf");
			View vw = dir.getView("$Users");
			Document person = vw.getDocumentByKey(abbreviatedName);
			if (person != null) {
				locationSID = person.getItemValueString("locationSID");
			} else {
				throw new NoUserFoundException("Unable to find a person record for " + abbreviatedName);
			}
		}
		return locationSID;
	}
	
	public static Sql getSql() {
		/*Sql sql = (Sql) ExtLibUtil.getApplicationScope().get("SQL");
		if (sql == null) {
			sql = new Sql();
		}*/
		Sql sql = new Sql();
		return sql;
	}
	
	public static List<Map<String, Object>> getJsonMapList(String json) {
		List<Map<String, Object>> jsonList = new ArrayList<Map<String, Object>>();
		jsonList = new Gson().fromJson(json, new TypeToken<List<Map<String, Object>>>() {
		private static final long serialVersionUID = 1L;}.getType());
		return jsonList;
	}
	
	@SuppressWarnings("unchecked")
	public static ICepObject getCepObject(String json, String javaType) {
		//System.out.println("CepUtil.getCepObject, json=" + json);
		ICepObject cepObject = CepFactory.getCepObject(javaType);
		Class clazz = cepObject.getClass();
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("MMM dd, yyyy");
		if (cepObject instanceof Review) {
			gsonBuilder.registerTypeAdapter(Review.ReviewState.class, new ReviewStateDeserializer());
		}else if (cepObject instanceof EMSubCategory) {
			gsonBuilder.registerTypeAdapter(Double.class, new DoubleDeserializer());
		}
		Gson gson = gsonBuilder.create();
		cepObject = (ICepObject) gson.fromJson(json, clazz);
		return cepObject;
	}
	
	public static String getJsonString(BufferedReader reader) throws IOException {
		String line;
		StringBuilder sb = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}
	
	public static String getErrorJson(Exception e) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\"message\": ");
		sb.append("\"" + e.getMessage() + "\",");
		sb.append("\"stack\": ");
		sb.append("\"" + e.toString() + "\"}");
		return sb.toString();
	}
	
	public static void updateUpdaters(Integer reviewId) throws NotesException, SQLException {
		Session session = ExtLibUtil.getCurrentSession();
		String effName;
		effName = session.getEffectiveUserName();
		Name userName = session.createName(effName);
		String common = userName.getCommon();
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Review_Updaters AS target");
		sb.append(" USING (SELECT " + reviewId + ",'" + common + "') AS source(revid,updatername)");
		sb.append(" ON (target.revid=source.revid AND target.updatername=source.updatername)");
		sb.append(" WHEN NOT MATCHED THEN");
		sb.append(" INSERT (revid,updatername)");
		sb.append(" VALUES (source.revid,source.updatername);");
		String query = sb.toString();
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
	}
	
	public static void updateLastEdited(Integer reviewId) throws NotesException, SQLException {
		Session session = ExtLibUtil.getCurrentSession();
		String effName = session.getEffectiveUserName();
		Name userName = session.createName(effName);
		String abbr = userName.getAbbreviated().replaceAll("'", "''");
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE Reviews");
		sb.append(" SET lasteditedby='" + abbr + "'");
		sb.append(" WHERE revid=" + reviewId + ";");
		String query = sb.toString();
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
	}
	
	public static void updateStateAndStart(Review review) throws SQLException {
		Integer reviewId = review.getRevid();
		review.setState(ReviewState.OPENED);
		review.setStartdate(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String startDateStr = review.getStartdate() == null ? "NULL" : "'" + sdf.format(review.getStartdate()) + "'";
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Reviews AS target");
		sb.append(" USING (SELECT " + reviewId + ") as source(revid)");
		sb.append(" ON (target.revid=source.revid)");
		sb.append(" WHEN MATCHED AND target.revid=source.revid THEN");
		sb.append(" UPDATE SET state='" + review.getState().name() + "'");
		sb.append(",startdate=" + startDateStr + ";");
		String query = sb.toString();
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
	}
	
	public static String getCurrentUser() {
		Session session = ExtLibUtil.getCurrentSession();
		String effName;
		try {
			effName = session.getEffectiveUserName();
			Name userName = session.createName(effName);
			return userName.getAbbreviated();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static void sendSubmittedEmail(Integer reviewId, String division) throws SQLException, NoResultsException, NoReviewFoundException, NoGroupFoundException {
		Review rev = new Review();
		rev.setRevid(reviewId);
		rev.getSelf();
		Database db = ExtLibUtil.getCurrentDatabase();
		try {
			Configuration config = new Configuration();
			Document doc = db.createDocument();
			// System.out.println("CepUtil.SendSubmittedEmail, locationId=" + rev.getLocationid());
			Vector corpManager = getCorpManager(division);
			System.out.println("SendSubmittedEmail, corpManager=" + corpManager);
			doc.replaceItemValue("sendTo", corpManager);
			String subject = "Review for " + rev.getCorpname() + " submitted";
			Configuration.ServerType serverType = config.getServerType();
			if (serverType.equals(Configuration.ServerType.DEVELOPMENT) || serverType.equals(Configuration.ServerType.STAGING)) {
				subject = "***TEST - " + subject + " - TEST***";
			}
			doc.replaceItemValue("Subject", subject);
			StringBuilder sb = new StringBuilder();
			sb.append("Hello,\n");
			sb.append("The Corps Evaluation and Planning Review for ");
			sb.append(rev.getCorpname() + " has been submitted by ");
			sb.append(rev.getSubmittedby() + ".\n");
			sb.append("You may access this review by visiting this link: \n");
			sb.append("http://");
			sb.append(config.getNotesHostName() + "/");
			sb.append(db.getFilePath() + "/");
			sb.append("index.html#/eval/introandlinks/" + rev.getRevid() + "/0");
			doc.replaceItemValue("Body", sb.toString());
			doc.send();
			CepUtil.logAction("CepUtil.sendSubmittedEmail: Recipients = " + corpManager.toString());
		} catch (NotesException e) {
			CepUtil.logAction("CepUtil.sendSubmittedEmail ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	
	@SuppressWarnings("unchecked")
	public static void sendSignedEmail(Integer reviewId, String systemToken, String url) throws SQLException, NoResultsException, NotesException, NoReviewFoundException, IOException {
		Review rev = new Review();
		rev.setRevid(reviewId);
		rev.getSelf();
		Database db = ExtLibUtil.getCurrentDatabase();
		try {
			Configuration config = new Configuration();
			String unitSID = CepUtil.getUnitSIDByLocationId(rev.getLocationid(), systemToken, url);
			Vector<String> corpOfficers = getCorpOfficers(unitSID);
			Document doc = db.createDocument();
			doc.replaceItemValue("sendTo", corpOfficers);
			String subject = "Review for " + rev.getCorpname() + " Signed & Completed";
			Configuration.ServerType serverType = config.getServerType();
			if (serverType.equals(Configuration.ServerType.DEVELOPMENT) || 
					serverType.equals(Configuration.ServerType.STAGING) ||
					serverType.equals(Configuration.ServerType.LOCAL)) {
				subject = "***TEST - " + subject + " - TEST***";
			}
			doc.replaceItemValue("Subject", subject);
			StringBuilder sb = new StringBuilder();
			sb.append("Hello,\n");
			sb.append("The Corps Evaluation and Planning Review for ");
			sb.append(rev.getCorpname() + " has been signed and completed by ");
			sb.append(rev.getSignedby() + ".\n");
			sb.append("You may access this review by visiting this link: \n");
			sb.append("http://");
			sb.append(config.getNotesHostName() + "/");
			sb.append(db.getFilePath() + "/");
			sb.append("index.html#/eval");
			doc.replaceItemValue("Body", sb.toString());
			doc.send();
			CepUtil.logAction("CepUtil.sendSignedEmail: Recipients = " + corpOfficers.toString());
		} catch (NotesException e) {
			CepUtil.logAction("CepUtil.sendSignedEmail ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Get the Unit from unit portal using the locationSID
	 * @param systemToken - The authentication token
	 * @param url - URL to the unit portal api
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws NotesException
	 * @throws NoLocationSIDException
	 * @throws NoUserFoundException
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getUnitPortalResponse(String systemToken, String url) throws MalformedURLException, IOException, NotesException, NoLocationSIDException, NoUserFoundException {
		Map<String, Object> unitObj = null;
		String currentUser = CepUtil.getCurrentUser();
		String locationSID = CepUtil.getLocationSID(currentUser);
		// System.out.println("CepUtil.getUnitPortalResponse, locationSID=" + locationSID);
		// System.out.println("CepUtil.getUnitPortalResponse, currentUser=" + currentUser);
		if (locationSID != null && !locationSID.isEmpty() && systemToken != null && url != null) {
			URL uri = new URL(url + "/api/unit/externalsystem/Active%20Directory/ID/" + locationSID + "?includeDivision=true");
			URLConnection conn = uri.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestProperty("System-Token", systemToken);
			conn.setRequestProperty("Accept-Encoding", "gzip");
			conn.setRequestProperty("accept", "application/json; charset=utf-8");
			InputStream response = conn.getInputStream();
			Gson gson = new Gson();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response)));
			unitObj = gson.fromJson(reader, Map.class);
			
			// Get the Extended Data which includes zip code
			uri = new URL(url + "/api/unit/extended-data/" + unitObj.get("UnitId"));
			conn = uri.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestProperty("System-Token", systemToken);
			conn.setRequestProperty("Accept-Encoding", "gzip");
			conn.setRequestProperty("accept", "application/json; charset=utf-8");
			response = conn.getInputStream();
			Gson gson1 = new Gson();
			BufferedReader reader1 = new BufferedReader(new InputStreamReader(new GZIPInputStream(response)));
			Map<String, Object> extdMap = gson1.fromJson(reader1, Map.class);
			unitObj.put("Data", extdMap.get("Data"));
		}else {
			CepUtil.logAction("CepUtil.getUnitPortalResponse ERROR: No LocationSID\nlocationSID=" + locationSID + ", systemToken=" + systemToken + ", url=" + url);
		}
		// System.out.println("getUnitPortalResponse, unitObj=" + unitObj.toString());
		return unitObj;
	}
	
	/**
	 * Lookup the locationSID from Unit Portal based on a location Id
	 * @param locationId
	 * @param systemToken
	 * @param url
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static String getUnitSIDByLocationId(Integer locationId, String systemToken, String url) throws IOException {
		String unitSID = null;
		if (locationId != null && systemToken != null && url != null) {
			URL uri = new URL(url + "/api/unit/externalsystem/Corps%20Evaluation/ID/" + locationId + "?includeDivision=true");
			URLConnection conn = uri.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestProperty("System-Token", systemToken);
			conn.setRequestProperty("Accept-Encoding", "gzip");
			conn.setRequestProperty("accept", "application/json; charset=utf-8");
			InputStream response = conn.getInputStream();
			Gson gson = new Gson();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response)));
			Map<String, Object> unitObj = gson.fromJson(reader, Map.class);
			List<Map<String, Object>> crossRefs = (List<Map<String, Object>>) unitObj.get("CrossReferences");
			for (int i = 0; i < crossRefs.size(); i++) {
				Map<String, Object> crossRef = crossRefs.get(i);
				Double externalSystemId = (Double) crossRef.get("ExternalSystemId");
				if (externalSystemId == 1019.0) {
					unitSID = (String) crossRef.get("ExternalId");
				}
			}
		}else {
			CepUtil.logAction("CepUtil.getUnitSIDByLocationID ERROR: No locationId\nlocationId=" + locationId);
		}
		return unitSID;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getUnitPortalDivResponse(String systemToken, String url, String divId) throws IOException {
		Map<String, Object> unitObj = null;
		if (divId != null && systemToken != null && url != null) {
			URL uri = new URL(url + "/api/unit/extended-data/" + divId);
			URLConnection conn = uri.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestProperty("System-Token", systemToken);
			conn.setRequestProperty("Accept-Encoding", "gzip");
			conn.setRequestProperty("accept", "application/json; charset=utf-8");
			InputStream response = conn.getInputStream();
			Gson gson = new Gson();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(response)));
			//ArrayList<Map<String, Object>> unitObj = gson.fromJson(reader, ArrayList.class);
			unitObj = gson.fromJson(reader, Map.class);
		}else {
			CepUtil.logAction("CepUtil.getUnitPortalDivResponse ERROR: No DivId\ndivId=" + divId);
		}
		return unitObj;
	}
	
	public static boolean isYearReadyForReviews(Integer year) throws SQLException {
		Boolean yearReady = false;
		String query = "SELECT * from Essential_Ministries WHERE [year]=" + year;
		Sql sql = CepUtil.getSql();
		ResultSet resultSet = sql.doQuery(query);
		if (resultSet != null) {
			yearReady = resultSet.next();
		}
		return yearReady;
	}
	
	public static void logAction(String task) {
		Session sess = ExtLibUtil.getCurrentSession();
		Database db = ExtLibUtil.getCurrentDatabase();
		try {
			Document doc = db.createDocument();
			doc.replaceItemValue("form", "RequestLog");
			doc.replaceItemValue("logDate", sess.createDateTime(new Date()));
			doc.replaceItemValue("user", CepUtil.getCurrentUser());
			doc.replaceItemValue("task", task);
			doc.save(true);
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param locationId
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	@SuppressWarnings("unchecked")
	public static String getCorpOfficer(Integer locationId) throws NotesException {
		String officer = null;
		Database db = getCorpDetail();
		View vw = db.getView("CEPbyLocationId");
		Document doc = vw.getDocumentByKey(locationId);
		if (doc != null) {
			Vector officers = doc.getItemValue("CEPViewers");
			// Ack, what a hack
			officer = officers.toString().replace("'", "''");
			officer = officer.replace("[", "");
			officer = officer.replace("]", "");
		}
		return officer;
	}

	/**
	 * 
	 * @param abbreviatedName
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	public static Integer getCorpLocationId(String abbreviatedName) throws NotesException {
		Integer locationId = null;
		Database db = getCorpDetail();
		View vw;
		vw = db.getView("CEPByPerson");
		DocumentCollection dc = vw.getAllDocumentsByKey(abbreviatedName);
		if (dc.getCount() > 0) {
			Document doc = dc.getFirstDocument();
			locationId = doc.getItemValueInteger("LocationID");
		}
		return locationId;
	}
	
	/**
	 * 
	 * @param abbreviatedName
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	public static Integer getCorpDivId(String abbreviatedName) throws NotesException {
		Integer divId = null;
		Database db = getCorpDetail();
		View vw;
		vw = db.getView("CEPByPerson");
		DocumentCollection dc = vw.getAllDocumentsByKey(abbreviatedName);
		if (dc.getCount() > 0) {
			Document doc = dc.getFirstDocument();
			/*String divIdStr = doc.getItemValueString("DIVID");
			divId = Integer.valueOf(divIdStr);*/
			divId = doc.getItemValueInteger("DIVID");
		}
		return divId;
	}
	
	/**
	 * 
	 * @param abbreviatedName
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	public static String getDivision(String abbreviatedName) throws NotesException {
		String division = null;
		Database db = getCorpDetail();
		View vw;
		vw = db.getView("CEPByPerson");
		DocumentCollection dc = vw.getAllDocumentsByKey(abbreviatedName);
		if (dc.getCount() > 0) {
			Document doc = dc.getFirstDocument();
			division = doc.getItemValueString("Division");
		}
		return division;
	}
	
	/**
	 * 
	 * @param abbreviatedName
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	public static String getCorpName(String abbreviatedName) throws NotesException {
		String corpName = null;
		Database db = getCorpDetail();
		View vw;
		vw = db.getView("CEPByPerson");
		DocumentCollection dc = vw.getAllDocumentsByKey(abbreviatedName);
		if (dc.getCount() > 0) {
			Document doc = dc.getFirstDocument();
			corpName = doc.getItemValueString("$corpval");
		}
		return corpName;
	}
	
	/**
	 * 
	 * @param locationId
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	public static String getCorpZip(Integer locationId) throws NotesException {
		String zipCode = null;
		if (locationId != null) {
			Database db = getCorpDetail();
			View vw = db.getView("CEPbyLocationId");
			Document doc = vw.getDocumentByKey(locationId);
			if (doc != null) {
				zipCode = doc.getItemValueString("Zipcode");
			}
		}
		return zipCode;
	}
	
	/**
	 * 
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	public static Database getCorpDetail() throws NotesException {
		Configuration config = new Configuration();
		Database db = null;
		Session sess = ExtLibUtil.getCurrentSession();
		String server = null;
		server = ExtLibUtil.getCurrentDatabase().getServer();
		db = sess.getDatabase(server, config.getCorpDetailPath());
		return db;
	}
	
	/**
	 * 
	 * @param locationId
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	@SuppressWarnings("unchecked")
	public static Vector getCorpOfficers(Integer locationId) throws NotesException {
		Vector<String> officers = null;
		Database db = getCorpDetail();
		View vw = db.getView("CEPbyLocationId");
		Document doc = vw.getDocumentByKey(locationId);
		if (doc != null) {
			officers = doc.getItemValue("CEPViewers");
		}
		return officers;
	}
	
	/**
	 * @param locationId
	 * @return
	 * @throws NotesException
	 * @deprecated
	 */
	@SuppressWarnings("unchecked")
	public static Vector getCorpManager(Integer locationId) throws NotesException {
		Vector<String> dhqOfficers = null;
		Database db = getCorpDetail();
		View vw = db.getView("CEPbyLocationId");
		Document doc = vw.getDocumentByKey(locationId);
		if (doc != null) {
			dhqOfficers = doc.getItemValue("CEPManager");
		}
		return dhqOfficers;
	}
}
