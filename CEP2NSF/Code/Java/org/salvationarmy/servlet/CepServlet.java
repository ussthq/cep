package org.salvationarmy.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lotus.domino.NotesException;

import org.salvationarmy.cep.CepFactory;
import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.sql.Sql;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.ibm.xsp.extlib.util.ExtLibUtil;

import frostillicus.xsp.servlet.AbstractXSPServlet;

public class CepServlet extends AbstractXSPServlet {

	private HttpServletRequest request;
	
	public enum RequestType {
		GET("get"), PUT("put"), POST("post"), DELETE("delete"), PATCH("patch");
		
		private String methodName;
		
		RequestType(String methodName) {
			this.methodName = methodName;
		}
		
		public String getMethodName() {
			return methodName;
		}
		
		public void setMethodName(String methodName) {
			this.methodName = methodName;
		}
	}
	
	@Override
	protected void doService(HttpServletRequest req, HttpServletResponse res,
			FacesContext facesContext, ServletOutputStream out)
			throws Exception {
		this.request = req;
		RequestType type = RequestType.valueOf(req.getMethod().toUpperCase());
		String jsonResult = "{}";
		String javaTypeStr = null;
		JavaType javaType = null;
		String id = null;
		switch (type) {
			case GET:
				javaTypeStr = req.getParameter("javatype");
				javaType = getJavaType(javaTypeStr);
				id = req.getParameter("id");
				try {
					jsonResult = getResult(javaType, id);
				}catch (Exception e) {
					res.setStatus(500);
					jsonResult = CepUtil.getErrorJson(e);
					e.printStackTrace();
					CepUtil.logAction("CepServlet.doService for GET ERROR: " + e.getMessage());
				}
				break;
			case PUT:
				try {
					jsonResult = putResult(req.getReader());
				}catch (Exception e) {
					res.setStatus(500);
					jsonResult = CepUtil.getErrorJson(e);
					e.printStackTrace();
					CepUtil.logAction("CepServlet.doService for PUT ERROR: " + e.getMessage());
				}
				break;
			case POST:
				try {
					if (req.getParameter("initiate") != null && req.getParameter("year") != null) {
						Integer year = Integer.valueOf(req.getParameter("year"));
						jsonResult = initiateReviews(req.getReader(), year);
					}else{
						jsonResult = postResult(req.getReader());
					}
				}catch (Exception e) {
					res.setStatus(500);
					jsonResult = CepUtil.getErrorJson(e);
					e.printStackTrace();
					CepUtil.logAction("CepServlet.doService for POST ERROR: " + e.getMessage());
				}
				break;
			case DELETE:
				javaTypeStr = req.getParameter("javatype");
				javaType = getJavaType(javaTypeStr);
				id = req.getParameter("id");
				try {
					jsonResult = deleteResult(javaType, id);
				}catch (Exception e) {
					if (e.getMessage().equalsIgnoreCase("The statement did not return a result set.")) {
						res.setStatus(204);
					}else{
						res.setStatus(500);
						e.printStackTrace();
						CepUtil.logAction("CepServlet.doService for DELETE ERROR: " + e.getMessage());
					}
					jsonResult = CepUtil.getErrorJson(e);
				}
				break;
			case PATCH:
				try {
					jsonResult = putResult(req.getReader());
				}catch (Exception e) {
					res.setStatus(500);
					jsonResult = CepUtil.getErrorJson(e);
					e.printStackTrace();
					CepUtil.logAction("CepServlet.doService for PATCH ERROR: " + e.getMessage());
				}
				break;
		}
		out.print(jsonResult);
	}

	private String getResult(JavaType javaType, String id) throws SQLException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		String json = null;
		Sql sql = getSql();
		String tableName = getTableName(javaType);
		String idColumnName = getIdColumnName(javaType);
		String query = "SELECT * FROM " + tableName + " WHERE " + idColumnName + "=" + id;
		ResultSet result = sql.doQuery(query);
		if (result != null) {
			ResultSetMetaData md = result.getMetaData();
			int colCount = md == null ? 0 : md.getColumnCount();
			while (result.next()) {
				ICepObject cepObject = CepFactory.getCepObject(javaType);
				for (int i = 1; i <= colCount; i++) {
					String label = md.getColumnLabel(i);
					if (!label.equalsIgnoreCase("javatype")) {
						String setMethod = CepFactory.getICepMethodName("set", label);
						Method method = CepFactory.getICepMethod(cepObject.getClass(), setMethod);
						if (method != null) {
							Object value = result.getObject(i);
							method.invoke(cepObject, value);
						}
					}
				}
				json = cepObject.toJson();
				//System.out.println("CepServlet.getResult, json=" + json);
				break;
			}
		}
		return json;
	}
	
	/**
	 * Do an update query
	 * @param reader
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	private String putResult(BufferedReader reader) throws SQLException, IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		String json = "{}";
		String line;
		StringBuilder sb = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		Map<String, Object> jsonMap = getJsonMap(sb.toString());
		Sql sql = getSql();
		JavaType javaType = getJavaType(String.valueOf(jsonMap.get("javatype")));
		String idColumnName = getIdColumnName(javaType);
		Integer id = null;
		try {
			id = ((Double) jsonMap.get(idColumnName)).intValue();
		}catch (ClassCastException e) {
			id = Double.valueOf(String.valueOf(jsonMap.get(idColumnName))).intValue();
		}
		sb = new StringBuilder();
		sb.append("UPDATE " + getTableName(javaType));
		sb.append(" SET ");
		int count = 0;
		// TODO: More stupidity, fix it.
		List<String> ignoreProps = new ArrayList<String>();
		ignoreProps.add("score");
		ignoreProps.add("MembershipScore");
		ignoreProps.add("StandardScore");
		ignoreProps.add("ParticipationScore");
		ignoreProps.add("GrowthScore");
		ignoreProps.add("MinistryScore");
		for (Entry<String, Object> ent : jsonMap.entrySet()) {
			String key = ent.getKey();
			//System.out.println("working on key " + key);
			if(!ignoreProps.contains(key)) {
				if (!idColumnName.equalsIgnoreCase(key)) { //Can't update the id
					Object value = ent.getValue();
					if (count > 0) {
						sb.append(",");
					}
					if (value instanceof Double) {
						int intValue = ((Double) value).intValue();
						sb.append(key + "=" + intValue);
					}else if (value instanceof String) {
						String strValue = String.valueOf(ent.getValue());
						strValue = strValue.replaceAll("'", "''");
						strValue = strValue.trim();
						sb.append(key + "='" + strValue + "'");
					}else if (value instanceof Boolean) {
						Boolean boolValue = (Boolean) ent.getValue();
						if (boolValue) {
							sb.append(key + "=" + 1);
						}else{
							sb.append(key + "=" + 0);
						}
					}else if (value == null) {
						sb.append(key + "=NULL");
					}
					count++; //count inside the if so if the first column is the id we don't get an erroneous comma
				}
			}
		}
		sb.append(" WHERE " + idColumnName + "=" + id);
		String query = sb.toString();
		CepUtil.logAction("CepServlet.putResult: " + query);
		Integer result = 0;
		//System.out.println("CepServlet.putResult, query=" + query);
		result = sql.doUpdate(query);
		if (result > 0) {
			json = getResult(javaType, String.valueOf(id)); // Return the updated record
		}
		//System.out.println("CepServlet.putResult, json=" + json);
		return json;
	}
	
	/**
	 * Do a insert query
	 * @param javaType
	 * @param id
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 * @throws IllegalArgumentException 
	 * @throws IOException 
	 */
	public String postResult(BufferedReader reader) throws IllegalArgumentException, SQLException, IllegalAccessException, InvocationTargetException, IOException {
		//System.out.println("CepServlet.postResult, reader=" + reader);
		String json = "{}";
		String line;
		StringBuilder sb = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		Map<String, Object> jsonMap = getJsonMap(sb.toString());
		Sql sql = getSql();
		JavaType javaType = getJavaType(String.valueOf(jsonMap.get("javatype")));
		String idColumnName = getIdColumnName(javaType);
		String tableName = getTableName(javaType);
		sb = new StringBuilder();
		sb.append("INSERT INTO " + tableName);
		sb.append("(");
		int count = 0;
		for (Entry<String, Object> ent : jsonMap.entrySet()) {
			if (!idColumnName.equalsIgnoreCase(ent.getKey())) { //Can't update the id
				if (count > 0) {
					sb.append(",");
				}
				sb.append(ent.getKey());
				count++; //count inside the if so if the first column is the id we don't get an erroneous comma
			}
		}
		sb.append(")");
		sb.append(" VALUES (");
		count = 0;
		for (Entry<String, Object> ent : jsonMap.entrySet()) {
			if (!idColumnName.equalsIgnoreCase(ent.getKey())) {
				if (count > 0) {
					sb.append(",");
				}
				Object value = ent.getValue();
				if (value instanceof Double) {
					int intValue = ((Double) value).intValue();
					sb.append(intValue);
				}else if (value instanceof String) {
					String strValue = String.valueOf(ent.getValue());
					strValue = strValue.replaceAll("'", "''");
					sb.append("'" + strValue + "'");
				}
				count++;
			}
		}
		sb.append(")");
		String selectQuery = " SELECT * FROM " + tableName + " WHERE " + idColumnName + "=SCOPE_IDENTITY();";
		sb.append(selectQuery);
		String query = sb.toString();
		//System.out.println("CepServlet.postResult, query=" + query);
		CepUtil.logAction("CepServlet.postResult: " + query);
		try {
			ResultSet result = sql.doQuery(query);
			ResultSetMetaData md = result.getMetaData();
			int colCount = md == null ? 0 : md.getColumnCount();
			while (result.next()) {
				ICepObject cepObject = CepFactory.getCepObject(javaType);
				for (int i = 1; i <= colCount; i++) {
					String label = md.getColumnLabel(i);
					if (!label.equalsIgnoreCase("javatype")) {
						String setMethod = CepFactory.getICepMethodName("set", label);
						Method method = CepFactory.getICepMethod(cepObject.getClass(), setMethod);
						if (method != null) {
							Object value = result.getObject(i);
							method.invoke(cepObject, value);
						}
					}
				}
				json = cepObject.toJson();
				//System.out.println("CepServlet.putResult, json=" + json);
				break;
			}
		}catch (SQLException e) {
			if (e.getMessage().contains("not return a result set")) {
				ResultSet result = sql.doQuery(selectQuery);
				ResultSetMetaData md = result.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				while (result.next()) {
					ICepObject cepObject = CepFactory.getCepObject(javaType);
					for (int i = 1; i <= colCount; i++) {
						String label = md.getColumnLabel(i);
						if (!label.equalsIgnoreCase("javatype")) {
							String setMethod = CepFactory.getICepMethodName("set", label);
							Method method = CepFactory.getICepMethod(cepObject.getClass(), setMethod);
							if (method != null) {
								Object value = result.getObject(i);
								method.invoke(cepObject, value);
							}
						}
					}
					json = cepObject.toJson();
					//System.out.println("CepServlet.putResult, json=" + json);
					break;
				}
			}
		}
		//System.out.println("CepServlet.putResult, json=" + json);
		return json;
	}
	
	/**
	 * Do a delete query
	 * @param javaType
	 * @param id
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 * @throws IllegalArgumentException 
	 */
	public String deleteResult(JavaType javaType, String id) throws IllegalArgumentException, SQLException, IllegalAccessException, InvocationTargetException {
		//System.out.println("CepServlet.deleteResult, javaType=" + javaType + ", id=" + id);
		String json = "{}";
		Sql sql = getSql();
		String tableName = getTableName(javaType);
		String idColumnName = getIdColumnName(javaType);
		StringBuilder sb = new StringBuilder();
		if (javaType.equals(JavaType.EssentialMinistry)) {
			sb.append("SELECT * FROM " + tableName + " WHERE " + idColumnName + "=" + id + ";");
			sb.append("EXEC deepDeleteEMModels " + id + ";");
		}else if (javaType.equals(JavaType.Review)) {
			sb.append("SELECT * FROM " + tableName + " WHERE " + idColumnName + "=" + id + ";");
			sb.append("EXEC deepDeleteReview " + id + ";");
		}else {
			// This is questionable if it'll work after reviews are initialized
			// Should only be called when deleting EMSubCategories and ImpactQuestions
			sb.append("SELECT * FROM " + tableName + " WHERE " + idColumnName + "=" + id);
			sb.append(" DELETE FROM " + tableName + " WHERE " + idColumnName + "=" + id);
		}
		String query = sb.toString();
		CepUtil.logAction("CepServlet.deleteResult: " + query);
		//System.out.println("CepServlet.deleteResult, query=" + query);
		ResultSet result = sql.doQuery(query);
		ResultSetMetaData md = result.getMetaData();
		int colCount = md == null ? 0 : md.getColumnCount();
		while (result.next()) {
			ICepObject cepObject = CepFactory.getCepObject(javaType);
			for (int i = 1; i <= colCount; i++) {
				String label = md.getColumnLabel(i);
				if (!label.equalsIgnoreCase("javatype")) {
					String setMethod = CepFactory.getICepMethodName("set", label);
					Method method = CepFactory.getICepMethod(cepObject.getClass(), setMethod);
					if (method != null) {
						Object value = result.getObject(i);
						method.invoke(cepObject, value);
					}
				}
			}
			json = cepObject.toJson();
			break;
		}
		//System.out.println("CepServlet.deleteResult, json=" + json);
		return json;
	}
	
	private String getTableName(JavaType javaType) {
		String tableName = null;
		switch(javaType) {
			case EssentialMinistry:
				tableName = "Essential_Ministries";
				break;
			case EMSubCategory:
				tableName = "EM_Sub_Categories";
				break;
			case ImpactQuestion:
				tableName = "Impact_Questions";
				break;
			case MinistryEvaluationScoreText:
				tableName = "Ministry_Evaluation_Score_Text";
				break;
			case IntroAndLinks:
				tableName = "Intro_And_Links";
				break;
			case PlanningIntro:
				tableName = "Planning_Intro";
				break;
			case Review:
				tableName = "Reviews";
				break;
		}
		return tableName;
	}
	
	private String getIdColumnName(JavaType javaType) {
		String idColumnName = null;
		switch(javaType) {
			case EssentialMinistry:
				idColumnName = "emid";
				break;
			case EMSubCategory:
				idColumnName = "emscid";
				break;
			case ImpactQuestion:
				idColumnName = "iqid";
				break;
			case MinistryEvaluationScoreText:
				idColumnName = "metid";
				break;
			case IntroAndLinks:
				idColumnName = "year";
				break;
			case PlanningIntro:
				idColumnName = "year";
				break;
			case Review:
				idColumnName = "revid";
				break;
		}
		return idColumnName;
	}
	
	private Sql getSql() {
		Sql sql = (Sql) ExtLibUtil.getApplicationScope().get("Sql");
		if (null == sql) {
			sql = new Sql();
		}
		return sql;
	}
	
	private JavaType getJavaType(String javaTypeStr) {
		JavaType javaType = JavaType.valueOf(javaTypeStr.substring(javaTypeStr.lastIndexOf(".") +1));
		return javaType;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> getJsonMap(String json) {
		Gson gson = new Gson();
		Map<String, Object> jsonMap = gson.fromJson(json, LinkedTreeMap.class);
		return jsonMap;
	}
	
	@SuppressWarnings("unused")
	private List<Map<String, Object>> getJsonMapList(String json) {
		List<Map<String, Object>> jsonList = new ArrayList<Map<String, Object>>();
		jsonList = new Gson().fromJson(json, new TypeToken<List<Map<String, Object>>>() {
		private static final long serialVersionUID = 1L;}.getType());
		return jsonList;
	}
	
	public String initiateReviews(BufferedReader reader, Integer year) throws IOException, SQLException, NotesException {
		String json = "{}";
		String unitPortalId = this.request.getParameter("unitportalid");
		String unitPortalUrl = this.request.getParameter("unitportalurl");
		String line;
		boolean isYearReady = CepUtil.isYearReadyForReviews(year);
		if (isYearReady == true) {
			StringBuilder sb = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			String jsonList = sb.toString();
			List<Map<String, Object>> reviewList = getJsonMapList(jsonList);
			int count = 0;
			for (Map<String, Object> review : reviewList) {
				Integer locationId = Double.valueOf(String.valueOf(review.get("LocationID"))).intValue();
				String unitSID = null;
				String officer = null;
				try {
					unitSID = CepUtil.getUnitSIDByLocationId(locationId, unitPortalId, unitPortalUrl);
					//System.out.println("CepServlet.initiateReviews, unitSID=" + unitSID);
					officer = CepUtil.getCorpOfficer(unitSID);
					//System.out.println("CepServlet.initiateReviews, officer=" + officer);
				}catch(Exception e) {
					System.out.println(e);
				}
				Integer divId = Double.valueOf(String.valueOf(review.get("div_id"))).intValue();
				Integer unitId = Double.valueOf(String.valueOf(review.get("unit_id"))).intValue();
				String corpName = String.valueOf(review.get("unit_name"));
				String query = "initiateReview " + year + "," + divId + "," + locationId + "," + unitId + ",'INITIALIZED','" + officer + "','" + corpName + "'";
				// System.out.println("CepServlet.initiateReviews, query=" + query);
				CepUtil.logAction("CepServlet.initiateReviews: " + query);
				Statement stmt = getSql().getStatement();
				stmt.execute(query);
				count++;
			}
			json = "{\"status\": \"Initiated " + count + " Reviews\"}";
		}else {
			json = "{\"status\": \"No Reviews Initiated! Year records have not been generated!\"}";
		}
		return json;
	}
	
}
