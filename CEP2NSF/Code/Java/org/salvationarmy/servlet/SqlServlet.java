package org.salvationarmy.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.salvationarmy.cep.CepFactory;
import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.model.EssentialMinistry;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.exception.WrongParametersException;
import org.salvationarmy.sql.Configuration;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import frostillicus.xsp.servlet.AbstractXSPServlet;

public class SqlServlet extends AbstractXSPServlet {
	
	private Configuration config;
	private Sql sql;
	private HttpServletRequest req;
	private HttpServletResponse res;
	
	@Override
	protected void doService(HttpServletRequest req, HttpServletResponse res,
			FacesContext facesContext, ServletOutputStream out) throws IOException {
		setReq(req);
		setRes(res);
		String json = null;
		try {
			if (req.getParameter("query") != null) {
				ResultSet resultSet;
				resultSet = getSql().doQuery(req.getParameter("query"));
				if (resultSet != null) {
					json = processResultset(resultSet);
				}
			}else if (req.getParameter("method") != null) {
				String methodName = req.getParameter("method");
				Method method = null;
				if (methodName.equals("createNewEvalFromCurrent") || methodName.equals("getReviews")) {
					method = this.getClass().getMethod(methodName, HttpServletRequest.class);
					if (method != null) {
						json = (String) method.invoke(this, req);
					}
				}else{
					if (methodName.equalsIgnoreCase("getLocationYearList")) {
						method = this.getClass().getMethod("getLocationYearList", String.class, String.class);
						String locationId = req.getParameter("locationid");
						String reviewId = req.getParameter("reviewId");
						if ((locationId == null || locationId.isEmpty()) && (reviewId != null && !reviewId.isEmpty())) {
							if (method != null) {
								json = (String) method.invoke(this, null, reviewId);
							}
						}else {
							if (method != null) {
								json = (String) method.invoke(this, locationId, null);
							}
						}
					}else{
						method = this.getClass().getMethod(methodName);
						if (method != null) {
							json = (String) method.invoke(this);
						}
					}
				}
			}else{
				throw new WrongParametersException("No Acceptable Parameters were defined for SqlServlet");
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setStatus(500);
			json = CepUtil.getErrorJson(e);
			CepUtil.logAction("SqlServlet.doService ERROR: " + e.getMessage());
		}
		out.print(json);
	}
	
	public String getVersion() {
		return getSql().getJdbcVersion();
	}
	
	public String getConfig() {
		if (null == config) {
			config = new Configuration();
		}
		return config.toJson();
	}
	
	private Sql getSql() {
		//sql = (Sql) ExtLibUtil.getApplicationScope().get("Sql");
		if (null == sql) {
			sql = new Sql();
		}
		return sql;
	}
	
	public String getReviews(HttpServletRequest req) throws NoResultsException, SQLException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		String json = "[]";
		String year = req.getParameter("year");
		String query = "SELECT * FROM Reviews WHERE year=" + year;
		ResultSet resultSet;
		resultSet = getSql().doQuery(query);
		if (resultSet != null) {
			json = processResultset(resultSet);
		}
		return json;
	}
	
	public String keepAlive() {
		String json = "{}";
		Map<String, String> keepAliveResponse = new HashMap<String, String>();
		keepAliveResponse.put("result", "IT'S ALIVE!!!");
		keepAliveResponse.put("time", new Date().toLocaleString());
		Gson gson = new Gson();
		json = gson.toJson(keepAliveResponse);
		return json;
	}
	
	public String getCorpInfo() throws SQLException {
		String result = "[]";
		String query = "getCorpInfo";
		Comparator<Map<String, Object>> comparator = new Comparator <Map<String, Object>>() {
			public int compare(Map<String, Object> map1, Map<String, Object> map2) {
				return map1.get("unit_name").toString().compareTo(map2.get("unit_name").toString());
			}
		};
		SortedSet<Map<String, Object>> corpList = new TreeSet<Map<String, Object>>(comparator);
		ResultSet resultSet = getSql().doQuery(query);
		ResultSetMetaData md = resultSet.getMetaData();
		int colCount = md == null ? 0 : md.getColumnCount();
		while(resultSet.next()) {
			Map<String, Object> recordMap = new HashMap<String, Object>();
			for (int i = 1; i <= colCount; i++) {
				String label = md.getColumnLabel(i);
				Object value = resultSet.getObject(label);
				recordMap.put(label, value);
			}
			corpList.add(recordMap);
		}
		Gson gson = new Gson();
		result = gson.toJson(corpList);
		return result;
	}
	
	public String getDivisionList() throws SQLException {
		String result = "[]";
		String query = "getCorpInfo";
		Comparator<Map<String, Object>> comparator = new Comparator <Map<String, Object>>() {
			public int compare(Map<String, Object> map1, Map<String, Object> map2) {
				return map1.get("div_name").toString().compareTo(map2.get("div_name").toString());
			}
		};
		SortedSet<Map<String, Object>> divList = new TreeSet<Map<String, Object>>(comparator);
		ResultSet resultSet = getSql().doQuery(query);
		while(resultSet.next()) {
			Map<String, Object> recordMap = new HashMap<String, Object>();
			recordMap.put("div_name", resultSet.getString("div_name"));
			recordMap.put("div_id", resultSet.getInt("div_id"));
			divList.add(recordMap);
		}
		
		Gson gson = new Gson();
		result = gson.toJson(divList);
		return result;
	}
	
	public String getYearList() throws SQLException {
		String query = "SELECT * FROM Eval_Years ORDER BY year ASC";
		List<String> yearList = new ArrayList<String>();
		String json = null;
		ResultSet result = getSql().doQuery(query);
		if (result != null) {
			while(result.next()) {
				int yr = result.getInt("year");
				yearList.add(String.valueOf(yr));
			}
		}
		Gson gson = new Gson();
		json = gson.toJson(yearList);
		return json;
	}
	
	public String getLocationYearList(String locationId, String reviewId) throws SQLException {
		//System.out.println("SqlServlet.getLocationYearList, locationId=" + locationId + ", reviewId=" + reviewId);
		List<Map<String,String>> yearList = new ArrayList<Map<String,String>>();
		Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		String json = null;
		String query = null;
		if (reviewId != null && !reviewId.isEmpty() && !reviewId.equals("undefined")) {
			query = "SELECT * FROM Reviews WHERE revid=" + reviewId + ";";
			ResultSet reviewResult;
			reviewResult = CepUtil.getSql().doQuery(query);
			if (reviewResult.next()) {
				Integer locationIdInt = reviewResult.getInt("locationid");
				locationId = Integer.toString(locationIdInt);
			}
		}
		if (locationId != null && !locationId.isEmpty() && !locationId.equals("undefined")) {
			query = "SELECT year,revid FROM Reviews WHERE locationid=" + locationId + " order by year desc;";
			ResultSet result;
			result = CepUtil.getSql().doQuery(query);
			int count = 0;
			if (result != null) {
				while (result.next() && count < 50) {
					Map<String,String> yearMap = new HashMap<String,String>();
					int yr = result.getInt("year");
					yearMap.put("year", String.valueOf(yr));
					int revid = result.getInt("revid");
					yearMap.put("revid", String.valueOf(revid));
					yearList.add(yearMap);
					count++;
				}
			}
			json = gson.toJson(yearList);
		}else {
			json = gson.toJson(yearList);
		}
		
		//System.out.println("getLocationYearList returning " + json);
		return json;
	}
	
	public String createNewEvalFromCurrent(HttpServletRequest req) throws SQLException {
		String result = null;
		Map<String,String> resultMap = new HashMap<String,String>();
		String oldYear = req.getParameter("oldYear");
		String newYear = req.getParameter("newYear");
		String query = "createNewFromCurrent " + oldYear + "," + newYear;
		CepUtil.logAction("SqlServlet.createNewEvalFromCurrent: " + query);
		Gson gson = new Gson();
		Statement stmt = getSql().getStatement();
		stmt.execute(query);
		resultMap.put("status", "Success");
		result = gson.toJson(resultMap);
		return result;
	}
	
	public String createNewEval(HttpServletRequest req) throws SQLException {
		String result = null;
		Map<String,String> resultMap = new HashMap<String,String>();
		String newYear = req.getParameter("newYear");
		String query = "createNewEval " + newYear;
		Statement stmt;
		stmt = getSql().getStatement();
		stmt.execute(query);
		CepUtil.logAction("SqlServlet.createNewEval: " + query);
		resultMap.put("status", "Success");
		Gson gson = new Gson();
		result = gson.toJson(resultMap);
		return result;
	}
	
	public String getScoreHistory() throws SQLException {
		//System.out.println("SqlServlet.getScoreHistory");
		String result = null;
		List<Map<String, Object>> scoresList = new ArrayList<Map<String, Object>>();
		String year = req.getParameter("year");
		String locationId = req.getParameter("locationid");
		String emscid = req.getParameter("emscid");
		String query = "EXEC Get_Leadership_Assessment_Score_By_Location " + locationId + "," + emscid;
		//System.out.println("SqlServlet.getScoreHistory, query=" + query);
		Sql sql = CepUtil.getSql();
		ResultSet resultSet = sql.doQuery(query);
		if (resultSet != null) {
			while (resultSet.next()) {
				Integer scoreYear = resultSet.getInt("year");
				if (!String.valueOf(scoreYear).equals(year)) {
					Map<String, Object> scoreMap = new HashMap<String, Object>();
					scoreMap.put("year", scoreYear);
					int score = resultSet.getInt("score");
					if (score < 0) {
						score = 0;
					}
					scoreMap.put("score", score);
					scoreMap.put("subcategory", resultSet.getString("Subcategory"));
					scoreMap.put("MinistryScore", resultSet.getFloat("MinistryScore"));
					scoreMap.put("ParticipationScore", resultSet.getFloat("ParticipationScore"));
					scoreMap.put("StandardScore", resultSet.getFloat("StandardScore"));
					scoreMap.put("MembershipScore", resultSet.getFloat("MembershipScore"));
					scoreMap.put("GrowthScore", resultSet.getFloat("GrowthScore"));
					scoresList.add(scoreMap);
				}
			}
		}
		Gson gson = new Gson();
		result = gson.toJson(scoresList);
		//System.out.println("SqlServlet.getScoreHistory, result=" + result);
		return result;
	}
	
	public String getEssentialMinistries() throws SQLException, NoResultsException {
		String result = null;
		List<EssentialMinistry> essentialMinistries = new ArrayList<EssentialMinistry>();
		String year = String.valueOf(getReq().getParameter("year"));
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM Essential_Ministries");
		sb.append(" WHERE year=" + year);
		sb.append(" ORDER BY sequence ASC");
		String query = sb.toString();
		System.out.println("Review.getEssentialMinistries, query=" + query);
		Sql sql = CepUtil.getSql();
		ResultSet resultSet = sql.doQuery(query);
		if (resultSet != null) {
			ResultSetMetaData md = resultSet.getMetaData();
			int colCount = md == null ? 0 : md.getColumnCount();
			if (colCount > 0) {
				while (resultSet.next()) {
					EssentialMinistry em = new EssentialMinistry();
					em.setEmid(resultSet.getInt("emid"));
					em.setName(resultSet.getString("name"));
					em.setSequence(resultSet.getInt("sequence"));
					em.setDefinition(resultSet.getString("definition"));
					em.setYear(resultSet.getInt("year"));
					//em.setReviewId(getRevid());
					//em.setLocationid(getLocationid());
					//em.setDivid(getDivid());
					em.getImpactQuestions(false);
					//em.getMinistryEvalScore();
					em.getSubCategories(false);
					//em.getEvalScoreText();
					essentialMinistries.add(em);
				}
			}else{
				throw new NoResultsException("No Results from Query");
			}
		}
		Gson gson = new Gson();
		result = gson.toJson(essentialMinistries);
		return result;
	}
	
	private String processResultset(ResultSet resultSet) throws NoResultsException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, SQLException {
		List<ICepObject> resultList = new ArrayList<ICepObject>();
		if (resultSet != null) {
			ResultSetMetaData md = resultSet.getMetaData();
			int colCount = md == null ? 0 : md.getColumnCount();
			if (colCount > 0) {
				while (resultSet.next()) {
					String javaTypeStr = resultSet.getString("javatype");
					ICepObject cepObj = CepFactory.getCepObject(javaTypeStr);
					for (int i = 1; i <= colCount; i++) {
						String label = md.getColumnLabel(i);
						if (!label.equalsIgnoreCase("javatype")) {
							String setMethod = CepFactory.getICepMethodName("set", label);
							Method method = CepFactory.getICepMethod(cepObj.getClass(), setMethod);
							if (method != null) {
								Object value = resultSet.getObject(i);
								method.invoke(cepObj, value);
							}
						}
					}
					resultList.add(cepObj);
				}
			}else{
				throw new NoResultsException("No results from query");
			}
		}else{
			throw new NoResultsException("No results from query");
		}
		Gson gson = new Gson();
		String json = null;
		json = gson.toJson(resultList);
		return json;
	}

	public HttpServletRequest getReq() {
		return req;
	}

	public void setReq(HttpServletRequest req) {
		this.req = req;
	}

	public HttpServletResponse getRes() {
		return res;
	}

	public void setRes(HttpServletResponse res) {
		this.res = res;
	}
}
