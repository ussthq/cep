package org.salvationarmy.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lotus.domino.NotesException;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.cep.model.Action;
import org.salvationarmy.cep.model.EMSubCategory;
import org.salvationarmy.cep.model.EssentialMinistry;
import org.salvationarmy.cep.model.Goal;
import org.salvationarmy.cep.model.ImpactQuestion;
import org.salvationarmy.cep.model.Objective;
import org.salvationarmy.cep.model.Review;
import org.salvationarmy.cep.model.Review.ReviewState;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.exception.NoReviewFoundException;
import org.salvationarmy.exception.UnexpectedCepObjectException;
import org.salvationarmy.servlet.CepServlet.RequestType;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibm.xsp.extlib.util.ExtLibUtil;

import frostillicus.xsp.servlet.AbstractXSPServlet;

public class ReviewServlet extends AbstractXSPServlet {

	private Integer locationId;
	private Integer reviewId;
	private Integer year;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String javaType;
	
	@Override
	protected void doService(HttpServletRequest req, HttpServletResponse res,
			FacesContext facesContext, ServletOutputStream out)
			throws Exception {
		setRequest(req);
		setResponse(res);
		String jsonResult = "{}";
		RequestType type = RequestType.valueOf(req.getMethod().toUpperCase());
		switch (type) {
			case GET:
				String sendEmail = req.getParameter("sendSubmittedEmail");
				String signEmail = req.getParameter("sendSignedEmail");
				if (sendEmail != null && !sendEmail.isEmpty()) {
					String division = req.getParameter("division");
					CepUtil.sendSubmittedEmail(getReviewId(), division);
				}else if (signEmail != null && !signEmail.isEmpty()) {
					String unitPortalId = req.getParameter("unitportalid");
					String unitPortalUrl = req.getParameter("unitportalurl");
					CepUtil.sendSignedEmail(getReviewId(), unitPortalId, unitPortalUrl);
				}else{
					jsonResult = doGet();
				}
				break;
			case PUT:
				jsonResult = doPut();
				break;
			case POST:
				jsonResult = doPut();
				break;
			case DELETE:
				jsonResult = doDelete();
				break;
		}
		
		out.print(jsonResult);
	}
	
	public String doGet() {
		String json = "{\"method\": \"doGet\"}";
		Map<String, Object> reviewMap = new HashMap<String, Object>();
		reviewMap.put("reviewId", getReviewId());
		reviewMap.put("locationId", getLocationId());
		reviewMap.put("year", getYear());
		Review review = null;
		Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		if (getReviewId() == null && getYear() != null && getLocationId() != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT revid FROM Reviews");
			sb.append(" WHERE year=" + getYear());
			sb.append(" AND locationid=" + getLocationId());
			String query = sb.toString();
			Sql sql = CepUtil.getSql();
			try {
				ResultSet resultSet = sql.doQuery(query);
				if (resultSet != null) {
					ResultSetMetaData md = resultSet.getMetaData();
					int colCount = md == null ? 0 : md.getColumnCount();
					if (colCount > 0) {
						if (resultSet.next()) {
							setReviewId(resultSet.getInt("revid"));
							review = getReview();
							reviewMap.put("review", review);
						}
					}
				}
				json = gson.toJson(reviewMap);
			} catch (Exception e) {
				e.printStackTrace();
				getResponse().setStatus(500);
				json = CepUtil.getErrorJson(e);
				CepUtil.logAction("ReviewServlet.doGet for GET ERROR: " + e.getMessage());
			}
		}else if (getReviewId() != null) {
			try {
				review = getReview();
				reviewMap.put("review", review);
				json = gson.toJson(reviewMap);
			} catch (Exception e) {
				e.printStackTrace();
				getResponse().setStatus(500);
				json = CepUtil.getErrorJson(e);
				CepUtil.logAction("ReviewServlet.doGet for GET ERROR: " + e.getMessage());
			}
		}else{
			try {
				String userName = ExtLibUtil.getCurrentSession().getEffectiveUserName();
				String msg = "Review for locationID " + getLocationId() + " and Year " + getYear() + " Not Found for user " + userName + "!";
				throw new NoReviewFoundException(msg);
			} catch (NoReviewFoundException e) {
				e.printStackTrace();
				getResponse().setStatus(500);
				CepUtil.logAction("ReviewServlet.doGet for GET ERROR: " + e.getMessage());
				json = CepUtil.getErrorJson(e);
			} catch (NotesException e) {
				e.printStackTrace();
				getResponse().setStatus(500);
				CepUtil.logAction("ReviewServlet.doGet for GET ERROR: " + e.getMessage());
				json = CepUtil.getErrorJson(e);
			}
		}
		return json;
	}
	
	private Review getReview() throws SQLException, NoResultsException, NoReviewFoundException {
		Review review = new Review();
		review.setRevid(getReviewId());
		review.getSelf();
		return review;
	}
	
	public String doPut() {
		String json = "{\"method\": \"doPut\"}";
		ICepObject cepObject = null;
		try {
			BufferedReader reader = getRequest().getReader();
			cepObject = CepUtil.getCepObject(CepUtil.getJsonString(reader), getJavaType());
			//System.out.println("ReviewServlet.doPut, cepObject=" + cepObject.toJson());
			cepObject.updateReviewRecord();
			json = cepObject.toJson();
			updateUpdater(cepObject);
			getResponse().setStatus(200);
		}catch (Exception e) {
			e.printStackTrace();
			getResponse().setStatus(500);
			CepUtil.logAction("ReviewServlet.doPut for PUT ERROR: " + e.getMessage());
			json = CepUtil.getErrorJson(e);
		}
		return json;
	}
	
	public String doDelete() {
		ICepObject cepObject = null;
		BufferedReader reader;
		String json = null;
		try {
			reader = getRequest().getReader();
			cepObject = CepUtil.getCepObject(CepUtil.getJsonString(reader), getJavaType());
			if (cepObject != null && cepObject instanceof Review) {
				Review review = (Review) cepObject;
				json = review.deleteSelf();
				CepUtil.logAction("ReviewServlet.doDelete: " + json);
			}else {
				String resultedClass = cepObject != null ? cepObject.getClass().getSimpleName() : null;
				throw new UnexpectedCepObjectException("Expected a Review but instead got a " + resultedClass);
			}
		} catch (Exception e) {
			e.printStackTrace();
			getResponse().setStatus(500);
			CepUtil.logAction("ReviewServlet.doDelete for DELETE ERROR: " + e.getMessage());
			json = CepUtil.getErrorJson(e);
		}
		return json;
	}
	
	private void updateUpdater(ICepObject cepObject) throws SQLException, NoResultsException, NotesException, NoReviewFoundException {
		JavaType javaType = JavaType.getJavaType(getJavaType());
		Integer reviewId = null;
		Review review = null;
		switch(javaType) {
			case EMSubCategory:
				EMSubCategory emSubCat = (EMSubCategory) cepObject;
				reviewId = emSubCat.getReviewId();
				review = new Review();
				review.setRevid(reviewId);
				break;
			case EssentialMinistry:
				EssentialMinistry em = (EssentialMinistry) cepObject;
				reviewId = em.getReviewId();
				review = new Review();
				review.setRevid(reviewId);
				break;
			case Goal:
				Goal goal = (Goal) cepObject;
				reviewId = goal.getRevid();
				review = new Review();
				review.setRevid(reviewId);
				break;
			case ImpactQuestion:
				ImpactQuestion iq = (ImpactQuestion) cepObject;
				reviewId = iq.getReviewId();
				review = new Review();
				review.setRevid(reviewId);
				break;
			case Review:
				review = (Review) cepObject;
				reviewId = review.getRevid();
				break;
			case Objective:
				Objective obj = (Objective) cepObject;
				reviewId = obj.getReviewId();
				break;
			case Action:
				Action act = (Action) cepObject;
				reviewId = act.getReviewId();
		}
		if (reviewId != null) {
			CepUtil.updateUpdaters(reviewId);
			CepUtil.updateLastEdited(reviewId);
		}
		if (review != null) {
			review.getSelf(false);
			if (review.getState().equals(ReviewState.INITIALIZED)) {
				CepUtil.updateStateAndStart(review);
			}
		}
	}
	
	public Integer getLocationId() {
		if (locationId == null) {
			String locationStr = getRequest().getParameter("locationid");
			if (locationStr != null && !locationStr.isEmpty() && !locationStr.equalsIgnoreCase("undefined")) {
				locationId = Integer.parseInt(locationStr);
			}
		}
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Integer getReviewId() {
		//System.out.println("ReviewServlet.getReviewId");
		if (reviewId == null) {
			String reviewStr = getRequest().getParameter("reviewid");
			//System.out.println("ReviewServlet.getReviewId, reviewStr=" + reviewStr);
			if (reviewStr != null && !reviewStr.isEmpty()) {
				reviewId = Integer.parseInt(reviewStr);
			}
		}
		//System.out.println("ReviewServlet.getReviewId, returning " + reviewId);
		return reviewId;
	}

	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}

	public Integer getYear() {
		if (year == null) {
			String yearStr = getRequest().getParameter("year");
			if (yearStr != null && !yearStr.isEmpty()) {
				year = Integer.parseInt(yearStr);
			}
		}
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public String getJavaType() {
		if (javaType == null) {
			String javaTypeStr = getRequest().getParameter("javatype");
			if (javaTypeStr != null && !javaTypeStr.isEmpty()) {
				javaType = javaTypeStr;
			}
		}
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

}
