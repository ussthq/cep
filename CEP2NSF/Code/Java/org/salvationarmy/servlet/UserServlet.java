package org.salvationarmy.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.salvationarmy.commons.CepUtil;

import lotus.domino.Database;
import lotus.domino.Name;
import lotus.domino.NotesException;
import lotus.domino.Session;

import com.google.gson.Gson;
import com.ibm.xsp.extlib.util.ExtLibUtil;

import frostillicus.xsp.servlet.AbstractXSPServlet;

public class UserServlet extends AbstractXSPServlet {

	/*
	 * public static final int LEVEL_NOACCESS = 0;
	 * public static final int LEVEL_DEPOSITOR = 1;
	 * public static final int LEVEL_READER = 2;
	 * public static final int LEVEL_AUTHOR = 3;
	 * public static final int LEVEL_EDITOR = 4;
	 * public static final int LEVEL_DESIGNER = 5;
	 * public static final int LEVEL_MANAGER = 6;
	 * public static final int ACL_MAX_ROLES = 80;
	 */
	
	/*
	 * URL for this servlet is cep2.nsf/xsp/user
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doService(HttpServletRequest req, HttpServletResponse res,
			FacesContext facesContext, ServletOutputStream out) throws IOException {
		Session session = ExtLibUtil.getCurrentSession();
		String effName;
		String json = null;
		try {
			effName = session.getEffectiveUserName();
			Name userName = session.createName(effName);
			
			Database db = ExtLibUtil.getCurrentDatabase();
			Vector<String> rolesVect = db.queryAccessRoles(effName);
	
			String abbreviatedName = userName.getAbbreviated();
			String canonicalName = userName.getCanonical();
			//Integer locationId = CepUtil.getCorpLocationId(canonicalName);
			
			String unitPortalUrl = req.getParameter("unitportalurl");
			String unitPortalId = req.getParameter("unitportalid");
			
			Map<String, Object> unitPortalInfo = CepUtil.getUnitPortalResponse(unitPortalId, unitPortalUrl);
			Map<String, Object> unitPortalDiv = null;
			if (unitPortalInfo != null) {
				if (((String) unitPortalInfo.get("UnitType")).equalsIgnoreCase("Divisional Headquarters")) {
					unitPortalDiv = unitPortalInfo;
				}else {
					unitPortalDiv = CepUtil.getUnitPortalDivResponse(unitPortalId, unitPortalUrl, (String) unitPortalInfo.get("DivisionId"));
				}
			}
			
			Map<String, Object> userMap = new HashMap<String, Object>();
			userMap.put("abbreviatedName", abbreviatedName);
			userMap.put("access", db.getCurrentAccessLevel());
			userMap.put("canonicalName", canonicalName);
			userMap.put("commonName", userName.getCommon());
			userMap.put("roles", rolesVect.toArray());

			// From Unit Portal
			if (unitPortalDiv != null) {
				ArrayList<Map<String, Object>> unitDataArr = (ArrayList<Map<String, Object>>) unitPortalInfo.get("Data");
				ArrayList<Map<String, Object>> divDataArr = (ArrayList<Map<String, Object>>) unitPortalDiv.get("Data");
				
				Integer locationId = null;
				Object locId = getExtendedDataItem(unitDataArr, "locationid", 1015.0);
				if (locId instanceof String) {
					locationId = Integer.valueOf(locId.toString());
				}else {
					CepUtil.logAction("Can't find location ID for " + abbreviatedName);
				}
				
				String zip = null;
				Object zipObj = getExtendedDataItem(unitDataArr, "zipcode", 1013.0);
				if (zipObj != null && zipObj instanceof String) {
					zip = zipObj.toString();
				}
				if (zip == null || zip.isEmpty()) {
					zipObj = getExtendedDataItem(divDataArr, "Zip", 1018.0);
					if (zipObj != null) {
						zip = zipObj.toString();
					}
				}
				
				Integer divId = null;
				Object divIdObj = getExtendedDataItem(unitDataArr, "div_id", 1014.0);
				if (divIdObj != null && divIdObj instanceof String) {
					divId = Integer.valueOf(divIdObj.toString());
				}
				
				String division = null;
				Object divObj = getExtendedDataItem(unitDataArr, "CommandCode", 1018.0);
				if (divObj != null && divObj instanceof String) {
					division = divObj.toString();
				}

				userMap.put("corpZip", zip);
				userMap.put("divid", divId);
				userMap.put("division", division);
				userMap.put("locationSID", CepUtil.getLocationSID(abbreviatedName));
				userMap.put("locationid", locationId);
				userMap.put("corpname", unitPortalInfo.get("UnitName").toString());
				userMap.put("unitPortalUnitId", unitPortalInfo.get("UnitId"));
				userMap.put("unitPortalDivId", unitPortalDiv.get("UnitId"));
				// For troubleshooting purposes
				// userMap.put("unitPortalInfo", unitPortalInfo);
				// userMap.put("unitPortalDiv", unitPortalDiv);
			}
			res.setStatus(HttpServletResponse.SC_OK);
			Gson gson = new Gson();
			json = gson.toJson(userMap);
		} catch (Exception e) {
			e.printStackTrace();
			res.setStatus(500);
			CepUtil.logAction("UserServlet.doService ERROR: " + e.getMessage());
			json = CepUtil.getErrorJson(e);
		}
		out.print(json);
		
	}
	
	/**
	 * Get an item from the extended data endpoint from Unit Portal
	 * @param extdData - The "Data" property from the extended data item
	 * @param property - The property in the found record to return
	 * @param extSystemId - The externalSystemId in the extended data item to get the property from
	 * @return Object
	 */
	@SuppressWarnings("unchecked")
	private Object getExtendedDataItem(ArrayList<Map<String, Object>> extdData, String property, double extSystemId) {
		// System.out.println("getExtendedDataItem property=" + property);
		// System.out.println("getExtendedDataItem extSystemId=" + extSystemId);
		Object val = null;
		if (extdData != null) {
			List<Map<String, Object>> externalItems = new ArrayList();
			for (int i = 0; i < extdData.size(); i++) {
				Map<String, Object> dataItem = extdData.get(i);
				Double externalSystemId = (Double) dataItem.get("ExternalSystemId");
				// System.out.println("getExtendedDataItem, externalSystemId=" + externalSystemId);
				if (externalSystemId == extSystemId) {
					Map<String, Object> extendedData = (Map<String, Object>) dataItem.get("ExtendedData");
					val = extendedData.get(property);
					externalItems.add(extendedData);
				}
			}
			// System.out.println("getExtendedDataItem, externalItems=" + externalItems.toString());
			if (externalItems.size() > 1) {
				for (int j = 0; j < externalItems.size(); j++) {
					Map<String, Object> extItem = externalItems.get(j);
					Object primary = extItem.get("isPrimary");
					if (primary != null && (Boolean) primary == true) {
						val = extItem.get(property);
					}
				}
			}
		}
		if (val == null) {
			CepUtil.logAction("UserServlet.getExtendedDataItem for property " + property + " and extSystemId " + extSystemId + " failed");
		}
		// System.out.println("getExtendedDataItem returning " + val);
		// System.out.println("*********************");
		return val;
	}

}
