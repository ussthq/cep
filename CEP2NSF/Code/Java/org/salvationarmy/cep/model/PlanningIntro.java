package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.exception.NoReviewFoundException;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class PlanningIntro implements ICepObject {

	private String intro;
	private Integer year;
	private final String javatype = JavaType.PlanningIntro.getJavaTypeString();
	
	public PlanningIntro() {}
	
	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public void getSelf() throws SQLException, NoResultsException,
			NoReviewFoundException {
		if (getYear() != null) {
			String query = "SELECT * FROM Planning_Intro WHERE year=" + getYear();
			Sql sql = (Sql) ExtLibUtil.getApplicationScope().get("SQL");
			try {
				ResultSet resultSet = sql.doQuery(query);
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setYear(resultSet.getInt("year"));
						setIntro(resultSet.getString("intro"));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void updateReviewRecord() throws SQLException {
		// Do Nothing
	}

}
