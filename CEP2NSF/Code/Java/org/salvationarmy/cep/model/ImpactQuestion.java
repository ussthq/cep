package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class ImpactQuestion implements ICepObject {

	private String question;
	private Integer emid;
	private Integer iqid;
	private Integer sequence;
	private Integer reviewId;
	private String answer;
	private final String javatype = JavaType.ImpactQuestion.getJavaTypeString();
	
	public ImpactQuestion() {
		
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getEmid() {
		return emid;
	}

	public void setEmid(Integer emid) {
		this.emid = emid;
	}

	public Integer getIqid() {
		return iqid;
	}

	public void setIqid(Integer iqid) {
		this.iqid = iqid;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public void getSelf() {
		if (getIqid() != null) {
			String query = "SELECT * FROM Impact_Questions WHERE iqid=" + getIqid();
			Sql sql = (Sql) ExtLibUtil.getApplicationScope().get("SQL");
			try {
				ResultSet resultSet = sql.doQuery(query);
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setEmid(resultSet.getInt("emid"));
						setQuestion(resultSet.getString("question"));
						setSequence(resultSet.getInt("sequence"));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Integer getReviewId() {
		return reviewId;
	}

	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}
	
	public String getAnswer() throws SQLException {
		if (answer == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT answer FROM Impact_Question_Answers");
			sb.append(" WHERE iqid=" + getIqid());
			sb.append(" AND revid=" + getReviewId());
			String query = sb.toString();
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					if (resultSet.next()) {
						answer = resultSet.getString("answer");
					}
				}else{
					answer = null;
				}
			}
		}
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public void updateReviewRecord() throws SQLException {
		String answer = getAnswer() != null ? "'" + getAnswer().replaceAll("'", "''") + "'" : null;
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Impact_Question_Answers AS iqa");
		sb.append(" USING Impact_Questions AS iq");
		sb.append(" ON iqa.iqid=" + getIqid() + " AND iqa.revid=" + getReviewId());
		sb.append(" WHEN NOT MATCHED AND iq.iqid=" + getIqid() + " THEN");
		sb.append(" INSERT (answer,iqid,revid) VALUES (" + answer + "," + getIqid() + "," + getReviewId() + ")");
		sb.append(" WHEN MATCHED AND iq.iqid=" + getIqid() + " THEN");
		sb.append(" UPDATE SET answer=" + answer + ";");
		String query = sb.toString();
		//System.out.println("ImpactQuestion.updateReviewRecord, query=" + query);
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
	}
	
}
