package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Objective implements ICepObject {

	private Integer objid;
	private Integer emscid;
	private Integer goalid;
	private String objectivename;
	private Integer idx;
	private List<Action> actions;
	private final String javatype = JavaType.Objective.getJavaTypeString();
	
	public Objective() {}
	
	public void getSelf() {
		if (getObjid() != null) {
			String query = "SELECT * FROM Planning_Goals WHERE goalid=" + getObjid();
			Sql sql = CepUtil.getSql();
			try {
				ResultSet resultSet = sql.doQuery(query);
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setEmscid(resultSet.getInt("emscid"));
						setGoalid(resultSet.getInt("goalid"));
						setObjectivename(resultSet.getString("objectivename"));
						setIdx(resultSet.getInt("idx"));
						getActions();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (NoResultsException e) {
				e.printStackTrace();
			}
		}
	}

	public String toJson() {
		Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		return gson.toJson(this);
	}

	public void updateReviewRecord() throws SQLException {
		//System.out.println("Objective.updateReviewRecord ");
		Integer objid = getObjid();
		Integer goalid = getGoalid();
		Integer emscid = getEmscid();
		Integer idx = getIdx();
		String objectivename = getObjectivename() != null ? "'" + getObjectivename().replaceAll("'", "''") + "'" : "NULL";
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Planning_Objectives AS target");
		sb.append(" USING (SELECT " + objid + ") AS source (objid)");
		sb.append(" ON (target.objid = " + objid + ")");
		sb.append(" WHEN MATCHED AND target.objid = source.objid THEN");
		sb.append(" UPDATE SET emscid = " + emscid);
		sb.append(",goalid = " + goalid);
		sb.append(",objectivename = " + objectivename);
		sb.append(",idx = " + idx);
		sb.append(" WHEN NOT MATCHED THEN");
		sb.append(" INSERT (emscid,goalid,idx,objectivename,javatype)");
		sb.append(" VALUES (" + emscid + "," + goalid + "," + idx + "," + objectivename + ",'" + javatype + "');");
		String query = sb.toString();
		//System.out.println("Objective.updateReviewRecord, query=" + query);
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
		// Got to set the new objid after an insert
		if (objid == null) {
			query = "SELECT * FROM Planning_Objectives WHERE objid=SCOPE_IDENTITY()";
			ResultSet resultSet = sql.doQuery(query);
			resultSet.next();
			setObjid(resultSet.getInt("objid"));
		}
	}

	public Integer getObjid() {
		return objid;
	}

	public void setObjid(Integer objid) {
		this.objid = objid;
	}

	public Integer getEmscid() {
		return emscid;
	}

	public void setEmscid(Integer emscid) {
		this.emscid = emscid;
	}

	public Integer getGoalid() {
		return goalid;
	}

	public void setGoalid(Integer goalid) {
		this.goalid = goalid;
	}

	public String getObjectivename() {
		return objectivename;
	}

	public void setObjectivename(String objectivename) {
		this.objectivename = objectivename;
	}

	public List<Action> getActions() throws SQLException, NoResultsException {
		if (actions == null) {
			actions = new ArrayList<Action>();
			String query = "SELECT * FROM Planning_Actions WHERE objid=" + objid + " ORDER BY idx ASC";
			//System.out.println("Objective.getActions, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						Action act = new Action();
						act.setActid(resultSet.getInt("actid"));
						act.setObjid(resultSet.getInt("objid"));
						act.setPersonresponsible(resultSet.getString("personresponsible"));
						act.setStep(resultSet.getString("step"));
						act.setDuedate(resultSet.getDate("duedate"));
						act.setIdx(resultSet.getInt("idx"));
						actions.add(act);
					}
				}else{
					throw new NoResultsException("No Objectives from Query");
				}
			}
		}
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}

	public Integer getReviewId() throws SQLException {
		Integer revid = null;
		if (getGoalid() != null) {
			String query = "SELECT revid FROM Planning_Goals WHERE goalid=" + getGoalid();
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null && resultSet.next()) {
				revid = resultSet.getInt("revid");
			}
		}
		return revid;
	}
}
