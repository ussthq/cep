package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Action implements ICepObject {

	private Integer actid;
	private Integer objid;
	private Integer idx;
	private String step;
	private String personresponsible;
	private Date duedate;
	private final String javatype = JavaType.Action.getJavaTypeString();
	
	public Action() {}
	
	public void getSelf() {
		if (getActid() != null) {
			String query = "SELECT * FROM Planning_Actions WHERE actid=" + getActid();
			Sql sql = CepUtil.getSql();
			try {
				ResultSet resultSet = sql.doQuery(query);
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setObjid(resultSet.getInt("objid"));
						setStep(resultSet.getString("step"));
						setPersonresponsible(resultSet.getString("personresponsible"));
						setDuedate(resultSet.getDate("duedate"));
						setIdx(resultSet.getInt("idx"));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public String toJson() {
		Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		return gson.toJson(this);
	}

	public void updateReviewRecord() throws SQLException {
		//System.out.println("Action.updateReviewRecord ");
		Integer actid = getActid();
		Integer objid = getObjid();
		Integer idx = getIdx();
		String step = getStep() != null ? "'" + getStep().replaceAll("'", "''") + "'" : "NULL";
		String personresponsible = getPersonresponsible() != null ? "'" + getPersonresponsible().replaceAll("'", "''") + "'" : "NULL";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String dueDateStr = getDuedate() == null ? "NULL" : "'" + sdf.format(getDuedate()) + "'";
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Planning_Actions AS target");
		sb.append(" USING (SELECT " + actid + ") AS source (actid)");
		sb.append(" ON (target.actid = " + actid + ")");
		sb.append(" WHEN MATCHED AND target.actid = source.actid THEN");
		sb.append(" UPDATE SET objid = " + objid);
		sb.append(",step=" + step);
		sb.append(",personresponsible=" + personresponsible);
		sb.append(",duedate=" + dueDateStr);
		sb.append(",idx=" + idx);
		sb.append(" WHEN NOT MATCHED THEN");
		sb.append(" INSERT (objid,idx,step,personresponsible,duedate,javatype)");
		sb.append(" VALUES (" + objid + "," + idx + "," + step + "," + personresponsible + "," + dueDateStr + ",'" + javatype + "');");
		String query = sb.toString();
		//System.out.println("Goal.updateReviewRecord, query=" + query);
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
		// Got to set the new actid after an insert
		if (actid == null) {
			query = "SELECT * FROM Planning_Actions WHERE actid=SCOPE_IDENTITY()";
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet.next()) {
				setActid(resultSet.getInt("actid"));
			}
		}
	}

	public Integer getActid() {
		return actid;
	}

	public void setActid(Integer actid) {
		this.actid = actid;
	}

	public Integer getObjid() {
		return objid;
	}

	public void setObjid(Integer objid) {
		this.objid = objid;
	}

	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public String getPersonresponsible() {
		return personresponsible;
	}

	public void setPersonresponsible(String personresponsible) {
		this.personresponsible = personresponsible;
	}

	public Date getDuedate() {
		return duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}
	
	public Integer getReviewId() throws SQLException {
		Integer revid = null;
		if (getObjid() != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT goal.revid from Planning_Actions AS act");
			sb.append(" JOIN Planning_Objectives AS obj");
			sb.append(" ON act.objid=obj.objid");
			sb.append(" JOIN Planning_Goals AS goal");
			sb.append(" ON obj.goalid=goal.goalid");
			sb.append(" WHERE act.actid=" + getActid() + ";");
			String query = sb.toString();
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				if (resultSet.next()) {
					revid = resultSet.getInt("revid");
				}
			}
		}
		return revid;
	}

}
