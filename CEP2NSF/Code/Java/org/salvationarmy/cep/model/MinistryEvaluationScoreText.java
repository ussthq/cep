package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class MinistryEvaluationScoreText implements ICepObject {

	private String text;
	private Integer metid;
	private Integer year;
	private Integer forscore;
	private final String javatype = JavaType.MinistryEvaluationScoreText.getJavaTypeString();
	
	public MinistryEvaluationScoreText() {
		
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getMetid() {
		return metid;
	}

	public void setMetid(Integer metid) {
		this.metid = metid;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getForscore() {
		return forscore;
	}

	public void setForscore(Integer forscore) {
		this.forscore = forscore;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public void getSelf() {
		if (getMetid() != null) {
			String query = "SELECT * FROM Ministry_Evaluation_Score_Text WHERE metid=" + getMetid();
			Sql sql = (Sql) ExtLibUtil.getApplicationScope().get("SQL");
			try {
				ResultSet resultSet = sql.doQuery(query);
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setYear(resultSet.getInt("year"));
						setText(resultSet.getString("text"));
						setForscore(resultSet.getInt("forscore"));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void updateReviewRecord() {
		// Do nothing, nothing to udpate
	}
}
