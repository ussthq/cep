package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.sql.Configuration;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;

public class EMSubCategory implements ICepObject {

	private String subcategory;
	private String emName;
	private Integer emscid;
	private Integer emid;
	private Integer sequence;
	private Review review;
	private String duration;
	private Integer score = 0;
	private Integer reviewId;
	private Double MembershipScore = 0.00;
	private Double StandardScore = 0.00;
	private Double ParticipationScore = 0.00;
	private Double GrowthScore = 0.00;
	private Double MinistryScore = 0.00;
	private final String javatype = JavaType.EMSubCategory.getJavaTypeString();
	
	public EMSubCategory() {
		
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	public Integer getEmscid() {
		return emscid;
	}

	public void setEmscid(Integer emscid) {
		this.emscid = emscid;
	}

	public Integer getEmid() {
		return emid;
	}

	public void setEmid(Integer emid) {
		this.emid = emid;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public void getSelf() {
		//System.out.println("EMSubCategory.getSelf running");
		if (getEmscid() != null) {
			String query = "SELECT * FROM EM_Sub_Categories WHERE emscid=" + getEmscid();
			Sql sql = CepUtil.getSql();
			try {
				ResultSet resultSet = sql.doQuery(query);
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setEmid(resultSet.getInt("emid"));
						setSubcategory(resultSet.getString("subcategory"));
						setSequence(resultSet.getInt("sequence"));
						setDuration(resultSet.getString("duration"));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Integer getScore() throws SQLException {
		if (score == null || score == 0) {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT score FROM Leadership_Assessment");
			sb.append(" WHERE revid=" + getReviewId());
			sb.append(" AND emscid=" + getEmscid());
			String query = sb.toString();
			//System.out.println("EMSubCategory.getScore, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					if (resultSet.next()) {
						score = resultSet.getInt("score");
						if (score < 0) {
							score = 0;
						}
					}
				}else{
					score = 0;
				}
			}else {
				score = 0;
			}
		}
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public void getScores(Integer year, Integer locationId) throws SQLException, NoResultsException {
		//System.out.println("EMSubCategory.getScores, year=" + year + ", locationId=" + locationId);
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT emsc.emscid,emsc.subcategory,");
		sb.append("scores.[MembershipScore],scores.[StandardScore],");
		sb.append("scores.[ParticipationScore],scores.[GrowthScore]");
		sb.append(" FROM EM_Sub_Categories as emsc");
		sb.append(" JOIN Leadership_Assessment as scores");
		sb.append(" ON emsc.emscid=scores.emscid");
		sb.append(" WHERE emsc.emscid=" + getEmscid());
		sb.append(" AND scores.revid=" + getReviewId() + ";");
		String query = sb.toString();
		Sql sql = CepUtil.getSql();
		ResultSet resultSet = sql.doQuery(query);
		if (resultSet != null) {
			ResultSetMetaData md = resultSet.getMetaData();
			int colCount = md == null ? 0 : md.getColumnCount();
			if (colCount > 0) {
				DecimalFormat df = new DecimalFormat("#.00");
				while (resultSet.next()) {
					Double growth = resultSet.getDouble("GrowthScore");
					growth = Double.valueOf(df.format(growth));
					Double standard = resultSet.getDouble("StandardScore");
					standard = Double.valueOf(df.format(standard));
					Double mem = resultSet.getDouble("MembershipScore");
					mem = Double.valueOf(df.format(mem));
					Double part = resultSet.getDouble("ParticipationScore");
					part = Double.valueOf(df.format(part));
					setGrowthScore(growth);
					setStandardScore(standard);
					setMembershipScore(mem);
					setParticipationScore(part);
				}
			}else {
				throw new NoResultsException("NO Results for " + query);
			}
		}
	}
	
	public Integer getReviewId() {
		return reviewId;
	}

	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}

	public void updateReviewRecord() throws SQLException {
		//System.out.println("EMSubCategory.updateReviewRecord, MinistryScore=" + MinistryScore);
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Leadership_Assessment AS la");
		sb.append(" USING EM_Sub_Categories AS emsc");
		sb.append(" ON la.emscid=" + getEmscid() + " AND la.revid=" + getReviewId());
		sb.append(" WHEN NOT MATCHED AND emsc.emscid=" + getEmscid() + " THEN");
		sb.append(" INSERT (score,emscid,revid,GrowthScore,MembershipScore,MinistryScore,ParticipationScore,StandardScore)");
		sb.append(" VALUES (" + score + "," + getEmscid() + "," + 
				getReviewId() + "," + getGrowthScore() + "," + 
				getMembershipScore() + "," + MinistryScore + "," + 
				getParticipationScore() + "," + getStandardScore() + 
		")");
		sb.append(" WHEN MATCHED AND emsc.emscid=" + getEmscid() + " THEN");
		sb.append(" UPDATE SET score=" + score + ",GrowthScore=" + getGrowthScore() +
				",MembershipScore=" + getMembershipScore() + ",MinistryScore=" + MinistryScore + 
				",ParticipationScore=" + getParticipationScore() +
				",StandardScore=" + getStandardScore() + ";");
		String query = sb.toString();
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
		//System.out.println("EMSubCategory.updateReviewRecord, DONE");
	}

	public Double getMembershipScore() {
		return MembershipScore;
	}

	public void setMembershipScore(Double membershipScore) {
		this.MembershipScore = membershipScore;
	}

	public Double getParticipationScore() {
		return ParticipationScore;
	}

	public void setParticipationScore(Double ParticipationScore) {
		this.ParticipationScore = ParticipationScore;
	}

	public Double getGrowthScore() {
		return GrowthScore;
	}

	public void setGrowthScore(Double GrowthScore) {
		this.GrowthScore = GrowthScore;
	}

	public Double getMinistryScore() throws SQLException {
		//System.out.println("EMSubCategory.getMinistryScore");
		if (MinistryScore == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT MinistryScore FROM Leadership_Assessment");
			sb.append(" WHERE revid=" + getReviewId());
			sb.append(" AND emscid=" + getEmscid());
			String query = sb.toString();
			//System.out.println("EMSubCategory.getMinistryScore, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				if (resultSet.next()) {
					//System.out.println("EMSubCategory.getMinistryScore, got a resultSet");
					MinistryScore = resultSet.getDouble("MinistryScore");
				}
				if (MinistryScore == null || MinistryScore.intValue() == 0) {
					//System.out.println("EMSubCategory.getMinistryScore, NO resultSet");
					Double growScore = getGrowthScore() == null ? 0 : getGrowthScore().doubleValue();
					Double memScore = getMembershipScore() == null ? 0 : getMembershipScore().doubleValue();
					Double partScore = getParticipationScore() == null ? 0 : getParticipationScore().doubleValue();
					Double standScore = getStandardScore() == null ? 0 : getStandardScore().doubleValue();
					Double leadScore = getScore() == null ? 0 : getScore().doubleValue();
					MinistryScore = (growScore + memScore + partScore + standScore + leadScore) / 5;
				}
			}
		}
		return MinistryScore;
	}

	public void setMinistryScore(Double ministryScore) {
		this.MinistryScore = ministryScore;
	}

	public Double getStandardScore() {
		return StandardScore;
	}

	public void setStandardScore(Double standardScore) {
		StandardScore = standardScore;
	}
	
	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}

	public String getEmName() {
		return emName;
	}

	public void setEmName(String emName) {
		this.emName = emName;
	}

}
