package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.exception.NoReviewFoundException;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;

public class Review implements ICepObject {
	
	private Integer year;
	private Boolean locked;
	private Date startdate;
	private ReviewState state;
	private String officer;
	private Integer revid;
	private Integer divid;
	private Boolean submitted;
	private Integer locationid;
	private String lasteditedby;
	private String corpname;
	private String attendees;
	private String submittedby;
	private Date submitteddate;
	private String officerreview;
	private String signedby;
	private Date signeddate;
	private Boolean signed;
	private Integer unitid;
	private List<EssentialMinistry> essentialMinistries;
	private List<Goal> goals;
	private final String javatype = JavaType.Review.getJavaTypeString();
	
	public enum ReviewState {
		INITIALIZED, CONFIRMED, SCORED, OPENED, SUBMITTED, REVIEWED, COMPLETED;
	}
	
	public Review() {

	}
	
	public void initValues() {
		
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public ReviewState getState() {
		return state;
	}

	public void setState(ReviewState state) {
		this.state = state;
	}
	
	public void setState(String state) {
		this.state = ReviewState.valueOf(state.toUpperCase());
	}

	public String getOfficer() {
		return officer;
	}

	public void setOfficer(String officer) {
		this.officer = officer;
	}

	public Integer getRevid() {
		return revid;
	}

	public void setRevid(Integer revid) {
		this.revid = revid;
	}

	public Integer getLocationid() {
		return locationid;
	}

	public void setLocationid(Integer locationid) {
		this.locationid = locationid;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public String deleteSelf() throws SQLException {
		String json = toJson();
		String query = "deepDeleteReview " + getRevid();
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
		return json;
	}
	
	public void getSelf() throws SQLException, NoResultsException, NoReviewFoundException {
		getSelf(true);
	}
	
	public void getSelf(Boolean getChildren) throws SQLException, NoResultsException, NoReviewFoundException {
		if (getRevid() != null) {
			String query = "SELECT * FROM Reviews WHERE revid=" + getRevid();
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setYear(resultSet.getInt("year"));
						setLocationid(resultSet.getInt("locationid"));
						setOfficer(resultSet.getString("officer"));
						setState(ReviewState.valueOf(resultSet.getString("state").toUpperCase()));
						setStartdate(resultSet.getDate("startdate"));
						setLocked(resultSet.getBoolean("locked"));
						setCorpname(resultSet.getString("corpname"));
						setDivid(resultSet.getInt("divid"));
						setState(resultSet.getString("state"));
						setSubmitted(resultSet.getBoolean("submitted"));
						setSubmittedby(resultSet.getString("submittedby"));
						setSubmitteddate(resultSet.getDate("submitteddate"));
						setAttendees(resultSet.getString("attendees"));
						setOfficerreview(resultSet.getString("officerreview"));
						setSigned(resultSet.getBoolean("signed"));
						setSignedby(resultSet.getString("signedby"));
						setSigneddate(resultSet.getDate("signeddate"));
						if (getChildren) {
							getEssentialMinistries();
							getGoals();
						}
					}
				}
			}
		}else{
			String msg = "No Review ID was defined";
			throw new NoReviewFoundException(msg);
		}
	}

	public List<EssentialMinistry> getEssentialMinistries() throws SQLException, NoResultsException {
		if (essentialMinistries == null) {
			essentialMinistries = new ArrayList<EssentialMinistry>();
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM Essential_Ministries");
			sb.append(" WHERE year=" + getYear());
			sb.append(" ORDER BY sequence ASC");
			String query = sb.toString();
			//System.out.println("Review.getEssentialMinistries, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						EssentialMinistry em = new EssentialMinistry();
						em.setEmid(resultSet.getInt("emid"));
						em.setName(resultSet.getString("name"));
						em.setSequence(resultSet.getInt("sequence"));
						em.setDefinition(resultSet.getString("definition"));
						em.setYear(resultSet.getInt("year"));
						em.setReviewId(getRevid());
						em.setLocationid(getLocationid());
						em.setDivid(getDivid());
						em.getImpactQuestions(true);
						//em.getMinistryEvalScore();
						em.getSubCategories(true);
						//em.getEvalScoreText();
						essentialMinistries.add(em);
					}
				}else{
					throw new NoResultsException("No Results from Query");
				}
			}
		}
		return essentialMinistries;
	}

	public void setEssentialMinistries(List<EssentialMinistry> essentialMinistries) {
		this.essentialMinistries = essentialMinistries;
	}

	public Integer getDivid() {
		return divid;
	}

	public void setDivid(Integer divid) {
		this.divid = divid;
	}

	public Boolean getSubmitted() {
		if (submitted == null) {
			submitted = false;
		}
		return submitted;
	}

	public void setSubmitted(Boolean submitted) {
		this.submitted = submitted;
	}

	public String getLasteditedby() {
		return lasteditedby;
	}

	public void setLasteditedby(String lasteditedby) {
		this.lasteditedby = lasteditedby;
	}

	public String getCorpname() {
		return corpname;
	}

	public void setCorpname(String corpname) {
		this.corpname = corpname;
	}

	public void updateReviewRecord() throws SQLException {
		Integer revid = getRevid();
		String attendees = getAttendees() == null ? "NULL" : "'" + getAttendees().replaceAll("'", "''") + "'";
		String officerreview = getOfficerreview() == null ? "NULL" : "'" + getOfficerreview().replaceAll("'", "''") + "'";
		String submittedBy = getSubmittedby() == null ? "NULL" : "'" + getSubmittedby() + "'";
		String signedBy = getSignedby() == null ? "NULL" : "'" + getSignedby() + "'";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String startDateStr = getStartdate() == null ? "NULL" : "'" + sdf.format(getStartdate()) + "'";
		String signedDateStr = getSigneddate() == null ? "NULL" : "'" + sdf.format(getSigneddate()) + "'";
		String submittedDateStr = getSubmitteddate() == null ? "NULL" : "'" + sdf.format(getSubmitteddate()) + "'";
		String state = getState() == null ? "NULL" : "'" + getState().name() + "'";
		String lastUpdatedBy = CepUtil.getCurrentUser() == null ? "NULL" : "'" + CepUtil.getCurrentUser() + "'";
		Integer locked = getLocked() ? 1 : 0;
		Integer submitted = getSubmitted() ? 1 : 0;
		Integer signed = getSigned() ? 1 : 0;
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Reviews AS target");
		sb.append(" USING (SELECT " + revid + ") as source(revid)");
		sb.append(" ON (target.revid=source.revid)");
		sb.append(" WHEN MATCHED AND target.revid=source.revid THEN");
		sb.append(" UPDATE SET attendees=" + attendees);
		sb.append(",officerreview=" + officerreview);
		sb.append(",state=" + state);
		sb.append(",startdate=" + startDateStr);
		sb.append(",submittedby=" + submittedBy);
		sb.append(",submitted=" + submitted);
		sb.append(",submitteddate=" + submittedDateStr);
		sb.append(",locked=" + locked);
		sb.append(",lasteditedby=" + lastUpdatedBy);
		sb.append(",signed=" + signed);
		sb.append(",signedby=" + signedBy);
		sb.append(",signeddate=" + signedDateStr + ";");
		String query = sb.toString();
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
	}

	public List<Goal> getGoals() throws SQLException, NoResultsException {
		if (goals == null) {
			goals = new ArrayList<Goal>();
			String query = "SELECT * FROM Planning_Goals WHERE revid=" + getRevid() + " ORDER BY idx ASC";
			//System.out.println("Review.getGoals, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0 ) {
					while (resultSet.next()) {
						//System.out.println("working on row " + resultSet.getRow());
						try {
							Goal goal = new Goal();
							goal.setGoalid(resultSet.getInt("goalid"));
							//System.out.println("goalid set to " + goal.getGoalid());
							goal.setRevid(resultSet.getInt("revid"));
							goal.setEmid(resultSet.getInt("emid"));
							goal.setIdx(resultSet.getInt("idx"));
							goal.getObjectives();
							try{
								goal.setGoalname(resultSet.getString("goalname"));
							}catch (Exception e) {
								e.printStackTrace();
								goal.setGoalname(null);
							}
							goals.add(goal);
						}catch (Exception tempE) {
							tempE.printStackTrace();
						}
					}
				}else{
					throw new NoResultsException("No Goals for this review");
				}
			}else {
				Goal goal1 = new Goal();
				goal1.setIdx(0);
				goal1.setRevid(getRevid());
				goal1.getObjectives();
				goals.add(goal1);
				Goal goal2 = new Goal();
				goal2.setIdx(1);
				goal2.setRevid(getRevid());
				goal2.getObjectives();
				goals.add(goal2);
			}
		}
		return goals;
	}
	
	public void setGoals(List<Goal> goals) {
		this.goals = goals;
	}

	public String getAttendees() {
		return attendees;
	}

	public void setAttendees(String attendees) {
		this.attendees = attendees;
	}

	public String getSubmittedby() {
		return submittedby;
	}

	public void setSubmittedby(String submittedby) {
		this.submittedby = submittedby;
	}

	public String getOfficerreview() {
		return officerreview;
	}

	public void setOfficerreview(String officerreview) {
		this.officerreview = officerreview;
	}

	public String getSignedby() {
		return signedby;
	}

	public void setSignedby(String signedby) {
		this.signedby = signedby;
	}

	public Date getSigneddate() {
		return signeddate;
	}

	public void setSigneddate(Date signeddate) {
		this.signeddate = signeddate;
	}

	public Date getSubmitteddate() {
		return submitteddate;
	}

	public void setSubmitteddate(Date submitteddate) {
		this.submitteddate = submitteddate;
	}

	public Boolean getSigned() {
		if (signed == null) {
			signed = false;
		}
		return signed;
	}

	public void setSigned(Boolean signed) {
		this.signed = signed;
	}

	public Integer getUnitid() {
		return unitid;
	}

	public void setUnitid(Integer unitid) {
		this.unitid = unitid;
	}

}
