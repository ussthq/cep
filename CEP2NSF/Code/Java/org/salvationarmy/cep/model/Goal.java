package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Goal implements ICepObject {

	private Integer goalid;
	private Integer revid;
	private Integer emid;
	private Integer idx;
	private List<Objective> objectives;
	private String goalname;
	private final String javatype = JavaType.Goal.getJavaTypeString();
	
	public Goal() {
		
	}
	
	public void getSelf() {
		if (getGoalid() != null) {
			String query = "SELECT * FROM Planning_Goals WHERE goalid=" + getGoalid();
			Sql sql = CepUtil.getSql();
			try {
				ResultSet resultSet = sql.doQuery(query);
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setEmid(resultSet.getInt("emid"));
						setRevid(resultSet.getInt("revid"));
						setIdx(resultSet.getInt("idx"));
						setGoalname(resultSet.getString("goalname"));
						getObjectives();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (NoResultsException e) {
				e.printStackTrace();
			}
		}
	}

	public String toJson() {
		Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		return gson.toJson(this);
	}

	public void updateReviewRecord() throws SQLException {
		Integer emid = getEmid();
		Integer goalid = getGoalid();
		Integer revid = getRevid();
		Integer idx = getIdx();
		String goalname = getGoalname() != null ? "'" + getGoalname().replaceAll("'", "''") + "'" : "NULL";
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Planning_Goals AS target");
		sb.append(" USING (SELECT " + goalid + ") AS source (goalid)");
		sb.append(" ON (target.goalid = " + goalid + ")");
		sb.append(" WHEN MATCHED AND target.goalid = source.goalid THEN");
		sb.append(" UPDATE SET revid = " + revid);
		sb.append(",emid = " + emid);
		sb.append(",idx = " + idx);
		sb.append(",goalname = " + goalname);
		sb.append(" WHEN NOT MATCHED THEN");
		sb.append(" INSERT (revid,emid,idx,goalname,javatype)");
		sb.append(" VALUES (" + revid + "," + emid + "," + idx + "," + goalname + ",'" + javatype + "');");
		String query = sb.toString();
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
		// Got to set the new goalid after an insert
		if (goalid == null) {
			query = "SELECT * FROM Planning_Goals WHERE goalid=SCOPE_IDENTITY()";
			ResultSet resultSet = sql.doQuery(query);
			resultSet.next();
			setGoalid(resultSet.getInt("goalid"));
		}
	}

	public Integer getGoalid() {
		return goalid;
	}

	public void setGoalid(Integer goalid) {
		this.goalid = goalid;
	}

	public Integer getRevid() {
		return revid;
	}

	public void setRevid(Integer revid) {
		this.revid = revid;
	}

	public Integer getEmid() {
		return emid;
	}

	public void setEmid(Integer emid) {
		this.emid = emid;
	}

	public List<Objective> getObjectives() throws SQLException, NoResultsException {
		if (objectives == null) {
			objectives = new ArrayList<Objective>();
			String query = "SELECT * FROM Planning_Objectives WHERE goalid=" + goalid + " ORDER BY idx ASC";
			//System.out.println("Goal.getObjectives, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				//System.out.println("Goal.getObjectives, colCount=" + colCount);
				if (colCount > 0) {
					while (resultSet.next()) {
						Objective obj = new Objective();
						obj.setObjid(resultSet.getInt("objid"));
						obj.setGoalid(resultSet.getInt("goalid"));
						obj.setEmscid(resultSet.getInt("emscid"));
						obj.setObjectivename(resultSet.getString("objectivename"));
						obj.setIdx(resultSet.getInt("idx"));
						obj.getActions();
						objectives.add(obj);
					}
				}else{
					throw new NoResultsException("No Objectives from Query");
				}
				if (objectives.size() == 1) {
					Integer presentObjIdx = objectives.get(0).getIdx();
					Integer otherIdx = 0;
					if (presentObjIdx == 0) {
						otherIdx = 1;
					}
					objectives.add(createBlankObjective(otherIdx));
				}else if (objectives.size() == 0) {
					objectives.add(createBlankObjective(0));
					objectives.add(createBlankObjective(1));
				}
			}else {
				objectives.add(createBlankObjective(0));
				objectives.add(createBlankObjective(1));
			}
		}
		return objectives;
	}

	public void setObjectives(List<Objective> objectives) {
		this.objectives = objectives;
	}
	
	private Objective createBlankObjective(int idx) {
		Objective obj = new Objective();
		obj.setGoalid(getGoalid());
		obj.setIdx(idx);
		obj.setEmscid(null);
		return obj;
	}

	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}

	public String getGoalname() {
		return goalname;
	}

	public void setGoalname(String goalname) {
		this.goalname = goalname;
	}

}
