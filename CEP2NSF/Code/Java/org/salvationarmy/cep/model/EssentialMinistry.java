package org.salvationarmy.cep.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.salvationarmy.cep.ICepObject;
import org.salvationarmy.cep.JavaType;
import org.salvationarmy.commons.CepUtil;
import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.sql.Sql;

import com.google.gson.Gson;

public class EssentialMinistry implements ICepObject {

	private String name;
	private Integer year;
	private Integer emid;
	private Integer sequence;
	private List<EMSubCategory> subCategories;
	private List<ImpactQuestion> impactQuestions;
	private Integer ministryEvalScore;
	private Integer reviewId;
	private List<MinistryEvaluationScoreText> evalScoreText;
	private String definition;
	private Integer divid;
	private Integer locationid;
	private Integer scoringYear;
	private final String javatype = JavaType.EssentialMinistry.getJavaTypeString();
	
	public EssentialMinistry() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getEmid() {
		return emid;
	}

	public void setEmid(Integer emid) {
		this.emid = emid;
	}

	public List<EMSubCategory> getSubCategories(Boolean includeScores) throws SQLException, NoResultsException {
		if (subCategories == null) {
			subCategories = new ArrayList<EMSubCategory>();
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT emsc.* FROM Essential_Ministries AS em");
			sb.append(" JOIN EM_Sub_Categories AS emsc");
			sb.append(" ON emsc.emid=em.emid");
			sb.append(" WHERE emsc.emid=" + getEmid());
			sb.append(" ORDER BY emsc.sequence ASC;");
			String query = sb.toString();
			//System.out.println("EssentialMinistry.getSubCategories, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						EMSubCategory emsc = new EMSubCategory();
						emsc.setDuration(resultSet.getString("duration"));
						emsc.setEmid(resultSet.getInt("emid"));
						emsc.setEmscid(resultSet.getInt("emscid"));
						emsc.setSequence(resultSet.getInt("sequence"));
						emsc.setSubcategory(resultSet.getString("subcategory"));
						emsc.setReviewId(getReviewId());
						emsc.setEmName(getName());
						if (includeScores) {
							emsc.getScore();
							emsc.getScores(getScoringYear(), getLocationid());
							emsc.getMinistryScore();
						}
						subCategories.add(emsc);
					}
				}else{
					throw new NoResultsException("No Results From Query");
				}
			}
		}
		return subCategories;
	}

	public void setSubCategories(List<EMSubCategory> subCategories) {
		this.subCategories = subCategories;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public void getSelf() {
		if (getEmid() != null) {
			String query = "SELECT * FROM Essential_Ministries WHERE emid=" + getEmid();
			Sql sql = CepUtil.getSql();
			try {
				ResultSet resultSet = sql.doQuery(query);
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						setYear(resultSet.getInt("year"));
						setName(resultSet.getString("name"));
						setSequence(resultSet.getInt("sequence"));
						setDefinition(resultSet.getString("definition"));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public List<ImpactQuestion> getImpactQuestions(Boolean includeAnswers) throws SQLException, NoResultsException {
		if (impactQuestions == null) {
			impactQuestions = new ArrayList<ImpactQuestion>();
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT iq.* FROM Essential_Ministries AS em");
			sb.append(" JOIN Impact_Questions AS iq");
			sb.append(" ON iq.emid=em.emid");
			sb.append(" WHERE iq.emid=" + getEmid());
			sb.append(" ORDER BY iq.sequence ASC;");
			String query = sb.toString();
			//System.out.println("EssentialMinisry.getImpactQuestions, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						ImpactQuestion iq = new ImpactQuestion();
						iq.setEmid(resultSet.getInt("emid"));
						iq.setIqid(resultSet.getInt("iqid"));
						iq.setQuestion(resultSet.getString("question"));
						iq.setSequence(resultSet.getInt("sequence"));
						iq.setReviewId(getReviewId());
						if (includeAnswers) {
							iq.getAnswer();
						}
						impactQuestions.add(iq);
					}
				}else{
					throw new NoResultsException("No results from query");
				}
			}
		}
		return impactQuestions;
	}

	public void setImpactQuestions(List<ImpactQuestion> impactQuestions) {
		this.impactQuestions = impactQuestions;
	}

	@Deprecated
	public Integer getMinistryEvalScore() throws SQLException {
		//System.out.println("EssentialMinistry.getMinistryEvalScore");
		if (ministryEvalScore == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT score FROM Ministry_Evaluation_Scores");
			sb.append(" WHERE emid=" + getEmid());
			sb.append(" AND revid=" + getReviewId());
			String query = sb.toString();
			//System.out.println("EssentialMinisry.getMinistryEvalScore, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					if (resultSet.next()) {
						ministryEvalScore = resultSet.getInt("score");
					}
				}else{
					ministryEvalScore = 0;
				}
			}else{
				ministryEvalScore = 0;
			}
	}
		return ministryEvalScore;
	}

	@Deprecated
	public void setMinistryEvalScore(Integer ministryEvalScore) {
		this.ministryEvalScore = ministryEvalScore;
	}

	public Integer getReviewId() {
		return reviewId;
	}

	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}

	@Deprecated
	public List<MinistryEvaluationScoreText> getEvalScoreText() throws SQLException, NoResultsException {
		if (evalScoreText == null) {
			evalScoreText = new ArrayList<MinistryEvaluationScoreText>();
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM Ministry_Evaluation_Score_Text AS met");
			sb.append(" WHERE met.year=" + getYear());
			sb.append(" ORDER BY met.forscore ASC;");
			String query = sb.toString();
			//System.out.println("EssentialMinisry.getEvalScoreText, query=" + query);
			Sql sql = CepUtil.getSql();
			ResultSet resultSet = sql.doQuery(query);
			if (resultSet != null) {
				ResultSetMetaData md = resultSet.getMetaData();
				int colCount = md == null ? 0 : md.getColumnCount();
				if (colCount > 0) {
					while (resultSet.next()) {
						MinistryEvaluationScoreText mest = new MinistryEvaluationScoreText();
						mest.setForscore(resultSet.getInt("forscore"));
						mest.setText(resultSet.getString("text"));
						mest.setMetid(resultSet.getInt("metid"));
						mest.setYear(resultSet.getInt("year"));
						evalScoreText.add(mest);
					}
				}else{
					throw new NoResultsException("No results from query");
				}
			}
		}
		return evalScoreText;
	}
	
	@Deprecated
	public void setEvalScoreText(List<MinistryEvaluationScoreText> evalScoreText) {
		this.evalScoreText = evalScoreText;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public void updateReviewRecord() throws SQLException {
		StringBuilder sb = new StringBuilder();
		sb.append("MERGE Ministry_Evaluation_Scores AS mes");
		sb.append(" USING Essential_Ministries as em");
		sb.append(" ON mes.emid=" + getEmid() + " AND mes.revid=" + getReviewId());
		sb.append(" WHEN NOT MATCHED AND em.emid=" + getEmid() + " THEN");
		sb.append(" INSERT (score,emid,revid) VALUES ('" + ministryEvalScore + "'," + getEmid() + "," + getReviewId() + ")");
		sb.append(" WHEN MATCHED AND em.emid=" + getEmid() + " THEN");
		sb.append(" UPDATE SET score=" + ministryEvalScore + ";");
		String query = sb.toString();
		Sql sql = CepUtil.getSql();
		sql.doUpdate(query);
	}

	public Integer getDivid() {
		return divid;
	}

	public void setDivid(Integer divid) {
		this.divid = divid;
	}

	public Integer getLocationid() {
		return locationid;
	}

	public void setLocationid(Integer locationid) {
		this.locationid = locationid;
	}

	public Integer getScoringYear() {
		if (getYear() != null) {
			Date curDate = new Date();
			if (curDate.getMonth() > 9) {
				scoringYear = getYear() - 1;
			}else {
				scoringYear = getYear();
			}
		}
		return scoringYear;
	}

	public void setScoringYear(Integer scoringYear) {
		this.scoringYear = scoringYear;
	}

}
