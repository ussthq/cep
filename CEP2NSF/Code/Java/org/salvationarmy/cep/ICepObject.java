package org.salvationarmy.cep;

import java.sql.SQLException;

import org.salvationarmy.exception.NoResultsException;
import org.salvationarmy.exception.NoReviewFoundException;

public interface ICepObject {

	public String toJson();
	public void getSelf() throws SQLException, NoResultsException, NoReviewFoundException;
	public void updateReviewRecord() throws SQLException;
}
