package org.salvationarmy.cep;

import java.lang.reflect.Method;
import java.util.Date;

import org.salvationarmy.cep.model.Action;
import org.salvationarmy.cep.model.EMSubCategory;
import org.salvationarmy.cep.model.EssentialMinistry;
import org.salvationarmy.cep.model.Goal;
import org.salvationarmy.cep.model.ImpactQuestion;
import org.salvationarmy.cep.model.IntroAndLinks;
import org.salvationarmy.cep.model.MinistryEvaluationScoreText;
import org.salvationarmy.cep.model.Objective;
import org.salvationarmy.cep.model.PlanningIntro;
import org.salvationarmy.cep.model.Review;

public class CepFactory {

	public static ICepObject getCepObject(JavaType javaType) {
		ICepObject cepObject = null;
		switch(javaType) {
			case EssentialMinistry:
				cepObject = new EssentialMinistry();
				break;
			case EMSubCategory:
				cepObject = new EMSubCategory();
				break;
			case ImpactQuestion:
				cepObject = new ImpactQuestion();
				break;
			case MinistryEvaluationScoreText:
				cepObject = new MinistryEvaluationScoreText();
				break;
			case IntroAndLinks:
				cepObject = new IntroAndLinks();
				break;
			case PlanningIntro:
				cepObject = new PlanningIntro();
				break;
			case Review:
				cepObject = new Review();
				break;
			case Goal:
				cepObject = new Goal();
				break;
			case Action:
				cepObject = new Action();
				break;
			case Objective:
				cepObject = new Objective();
		}
		return cepObject;
	}
	
	public static ICepObject getCepObject(String javaTypeStr) {
		//System.out.println("CepFactory.getCepObject, javaTypeStr=" + javaTypeStr);
		if (javaTypeStr.indexOf(".") > -1) {
			javaTypeStr = javaTypeStr.substring(javaTypeStr.lastIndexOf(".") +1);
		}
		JavaType javaType = JavaType.valueOf(javaTypeStr);
		return getCepObject(javaType);
	}
	
	@SuppressWarnings("unchecked")
	public static Method getICepMethod(Class clazz, String methodName) {
		Method iCepMethod = null;
		try {
			if (methodName.endsWith("id") || methodName.endsWith("Year") || methodName.endsWith("Sequence") || methodName.endsWith("score")) {
				iCepMethod = clazz.getMethod(methodName, Integer.class);
			}else if (methodName.endsWith("date")) {
				iCepMethod = clazz.getMethod(methodName, Date.class);	
			}else if (methodName.equalsIgnoreCase("setSubmitted") || methodName.equalsIgnoreCase("setLocked") || methodName.equalsIgnoreCase("setSigned")) {
				iCepMethod = clazz.getMethod(methodName, Boolean.class);
			}else {
				iCepMethod = clazz.getMethod(methodName, String.class);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return iCepMethod;
	}
	
	public static String getICepMethodName(String methodType, String label) {
		String methodName = null;
		if (methodType == "get") {
			methodName = "get";
		}else if (methodType == "set") {
			methodName = "set";
		}
		char[] labelArray = label.toCharArray();
		labelArray[0] = Character.toUpperCase(labelArray[0]);
		label = new String(labelArray);
		methodName = methodName + label;
		return methodName;
	}
}
