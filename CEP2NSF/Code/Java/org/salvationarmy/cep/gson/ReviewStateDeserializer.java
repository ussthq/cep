package org.salvationarmy.cep.gson;

import java.lang.reflect.Type;

import org.salvationarmy.cep.model.Review;
import org.salvationarmy.cep.model.Review.ReviewState;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class ReviewStateDeserializer implements JsonDeserializer<Review.ReviewState> {

	public ReviewState deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		Review.ReviewState[] states = Review.ReviewState.values();
		for (Review.ReviewState state : states) {
			String jsonStateStr = json.getAsString();
			if (state.name().equals(jsonStateStr)) {
				return state;
			}
		}
		return null;
	}

}
