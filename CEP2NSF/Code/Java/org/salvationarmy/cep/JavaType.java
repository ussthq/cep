package org.salvationarmy.cep;

public enum JavaType {
	EssentialMinistry("org.salvationarmy.cep.model.EssentialMinistry"), EMSubCategory("org.salvationarmy.cep.model.EMSubCategory"), 
	ImpactQuestion("org.salvationarmy.cep.model.ImpactQuestion"), MinistryEvaluationScoreText("org.salvationarmy.cep.model.MinistryEvaluationScoreText"), 
	IntroAndLinks("org.salvationarmy.cep.model.IntroAndLinks"), Review("org.salvationarmy.cep.model.Review"), 
	Goal("org.salvationarmy.cep.model.Goal"), PlanningIntro("org.salvationarmy.cep.model.PlanningIntro"), 
	Objective("org.salvationarmy.cep.model.Objective"), Action("org.salvationarmy.cep.model.Action");
	
	private String javaTypeString;
	
	JavaType(String javaType) {
		String javaTypeClassName = javaType;
		this.javaTypeString = javaTypeClassName;
	}
	
	public String getJavaTypeString() {
		return javaTypeString;
	}
	
	public void setJavaTypeString(String javaTypeString) {
		this.javaTypeString = javaTypeString;
	}
	
	public static JavaType getJavaType(String javaType) {
		JavaType javaTypeEnum = null;
		if (javaType != null && javaType.contains("org.salvationarmy")) {
			javaTypeEnum = JavaType.valueOf(javaType.substring(javaType.lastIndexOf(".") +1));
		}
		return javaTypeEnum;
	}
	
}
