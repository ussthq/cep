package org.salvationarmy.exception;

public class NoConfigurationException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NoConfigurationException() {}
	
	public NoConfigurationException(String message) {
		super(message);
	}
	
	public NoConfigurationException(Throwable cause) {
		super(cause);
	}
	
	public NoConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

}
