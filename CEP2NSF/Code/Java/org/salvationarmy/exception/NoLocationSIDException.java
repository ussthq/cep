package org.salvationarmy.exception;

public class NoLocationSIDException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NoLocationSIDException() {}
	
	public NoLocationSIDException(String message) {
		super(message);
	}
	
	public NoLocationSIDException(Throwable cause) {
		super(cause);
	}
	
	public NoLocationSIDException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
