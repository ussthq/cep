package org.salvationarmy.exception;

public class WrongParametersException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public WrongParametersException() {}
	
	public WrongParametersException(String message) {
		super(message);
	}
	
	public WrongParametersException(Throwable cause) {
		super(cause);
	}
	
	public WrongParametersException(String message, Throwable cause) {
		super(message, cause);
	}

}
