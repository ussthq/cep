package org.salvationarmy.exception;

public class UnexpectedCepObjectException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public UnexpectedCepObjectException() {}
	
	public UnexpectedCepObjectException(String message) {
		super(message);
	}
	
	public UnexpectedCepObjectException(Throwable cause) {
		super(cause);
	}
	
	public UnexpectedCepObjectException(String message, Throwable cause) {
		super(message, cause);
	}

}
