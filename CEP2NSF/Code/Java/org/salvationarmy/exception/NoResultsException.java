package org.salvationarmy.exception;

public class NoResultsException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoResultsException() {}
	
	public NoResultsException(String message) {
		super(message);
	}
	
	public NoResultsException(Throwable cause) {
		super(cause);
	}
	
	public NoResultsException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
