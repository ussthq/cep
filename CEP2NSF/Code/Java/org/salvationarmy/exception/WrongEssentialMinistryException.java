package org.salvationarmy.exception;

public class WrongEssentialMinistryException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public WrongEssentialMinistryException() {}
	
	public WrongEssentialMinistryException(String message) {
		super(message);
	}
	
	public WrongEssentialMinistryException(Throwable cause) {
		super(cause);
	}
	
	public WrongEssentialMinistryException(String message, Throwable cause) {
		super(message, cause);
	}
}
