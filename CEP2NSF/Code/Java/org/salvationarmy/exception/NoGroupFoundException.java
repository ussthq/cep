package org.salvationarmy.exception;

public class NoGroupFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NoGroupFoundException() {}
	
	public NoGroupFoundException(String message) {
		super(message);
	}
	
	public NoGroupFoundException(Throwable cause) {
		super(cause);
	}
	
	public NoGroupFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
