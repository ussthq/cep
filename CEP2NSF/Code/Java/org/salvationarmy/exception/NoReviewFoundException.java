package org.salvationarmy.exception;

public class NoReviewFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoReviewFoundException() {}
	
	public NoReviewFoundException(String message) {
		super(message);
	}
	
	public NoReviewFoundException(Throwable cause) {
		super(cause);
	}
	
	public NoReviewFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
