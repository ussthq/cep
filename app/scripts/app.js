//Ensure user logged in
var origOpen = XMLHttpRequest.prototype.open;
XMLHttpRequest.prototype.open = function() {
	//console.log('XMLHttpRequest.open, args=', arguments);
	var that = this;
	this.addEventListener('loadend', function(xhr) {
		//console.log('XMLHttpRequest.onLoad, args=', arguments);
		//console.log('all headers=', this.getAllResponseHeaders());
		var responseType = this.getResponseHeader('content-type');
		var response = xhr.target.response;
		var responseTxt = xhr.responseText;
		//console.log('XMLHttpRequest.onLoad, response=', response);
		if (response && 'exceptiontype' in response) {
			if (response.exceptiontype === 'UserAccessException') {
				location.href = '/names.nsf?login&redirectto=' + location.href;
			}
		}else if (!response && responseType && responseType.indexOf('text/html') > -1) {
			var serverHeader = this.getResponseHeader('server');
			if (serverHeader === 'Lotus-Domino') {
				location.href = '/names.nsf?login&redirectto=' + location.href;
			}
		}
	});
	origOpen.apply(this, arguments);
	this.setRequestHeader('cache-control', 'max-age=0');
};
(function(document) {
	'use strict';

	var app = document.querySelector('#app');

	app.displayToast = function(toastMsg) {
		this.$.toast.text = toastMsg;
		this.$.toast.show();
	};

})(document);
