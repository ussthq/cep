/// <reference path="../../typings/index.d.ts"/>

/** Base Page Object class. Subclass this to make it easier to test pages. */
abstract class BasePageTester<T extends polymer.Base> {

	/** Page element we're testing */

	get page():T {
		return this._page;
	}

	private _page:T;

	constructor(pageElement:T) {
		this._page = pageElement;
		var className:string = this.constructor.toString().match(/\w+/g)[1];
		console.info("Instantiating new " + className + " instance with page component", pageElement);
	}

	/**
	 * Override to initialize any variables required to test this page (this includes firing or mocking Ajax requests).
	 */
	init(done:Function) {
		done();
	}

	/**
	 * Override to clear any global state or handlers that might have been set during a test; usually called in afterEach().
	 */
	reset(done:Function) {
		done();
	}

	flush(doneCallback:any) {
		let cb;
		doneCallback === undefined ? cb = function () {
		} : cb = doneCallback;
		return this.page.async(cb);
	}
}

/**
 * Page object for testing simple views such as {@link NowView1}.
 */
class SimpleViewTester extends BasePageTester<polymer.Base> {

	get circle():HTMLDivElement {
		return <HTMLDivElement>this.page.$.circle;
	}

	/*constructor(pageElement:NowView1) {
		super(pageElement);
	}*/
}
