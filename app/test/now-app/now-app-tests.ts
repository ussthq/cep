/// <reference path="../../../typings/index.d.ts"/>
/// <reference path="../test-core.ts"/>
/// <reference path="../../elements/now-app/now-app.ts"/>

/* No TS definitions for WebComponentTester, so use declare. */
declare var fixture;
declare var flush;
declare var stub;

/**
 * Page object for testing {@link NowApp}.
 */
class NowAppTester extends BasePageTester<NowApp> {

	private _originalRoute : any;

	get navigationLinks():NodeListOf<Element> {
		return this.page.$.navigationSelector.querySelectorAll("a");
	}

	get currentPage():SimpleViewTester {
		let element:polymer.Base =
			<polymer.Base>Polymer.dom(this.page.$.appPages).querySelector("neon-animatable.iron-selected").firstElementChild;
		return new SimpleViewTester(element);
	}

	constructor(pageElement:NowApp) {
		super(pageElement);
	}

	init(done:Function) {
		console.log(this.page.$.location.path, this.page.$.location.route);
		this._originalRoute = this.page.$.location.route;
		done();
	}

	reset(done:Function) {
		console.log(this.page.$.location.path, this.page.$.location.route);
		this.page.$.location.route = this._originalRoute;
		done();
	}
}

/**
 * Tests for now-app.
 */
describe('now-app tests', function () {
	let app:NowAppTester;

	before(function (done) {
		app = new NowAppTester(fixture('now-app'));
		app.init(done)
	});

	afterEach(function(done) {
		app.reset(done);
	});

	it('Should display the proper page when a navigation link is clicked', function () {
		console.log(document.location);
		let navigationLinks:NodeListOf<Element> = app.navigationLinks;
		for (let i = 0; i < 3; i++) {
			(<HTMLElement>navigationLinks[i]).click();
			chai.expect(app.currentPage.page.is).to.equal('now-view' + (i + 1));
		}
		console.log(document.location);
	})
});
