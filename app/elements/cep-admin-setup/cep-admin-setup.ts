/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-setup')
class CepAdminSetup extends polymer.Base {
	/**
	 * Loading boolean for the spinner
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;

	@property({
		type: Boolean
	})
	emLoading:boolean;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object,
		observer: '_onUserChange'
	})
	user:any;
	/**
	 * Route for the bottom tab
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	bottomTabRoute:any;

	@property({
		type: Boolean
	})
	bottomTabRouteActive:boolean;

	@property({
		type: Number
	})
	reviewYear:number;

	@property({
		type: Array
	})
	essentialMinistries:any;

	attached() {
		document.addEventListener('admin-year-changed', this._onAdminYearChanged.bind(this));
	}

	@observe('mainLoading')
	private _onMainLoadingChange(newVal) {
		if (this.emLoading && !newVal) {
			this.set('mainLoading', this.emLoading);
		}
	}

	@observe('emLoading')
	private _onEvalLoadingChange(newVal) {
		this.set('mainLoading', newVal);
	}

	@observe('bottomTabRoute.prefix,settings')
	private _onBottomTabRoutePrefix(bottomTabRoutePrefix, settings) {
		/*console.log(this.is, '_onBottomTabRoutePrefix', arguments);
		console.log(this.is, '_onBottomTabRoutePrefix, bottomTabRoute=', this.bottomTabRoute);*/
		if (bottomTabRoutePrefix && bottomTabRoutePrefix.indexOf('/admin/setup') > -1) {
			var firstEm = this.essentialMinistries ? this.essentialMinistries[0] : null;
			if (!firstEm || (firstEm && firstEm.year != this.reviewYear)) {
				this._fetchSetupData();
			}
			if (!this.bottomTabRoute.path) {
				this.set('bottomTabRoute.path', '/essentialministries');
			}
		}
		/*if (newVal && newVal.indexOf('/admin/setup') > -1 && !this.bottomTabRoute.path) {
			this.set('bottomTabRoute.path', '/essentialministries');
		}*/
	}

	@listen('fetch-essential-ministries')
	private _onFetchEssentialMinistries(evt, detail) {
		this._fetchSetupData();
	}

	private _onAdminYearChanged(evt) {
		// console.log(this.is, '_onAdminYearChanged', arguments);
		var detail = evt.detail;
		if (detail) {
			var yr = detail;
			// console.log(this.is, '_onAdminYearChanged, yr=', yr);
			this.set('reviewYear', detail);
			// console.log(this.is, '_onAdminYearChanged, reviewYear=', this.reviewYear);
			if (this.bottomTabRouteActive) {
				this._fetchSetupData();
			}
		}
	}

	private _fetchSetupData() {
		// console.log(this.is, '_fetchSetupData, reviewYear=', this.reviewYear);
		if (this.reviewYear) {
			var emAjax = this.$.emAjax;
			emAjax.params = {
				method: 'getEssentialMinistries',
				year: this.reviewYear
			}
			emAjax.generateRequest();

			var introAjax = this.$.introAjax;
			introAjax.params = {
				id: this.reviewYear,
				javatype: 'org.salvationarmy.cep.model.IntroAndLinks'
			};
			introAjax.generateRequest();

			var planningAjax = this.$.planningIntroAjax;
			planningAjax.params = {
				id: this.reviewYear,
				javatype: 'org.salvationarmy.cep.model.PlanningIntro'
			};
			planningAjax.generateRequest();
		}
	}

	private _onUserChange(newVal, oldVal) {
		// Place holder
	}
}

CepAdminSetup.register();
