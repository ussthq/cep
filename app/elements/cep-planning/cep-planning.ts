/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-planning')
class CepPlanning extends polymer.Base {
	/**
	 * The review
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true,
		observer: '_onEvalChange'
	})
	evaluation:any;
	/**
	 * The selected year
	 * @type {Number}
	 */
	@property({
		type: Number,
		observer: '_onSelectedYearChange'
	})
	selectedYear:number;
	/**
	 * The starting list of essential ministries. This array is for
	 * maintaining the lists. Specifically to add a removed item
	 * 'BACK' to the 'other' list
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	emItems:any;
	/**
	 * The starting list of Objective 1.x sub ministries. This array is for
	 * maintaining the lists. Specifically to add a removed item
	 * 'BACK' to the 'other' list
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj1Items:any;
	/**
	 * The starting list of Objective 2.x sub ministries. This array is for
	 * maintaining the lists. Specifically to add a removed item
	 * 'BACK' to the 'other' list
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj2Items:any;
	/**
	 * The essential ministries available for selection of the goal. We need 2
	 * separate arrays of the essential ministries so that we can manipulate their
	 * content based on was was selected in the 'other' goal combo-box
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	em1Items:any;
	/**
	 * The essential ministries available for selection of the goal. We need 2
	 * separate arrays of the essential ministries so that we can manipulate their
	 * content based on was was selected in the 'other' goal combo-box
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	em2Items:any;
	/**
	 * The essential ministry sub categories available for selection of the objective. We need 2
	 * separate arrays of the sub categories so that we can manipulate their
	 * content based on was was selected in the 'other' objective combo-box
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj11Items:any;
	/**
	 * The essential ministry sub categories available for selection of the objective. We need 2
	 * separate arrays of the sub categories so that we can manipulate their
	 * content based on was was selected in the 'other' objective combo-box
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj12Items:any;
	/**
	 * The essential ministry sub categories available for selection of the objective. We need 2
	 * separate arrays of the sub categories so that we can manipulate their
	 * content based on was was selected in the 'other' objective combo-box
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj21Items:any;
	/**
	 * The essential ministry sub categories available for selection of the objective. We need 2
	 * separate arrays of the sub categories so that we can manipulate their
	 * content based on was was selected in the 'other' objective combo-box
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj22Items:any;
	/**
	 * The goals either from the server or what was selected
	 * @type {Array}
	 */
	@property({
		type: Array,
		notify: true,
		value: []
	})
	goals:any;
	/**
	 * Array of invalid items
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	invalidItems:any;
	/**
	 * True if the planning section is valid
	 * @type {Boolean}
	 */
	@property({
		type: Boolean
	})
	isValid:boolean;
	/**
	 * True if this review is locked
	 * @type {Boolean}
	 */
	@computed()
	locked(evaluation) {
		var returnVal = false;
		if (evaluation) {
			returnVal = evaluation.review.locked;
		}
		return returnVal;
	}
	/**
	 * true when we're processing the evaluation we retrieved from the server
	 * @type {Boolean}
	 */
	@property({
		type: Boolean
	})
	_fromServer:any;

	private _onGoalNameChanged(e) {
		let gid = (e.model.__data__.gIdx);
		// Now fire the item-update event for the goal
		this.fire('em-item-updated', e.model.goal);
	}

	/**
	 * Fired when the selected year is changed. Fetch the planning text
	 * @param {String} newVal The new year
	 * @param {String} oldVal The old year
	 */
	private _onSelectedYearChange(newVal, oldVal) {
		if (newVal) {
			var ajax = this.$.planningIntroAjax;
			ajax.params = {
				id: newVal,
				javatype: 'org.salvationarmy.cep.model.PlanningIntro'
			};
			ajax.generateRequest();
		}
	}
	/**
	 * Fired when the evaluation changes. Setup the em#Items arrays and the goals property
	 * @param {Object} newVal The new review
	 * @param {Object} oldVal The old review
	 */
	private _onEvalChange(newVal, oldVal) {
		// console.log(this.is, '_onEvalChange', arguments);
		if (newVal) {
			this._fromServer = true;
			// Populate the emItems arrays for the combo boxes
			var ems = newVal.review.essentialMinistries;
			var goals = newVal.review.goals;
			// Have to maintain these separately so that the lists aren't linked
			// together. If we maintain them together, when one changes they all
			// change
			this.emItems = [];
			this.em1Items = [];
			this.em2Items = [];
			for (let i = 0; i < ems.length; i++) {
				var em = ems[i];
				var emItem = this.getEmItemObj(em.emid).item;
				this.push('emItems', emItem);
				this.push('em1Items', emItem);
				this.push('em2Items', emItem);
			}
			this.goals = goals;
			this._fromServer = false;
		}
	}
	/**
	 * Fired when a new goal 1 is chosen
	 * @param {Number} newVal The ID of the Essential Ministry
	 */
	@observe('goals.#0.emid')
	private _onGoal1Changed(newVal) {
		//console.log(this.is, '_onGoal1Change', arguments);
		//console.log(this.is, '_onGoal1Change, fromServer=', this._fromServer);
		var origItem = this._restoreRemoved('goal', 'goal', 'em2Items');
		this._onGoalChange(newVal, 'goal1');
		if (!this._fromServer) {
			if (!newVal && this.goals[0]) {
				this.goals[0].emid = null;
			}
			// Clear out any objective selections
			var obj11 = this.get('goals.#0.objectives.#0.emscid');
			var obj12 = this.get('goals.#0.objectives.#1.emscid');
			if ((!newVal || (origItem && newVal !== origItem.emid)) && obj11) {
				this.set('goals.#0.objectives.#0.emscid', null);
			}
			if ((!newVal || (origItem && newVal !== origItem.emid)) && obj12) {
				this.set('goals.#0.objectives.#1.emscid', null);
			}
			// Now fire the item-update event for the goal
			this.fire('em-item-updated', this.goals[0]);
		}
	}
	/**
	 * Fired when a new goal 2 is chosen
	 * @param {Number} newVal The ID of the Essential Ministry
	 */
	@observe('goals.#1.emid')
	private _onGoal2Changed(newVal) {
		//console.log(this.is, '_onGoal2Change', arguments);
		// restore any removed items to the 'other' list
		var origItem = this._restoreRemoved('goal', 'goal', 'em1Items');
		this._onGoalChange(newVal, 'goal2');
		if (!this._fromServer) {
			if (!newVal && this.goals[1]) {
				this.goals[1].emid = null;
			}
			// Clear out any objective selections
			var obj21 = this.get('goals.#1.objectives.#0.emscid');
			var obj22 = this.get('goals.#1.objectives.#1.emscid');
			if ((!newVal || (origItem && newVal !== origItem.emid)) && obj21) {
				this.set('goals.#1.objectives.#0.emscid', null);
			}
			if ((!newVal || (origItem && newVal !== origItem.emid)) && obj22) {
				this.set('goals.#1.objectives.#1.emscid', null);
			}
			// Now fire the item-update event for the goal
			this.fire('em-item-updated', this.goals[1]);
		}
	}

	/**
	 * Called by the goal observers. Remove the selection from the 'other'
	 * essential ministry combo-box items. If the value was changed (i.e. oldValue isn't null)
	 * then add the old entry back to the 'other' combo-box items. Also update the goals
	 * array with the proper essential ministry
	 * @param {Number} newVal   The selected essential ministry id
	 * @param {String} goalPath The goal property that was updated
	 */
	private _onGoalChange(newVal, goalPath) {
		//console.log(this.is, '_onGoalChange', arguments);
		var emItems = null;
		var obj1Items = null;
		var obj2Items = null;
		var objItems = null;
		if (goalPath === 'goal1') {
			emItems = 'em2Items';
			obj1Items = 'obj11Items';
			obj2Items = 'obj12Items';
			objItems = 'obj1Items';
		}else if (goalPath === 'goal2') {
			emItems = 'em1Items';
			obj1Items = 'obj21Items';
			obj2Items = 'obj22Items';
			objItems = 'obj2Items';
		}
		if (newVal) {
			// Remove the selected item from the 'other' list
			this.splice(emItems, this.findGoalItemIndex(emItems, newVal), 1);
			// Setup the lists for the objectives combo-boxes
			var newEmItemObj = this.getEmItemObj(newVal);
			this.set(objItems, this.getObjectiveItems(newEmItemObj.item));
			this.set(obj1Items, this.getObjectiveItems(newEmItemObj.item));
			this.set(obj2Items, this.getObjectiveItems(newEmItemObj.item));
		}else {
			this.set(obj1Items, []);
			this.set(obj2Items, []);
			this.set(objItems, []);
		}
	}
	/**
	 * Get the default objective items to put in the combo boxes
	 * @param {Object} emObj The essential ministry object
	 */
	getObjectiveItems(emObj) {
		var items = [];
		if (emObj) {
			for (let i = 0; i < emObj.subcats.length; i++) {
				var subCat = emObj.subcats[i];
				var obj = {
					subcategory: subCat.subcategory,
					id: subCat.emscid
				};
				items.push(obj);
			}
		}
		return items;
	}
	/**
	 * Fired when a new objective is chosen
	 * @param {Number} newVal The ID of the Sub Ministry
	 */
	@observe('goals.#0.objectives.#0.emscid')
	private _onObj11Changed(newVal) {
		var origItem = this._restoreRemoved('objective', 'obj1', 'obj12Items');
		this._onObjectiveChange(newVal, 'obj11');
		if (!this._fromServer) {
			if (this.goals[0] && this.goals[0].objectives[0]) {
				if (!newVal) {
					this.goals[0].objectives[0].emscid = null;
				}
				this.fire('em-item-updated', this.goals[0].objectives[0]);
			}
		}
	}
	/**
	 * Fired when a new objective is chosen
	 * @param {Number} newVal The ID of the Sub Ministry
	 */
	@observe('goals.#0.objectives.#1.emscid')
	private _onObj12Changed(newVal) {
		var origItem = this._restoreRemoved('objective', 'obj1', 'obj11Items');
		this._onObjectiveChange(newVal, 'obj12');
		if (!this._fromServer) {
			if (this.goals[0] && this.goals[0].objectives[1]) {
				if (!newVal) {
					this.goals[0].objectives[1].emscid = null;
				}
				this.fire('em-item-updated', this.goals[0].objectives[1]);
			}
		}
	}
	/**
	 * Fired when a new objective is chosen
	 * @param {Number} newVal The ID of the Sub Ministry
	 */
	@observe('goals.#1.objectives.#0.emscid')
	private _onObj21Changed(newVal) {
		var origItem = this._restoreRemoved('objective', 'obj2', 'obj22Items');
		this._onObjectiveChange(newVal, 'obj21');
		if (!this._fromServer) {
			if (this.goals[1] && this.goals[1].objectives[0]) {
				if (!newVal) {
					this.goals[1].objectives[0].emscid = null;
				}
				this.fire('em-item-updated', this.goals[1].objectives[0]);
			}
		}
	}
	/**
	 * Fired when a new objective is chosen
	 * @param {Number} newVal The ID of the Sub Ministry
	 */
	@observe('goals.#1.objectives.#1.emscid')
	private _onObj22Changed(newVal) {
		var origItem = this._restoreRemoved('objective', 'obj2', 'obj21Items');
		this._onObjectiveChange(newVal, 'obj22');
		if (!this._fromServer) {
			if (this.goals[1] && this.goals[1].objectives[1]) {
				if (!newVal) {
					this.goals[1].objectives[1].emscid = null;
				}
				this.fire('em-item-updated', this.goals[1].objectives[1]);
			}
		}
	}
	/**
	 * Called from the objective observers. Will remove the selected objective from
	 * the 'other' combo-box items. If the item was changed, then add the old one
	 * back to the 'other' combo-box items.
	 * @param {Number} newVal  The ID of the Sub Ministry
	 * @param {Number} newVal  The ID of the old Sub Ministry
	 * @param {String} objPath The variable name that changed
	 */
	private _onObjectiveChange(newVal, objPath) {
		//console.log(this.is, '_onObjectiveChange', arguments);
		var objItems = null;
		var goal = null;
		if (objPath === 'obj11') {
			objItems = 'obj12Items';
			goal = 'goal1';
		}else if (objPath === 'obj12') {
			objItems = 'obj11Items';
			goal = 'goal1';
		}else if (objPath === 'obj21') {
			objItems = 'obj22Items';
			goal = 'goal2';
		}else if (objPath === 'obj22') {
			objItems = 'obj21Items';
			goal = 'goal2';
		}
		// Remove items from the 'other' list
		if (newVal) {
			var idx = this.findObjectiveIndex(objItems, newVal);
			this.splice(objItems, this.findObjectiveIndex(objItems, newVal), 1);
		}
	}
	/**
	 * Restore any removed array items. Scenario: You change a goal or objective
	 * we want to add the previously removed one back to the 'other' list
	 * @param {String} type            'goal' or 'objective'
	 * @param {String} name            The name of the original list
	 * @param {String} restoreListPath The path to the list to add the missing item to
	 * @return {Object} The item restored to the list
	 */
	private _restoreRemoved(type, name, restoreListPath) {
		var restoredItem = null;
		var orig = null;
		var restoreList = this.get(restoreListPath);
		// Get the starting list with all the items in it
		if (type === 'goal') {
			orig = this.emItems;
		}else if (type === 'objective') {
			if (name === 'obj1') {
				orig = this.obj1Items;
			}else if (name === 'obj2') {
				orig = this.obj2Items;
			}
		}
		// Make sure we've got everything
		if (orig && orig.length > 0 && restoreList) {
			// Loop through the original list and find the missing item and add it back
			// to the restoreListPath array
			for (let i = 0; i < orig.length; i++) {
				var origItem = orig[i];
				var restoreItem = restoreList.find(function(item) {
					return item.id === origItem.id;
				});
				if (!restoreItem) {
					this.splice(restoreListPath, i, 0, origItem);
					restoredItem = origItem;
					break;
				}
			}
		}
		return restoredItem;
	}
	/**
	 * Get the items for the 'Goal #' combo box
	 * @param {Number} index The index of the goal
	 */
	getEmComboItems(index) {
		if (index === 0) {
			return this.em1Items;
		}else {
			return this.em2Items;
		}
	}
	/**
	 * Get the items for the 'Objective' combo box
	 * @param {Number} goalIndex  The index of the goal
	 * @param {Number} objIndex   The index of the objective
	 * @param {Array} obj11Items The array for objective 1.1 items
	 * @param {Array} obj12Items The array for objective 1.2 items
	 * @param {Array} obj21Items The array for objective 2.1 items
	 * @param {Array} obj22Items The array for objective 2.2 items
	 */
	getObjComboItems(goalIndex, objIndex, obj11Items, obj12Items, obj21Items, obj22Items) {
		//console.log(this.is, 'getObjItems', arguments);
		if (goalIndex === 0) {
			if (objIndex === 0) {
				return obj11Items;
			}else if (objIndex === 1) {
				return obj12Items;
			}
		}else if (goalIndex === 1) {
			if (objIndex === 0) {
				return obj21Items;
			}else if (objIndex === 1) {
				return obj22Items;
			}
		}
	}
	/**
	 * Get the label for the goal combo box
	 * @param {Number} goalIndex The index of the goal combo box
	 * @return {String} The label
	 */
	getGoalComboLabel(goalIndex) {
		var goalVal = goalIndex + 1;
		return 'Goal ' + goalVal;
	}
	/**
	 * Get the label for the objective combo box
	 * @param {Number} goalIndex The index of the goal
	 * @param {Number} objIndex  The index of the objective
	 * @return {String} The label
	 */
	getObjComboLabel(goalIndex, objIndex) {
		var goalVal = goalIndex + 1;
		var objVal = objIndex + 1;
		return 'Objective ' + goalVal + '.' + objVal;
	}
	/**
	 * Determine if Objective combo box is disabled
	 * @param {Number} emid The emid/goal for this objective
	 * @return {Boolean} true if disabled
	 */
	private _isObjComboLocked(emid) {
		var returnVal = false;
		if (this.locked) {
			returnVal = true;
		}else if (!this.locked && !emid) {
			returnVal = true;
		}
		return returnVal;
	}
	/**
	 * Find the index from the proper list for the passed in goal
	 * @param {String} goal The goal list name
	 * @param {Number} id   The goal id
	 * @returns {Number} the index of the item or null
	 */
	findGoalItemIndex(goal, id) {
		var emItems = this[goal];
		var index = null;
		var foundEm = emItems.find(function(item, idx) {
			index = idx;
			return item.id === id;
		});
		return index;
	}
	/**
	 * Find the index from the proper list for the passed in obj
	 * @param {String} obj The objective item list name
	 * @param {Number} id  The id of the objective
	 * @return {Number} the index of the item or null;
	 */
	findObjectiveIndex(obj, id) {
		var objItems = this[obj];
		var index = null;
		var foundObj = objItems.find(function(item, idx) {
			index = idx;
			return item.id === id;
		})
		return index;
	}
	/**
	 * Get the combo box list goal item. This item will be added
	 * to the combo box
	 * @param {Number} id The goal emid
	 * @returns {Object} an Object with the following properties
	 * index {Number} the index of the item
	 * item {Object} the item for the combo box
	 * em {Object} the essential ministry
	 */
	getEmItemObj(id) {
		var ems = this.evaluation.review.essentialMinistries;
		var index = null;
		var em = ems.find(function(emItem, idx) {
			index = idx;
			return emItem.emid === id;
		});
		var addObj = null;
		if (em) {
			addObj = {
				name: em.name,
				id: em.emid,
				subcats: em.subCategories
			};
		}
		return {index: index, item: addObj, em: em};
	}
	/**
	 * Get the em property from the getEmItemObj function
	 * @param {Number} id The goal emid
	 * @returns {Object} the essential ministry
	 */
	getEmObject(id) {
		return this.getEmItemObj(id).em;
	}
	/**
	 * Get an objective by it's ID
	 * @param {Number} objid The objid
	 */
	getObjById(objid) {
		var returnVal = null;
		var goals = this.goals;
		for (let i = 0; i < goals.length; i++) {
			var goal = goals[i];
			var objs = goal.objectives;
			for (let j = 0; j < objs.length; j++) {
				var obj = objs[j];
				if (obj.objid === objid) {
					returnVal = obj;
					break;
				}
			}
		}
		return returnVal;
	}
	/**
	 * Get a Goal by it's ID
	 * @param {Number} goalid The goalid
	 */
	getGoalById(goalid) {
		var returnVal = null;
		var goals = this.goals;
		for (let i = 0; i < goals.length; i++) {
			var goal = goals[i];
			if (goal.goalid === goalid) {
				returnVal = goal;
				break;
			}
		}
		return returnVal;
	}
	/**
	 * Get the old value
	 * @param {String} type            'goal' or 'objective'
	 * @param {String} name            The name of the original list
	 * @param {String} restoreListPath The path to the list to add the missing item to
	 * @return {Object} The item restored to the list
	 */
	private getOldVal(type, name, restoreListPath) {
		var oldVal = null;
		var orig = null;
		var restoreList = this.get(restoreListPath);
		// Get the starting list with all the items in it
		if (type === 'goal') {
			orig = this.emItems;
		}else if (type === 'objective') {
			if (name === 'obj1') {
				orig = this.obj1Items;
			}else if (name === 'obj2') {
				orig = this.obj2Items;
			}
		}
		// Make sure we've got everything
		if (orig && orig.length > 0 && restoreList) {
			// Loop through the original list and find the missing item
			for (let i = 0; i < orig.length; i++) {
				var origItem = orig[i];
				var restoreItem = restoreList.find(function(item) {
					return item.id === origItem.id;
				});
				if (!restoreItem) {
					oldVal = origItem;
					break;
				}
			}
		}
		return oldVal;
	}

	validate() {
		var isValid = true;
		var invalidItems = [];
		if (!this.locked) {
			if (this.goals && this.goals.length > 0) {
				for (let i = 0; i < this.goals.length; i++) {
					var goal = this.goals[i];
					if (!goal.emid || goal.emid === 0) {
						isValid = false;
						invalidItems.push(goal);
					}
					for (let j = 0; j < goal.objectives.length; j++) {
						var obj = goal.objectives[j];
						if (!obj.emscid || obj.emscid === 0) {
							isValid = false;
							invalidItems.push(obj);
						}
						var validActionCount = 0;
						for (let k = 0; k < obj.actions.length; k++) {
							var action = obj.actions[k];
							if (!('step' in action) && validActionCount === 0) {
								isValid = false;
								invalidItems.push(action);
							}else {
								validActionCount++;
								break;
							}
						}
					}
				}
			}
			this.isValid = isValid;
		}else {
			this.isValid = true;
		}
		this.invalidItems = {
			invalidGoals: invalidItems.filter(function(item) {return item.javatype.indexOf('Goal') > -1}),
			invalidObjectives: invalidItems.filter(function(item) {return item.javatype.indexOf('Objective') > -1}),
			invalidActions: invalidItems.filter(function(item) {return item.javatype.indexOf('Action') > -1}),
			invalidItems: invalidItems
		};
		console.log(this.is, 'validate, invalidItems=', this.invalidItems);
		return isValid;
	}

	continueToReview(evt) {
		location.href = '#/eval/verifyeval/' + this.evaluation.review.revid + '/0';
	}

}

CepPlanning.register();
