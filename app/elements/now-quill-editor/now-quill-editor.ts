/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../../bower_components/DefinitelyTyped/quill/quill.d.ts"/>

@component('now-quill-editor')
class NowQuillEditor extends polymer.Base {
	@property({
		type: String
	})
	value:string;

	@property({
		type: Object
	})
	quill:any;

	@property({
		type: String
	})
	placeHolder:string;

	attached() {
		var toolbarControls = [
			[{'header': [1, 2, 3, 4, 5, 6, false]}, {'align': []}],
			['bold', 'italic', 'underline', 'strike'],
			[{'color': []}, {'background': []}],
			[{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
			['link'],
			['save']
		];
		var that = this;
		this.quill = new Quill(this.$.editor, {
			'modules': {
				'toolbar': {
					container: toolbarControls,
					handlers: {
						'save': function() {
							var editor = that.querySelector('.ql-editor');
							var html = editor.innerHTML.replace(/\>\s+\</g, '>&nbsp;<');
							that.fire('now-quill-editor-save', html);
						}
					}
				},
				'history': {
					delay: 2000,
					maxStack: 500,
					userOnly: true
				}
			},
			'placeholder': that.placeHolder,
			'theme': 'snow'
		});
	}

	@observe('value')
	private _onValueChange(newVal) {
		this.quill.pasteHTML(newVal);
	}
}

NowQuillEditor.register();
