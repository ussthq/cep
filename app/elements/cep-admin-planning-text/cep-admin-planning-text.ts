/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-planning-text')
class CepAdminPlanningText extends polymer.Base {

	@property({
		type: String
	})
	value:string;

	@observe('value')
	private _onValueChange(newVal) {
		if (newVal && newVal.intro) {
			this.$.planningEditor.value = newVal.intro;
		}
	}

	@listen('now-quill-editor-save')
	private _onSave(evt, detail) {
		if (detail) {
			var adminPage = document.querySelector('cep-admin-page') as CepAdminPage;
			var ajax = this.$.planningIntroAjax;
			ajax.params = {
				id: adminPage.selectedYear,
				javatype: 'org.salvationarmy.cep.model.PlanningIntro'
			};
			ajax.body = {
				year: adminPage.selectedYear,
				intro: detail,
				javatype: 'org.salvationarmy.cep.model.PlanningIntro'
			};
			ajax.generateRequest();
		}
	}

}

CepAdminPlanningText.register();
