/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-confirm-dialog')
class CepConfirmDialog extends polymer.Base {
	/**
	 * The title of the dialog
	 * @type {String}
	 */
	@property({
		type: String
	})
	dialogTitle:string;
	/**
	 * The text of the dialog
	 * @type {String}
	 */
	@property({
		type: String
	})
	dialogText:string;
	/**
	 * The text which shows in the confirm button
	 * @type {String}
	 */
	@property({
		type: String,
		value: 'OK'
	})
	confirmButtonText:string;
	/**
	 * The text which shows in the cancel button
	 * @type {Stromg}
	 */
	@property({
		type: String,
		value: 'Cancel'
	})
	cancelButtonText:string;
	/**
	 * The hex color of the confirm button background
	 * @type {String}
	 */
	@property({
		type: String,
		observer: '_onConfirmBackgroundChange'
	})
	confirmButtonBackground:string;
	/**
	 * The hex color of the confirm button text
	 * @type {String}
	 */
	@property({
		type: String,
		observer: '_onConfirmColorChange'
	})
	confirmButtonColor:string;
	/**
	 * The hex color of the cancel button background
	 * @type {String}
	 */
	@property({
		type: String,
		observer: '_onCancelBackgroundChange'
	})
	cancelButtonBackground:string;
	/**
	 * The hex color of the cancel button text
	 * @type {String}
	 */
	@property({
		type: String,
		observer: '_onCancelColorChange'
	})
	cancelButtonColor:string;
	/**
	 * The function which runs when the dialog is cancelled
	 * @type {Function}
	 */
	@property({
		type: Object
	})
	_cancelCallback:any;
	/**
	 * The function which runs when the dialog is confirmed
	 * @type {Function}
	 */
	@property({
		type: Object
	})
	_confirmCallback:any;

	@property({
		type: Boolean,
		value: false,
		reflectToAttribute: true
	})
	noButtons: boolean;

	@property({
		type: Boolean,
		value: false,
		reflectToAttribute: true
	})
	opened: boolean;
	/**
	 * Fired when the confirmButtonBackground changes. Sets the custom style variable --confirm-button-background
	 * @param  {String} newVal The new color
	 * @param  {String} oldVal The old color
	 */
	private _onConfirmBackgroundChange(newVal, oldVal) {
		if (newVal) {
			this.customStyle['--confirm-button-background'] = newVal;
			this.updateStyles();
		}
	}
	/**
	 * Fired when the confirmButtonColor changes. Sets the custom style variable --confirm-button-color
	 * @param  {String} newVal The new color
	 * @param  {String} oldVal The old color
	 */
	private _onConfirmColorChange(newVal, oldVal) {
		if (newVal) {
			this.customStyle['--conform-button-color'] = newVal;
			this.updateStyles();
		}
	}
	/**
	 * Fired when the cancelButtonBackground changes. Sets the custom style variable --cancel-button-background
	 * @param  {String} newVal The new color
	 * @param  {String} oldVal The old color
	 */
	private _onCancelBackgroundChange(newVal, oldVal) {
		if (newVal) {
			this.customStyle['--cancel-button-background'] = newVal;
			this.updateStyles();
		}
	}
	/**
	 * Fired when the cancelButtonColor changes. Sets the custom style variable --cancel-button-color
	 * @param  {String} newVal The new color
	 * @param  {String} oldVal The old color
	 * @return {[type]}        [description]
	 */
	private _onCancelColorChange(newVal, oldVal) {
		if (newVal) {
			this.customStyle['--cancel-button-color'] = newVal;
			this.updateStyles();
		}
	}
	/**
	 * Fired when the dialog closes. Fires dig-confirm-canceled and dig-confirm-confirmed. Also
	 * runs any cancelCallback or confirmCallback methods
	 * @param  {Event} evt    The event object
	 * @param  {Object} detail The detail object
	 */
	@listen('dialog.iron-overlay-closed')
	_onDialogClosed(evt, detail) {
		console.log('confirm-dialog._onDialogClosed', arguments);
		if (detail.canceled || !detail.confirmed) {
			if (this._cancelCallback) {
				this._cancelCallback.call(this);
			}
			this.fire('dig-confirm-canceled');
		}else if (detail.confirmed) {
			if (this._confirmCallback) {
				this._confirmCallback.call(this);
			}
			this.fire('dig-confirm-confirmed');
		}
	}
	/**
	 * Opens the dialog and sets the cancel and confirm callbacks
	 * @param  {Function} confirmCallback function to run when the dialog is confirmed
	 * @param  {Function} cancelCallback function to run when the dialog is cancelled
	 */
	open(confirmCallback, cancelCallback) {
		this.$.dialog.open();
		this.opened = true;
		if (cancelCallback) {
			this._cancelCallback = cancelCallback;
		}
		if (confirmCallback) {
			this._confirmCallback = confirmCallback;
		}
	}
	/**
	 * Closes the dialog
	 */
	close() {
		this.$.dialog.close();
		this.opened = false;
	}

	patchOverlay(evt, detail) {
		if (evt.target.withBackdrop) {
			evt.target.parentNode.insertBefore(evt.target.backdropElement, evt.target);
		}
	}
}

CepConfirmDialog.register();
