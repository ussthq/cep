/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-print-page')
class CepPrintPage extends polymer.Base {

	@property({
		type: Object
	})
	review:any;

	@property({
		type: Object,
		notify: true
	})
	settings:any;

	@property({
		type: Object,
		notify: true
	})
	user:any;

	@computed()
	settingsUrl(route) {
		var url = '/thq/cep2.nsf/scripts/settings.json';
		var port = location.port;
		// Setup for running local
		if (port && port !== '80' && port !== '443') {
			url = '../../scripts/settings.json';
		}
		return url;
	}

	@observe('routeData.revid')
	private _onReviewIdChange(revid) {
		console.log(this.is, '_onReviewIdChange', arguments);
		var ajax = this.$.printReviewAjax;
		ajax.params = {
			reviewid: revid
		};
		ajax.generateRequest();
	}

	private getUser() {
		this.$.userAjax.generateRequest();
	}
}

CepPrintPage.register();
