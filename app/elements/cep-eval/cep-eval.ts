/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../now-animated-behavior/now-animated-behavior.ts" />
/// <reference path="../now-scroll-behavior/now-scroll-behavior.ts" />
/// <reference path="../../bower_components/DefinitelyTyped/es6-promise/es6-promise.d.ts" />

@component('cep-eval')
@behavior(NowBehaviors.AnimatedPagesBehavior)
@behavior(NowBehaviors.ScrollBehavior)
class CepEval extends polymer.Base implements NowBehaviors.AnimatedPagesBehavior,
	NowBehaviors.ScrollBehavior {
	/**
	 * The evaluation
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	evaluation:any;
	/**
	 * True if an iron-ajax is loading data
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	user:any;
	/**
	 * The index defined in the route
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	evalIndexRoute:any;
	/**
	 * The selected year
	 * @type {Number}
	 */
	@property({
		type: Number,
		notify: true
	})
	selectedYear:number;
	/**
	 * The neon animated pages element
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: function() {
			return this.$.essentialMinistryPages;
		}
	})
	neonPagesElement:any;
	/**
	 * The attr-for-selected property
	 * @type {String}
	 */
	@property({
		type: String,
		value: 'page'
	})
	selectedPageProperty:string;
	/**
	 * The animation type for the neon-animated-pages element
	 * @type {String}
	 */
	@property({
		type: String,
		value: 'slideHorizontal'
	})
	animationType:string;
	/**
	 * Listener for the neon-animation-finished event. Will scroll the page to the top
	 */
	@listen('essentialMinistryPages.neon-animation-finish')
	private _onAnimationComplete() {
		//document.querySelector('#content').scrollTop = 0;
		var content = document.querySelector('#content')
		this.smoothScroll(content, 0, 250);
	}
	/**
	 * The listener for when the next button is clicked to navigate between pages
	 * @param {Event} evt The event object
	 */
	@listen('move-next-eval-page')
	private _onMoveNextPage(evt) {
		this.transitionNext();
	}
	/**
	 * The listener for when the back button is clicked to navigate between pages
	 * @param {Event} evt The event object
	 */
	@listen('move-prev-eval-page')
	private _onMovePrevPage(evt) {
		this.transitionPrev();
	}
	/**
	 * Fired when the route prefix changes. Set the selected eval page to 0 if it's not declared in the route
	 * @param {String} prefix The route prefix
	 */
	@observe('evalIndexRoute.prefix')
	private _onEvalIndexRouteChange(prefix) {
		if (!this.evalIndexRoute.path) {
			this.set('evalIndexRoute.path', '/0');
		}
	}
	/**
	 * Stand-in methods for AnimatedPagesBehavior
	 */
	_getCurrentPage: () => string;
	_getAvailablePages: () => string;
	_getPageIndex: (pageName:string) => string;
	_getTransitionType: (currentPageIndex:number, destinationPageIndex:number, transitionContext:string) => string;
	setTransitionType: (destinationPage:string) => void;
	transitionToPage: (destinationPage:string) => void;
	transitionNext: () => void;
	transitionPrev: () => void;
	smoothScroll: (element:any, target:number, duration:number) => Promise<any>;
}

CepEval.register();