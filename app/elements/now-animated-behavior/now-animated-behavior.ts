/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
namespace NowBehaviors {
	export class AnimatedPagesBehavior extends polymer.Base {
		/**
		 * The neon-animated-pages this behavior should act upon
		 * @type {Element | undefined}
		 */
		@property({type: Object})
		neonPagesElement:any;
		/**
		 * The attribute on the neonPagesElement children which is the attrForSelected
		 * @type {String}
		 */
		@property({type: String, value: 'data-route'})
		selectedPageProperty:string;
		/**
		 * The type of animations. Valid values are slideHorizontal, slideVertical, scale
		 * @type {String}
		 */
		@property({type: String, value: 'slideHorizontal'})
		animationType:string;

		/**
		 * Get the currently selected page
		 * @return {Element} [description]
		 */
		_getCurrentPage() {
			var returnVal = null;
			if (this.neonPagesElement) {
				returnVal = this.neonPagesElement.selected;
			}
			return returnVal;
		}

		/**
		 * Get all of the pages inside the neonPagesElement
		 * @return {Array} The array of elements which are child pages
		 */
		_getAvailablePages() {
			var returnVal = null;
			if (this.neonPagesElement) {
				var children = Polymer.dom(this.neonPagesElement).children;
				returnVal = [];
				for (let i = 0; i < children.length; i++) {
					var node = children[i];
					if (node.localName === 'neon-animatable') {
						returnVal.push(node);
					}
				}
			}
			return returnVal;
		}

		/**
		 * Get the index of the pagename
		 * @param  {String} pageName The name of the page
		 * @return {Number}          The index of the element for pageName
		 */
		_getPageIndex(pageName) {
			var returnVal = null;
			var availPages = this._getAvailablePages();
			if (this.neonPagesElement && availPages.length > 0) {
				for (var i = 0; i < availPages.length; i++) {
					var page = availPages[i];
					var selPageName = page.getAttribute(this.selectedPageProperty);
					if (!selPageName && selPageName != 0) {
						selPageName = page[this.selectedPageProperty];
					}
					if (selPageName == pageName) {
						returnVal = i;
						break;
					}
				}
			}
			return returnVal;
		}

		/**
		 * Get the animation type which will be added to the neonPagesElement's entry/exit-animation. This
		 * is based on the order of the pages within the neonPagesElement.
		 * @param  {Number} currentPageIndex     The index of the current page
		 * @param  {Number} destinationPageIndex The index of the destination page
		 * @param  {String} transitionContext    The context for the animation. Valid values are 'exit' and 'entry'
		 * @return {String}                      The animation name based on the parameters
		 */
		_getTransitionType(currentPageIndex, destinationPageIndex, transitionContext) {
			var returnVal = 'scale-up-animation';
			if (currentPageIndex < destinationPageIndex) {
				if (this.animationType === 'none') {
					returnVal = null;
				} else if (this.animationType === 'fade') {
					if (transitionContext === 'exit') {
						returnVal = 'fade-out-animation';
					} else if (transitionContext === 'entry') {
						returnVal = 'fade-in-animation';
					}
				} else if (this.animationType === 'slideHorizontal') {
					if (transitionContext === 'exit') {
						returnVal = 'slide-left-animation';
					} else if (transitionContext === 'entry') {
						returnVal = 'slide-from-right-animation';
					}
				} else if (this.animationType === 'slideVertical') {
					if (transitionContext === 'exit') {
						returnVal = 'scale-down-animation';
					} else if (transitionContext === 'entry') {
						returnVal = 'slide-from-bottom-animation';
					}
				} else if (this.animationType === 'scale') {
					if (transitionContext === 'exit') {
						returnVal = 'fade-out-animation';
					} else if (transitionContext === 'entry') {
						returnVal = 'scale-up-animation';
					}
				}
			} else if (currentPageIndex > destinationPageIndex) {
				if (this.animationType === 'none') {
					returnVal = null;
				} else if (this.animationType === 'fade') {
					if (transitionContext === 'exit') {
						returnVal = 'fade-in-animation';
					} else if (transitionContext === 'entry') {
						returnVal = 'fade-out-animation';
					}
				} else if (this.animationType === 'slideHorizontal') {
					if (transitionContext === 'exit') {
						returnVal = 'slide-right-animation';
					} else if (transitionContext === 'entry') {
						returnVal = 'slide-from-left-animation';
					}
				} else if (this.animationType === 'slideVertical') {
					if (transitionContext === 'exit') {
						returnVal = 'scale-down-animation';
					} else if (transitionContext === 'entry') {
						returnVal = 'scale-up-animation';
					}
				} else if (this.animationType === 'scale') {
					if (transitionContext === 'exit') {
						returnVal = 'fade-out-animation';
					} else if (transitionContext === 'entry') {
						returnVal = 'scale-up-animation';
					}
				}
			}
			return returnVal;
		}

		/**
		 * Set the entry-animation and exit-animation to the proper animation type
		 * @param {String} destinationPage The destination page name
		 */
		setTransitionType(destinationPage) {
			if (this.neonPagesElement) {
				var currentPageIndex = this._getPageIndex(this._getCurrentPage());
				var destinationPageIndex = this._getPageIndex(destinationPage);
				var exitTransition = this._getTransitionType(currentPageIndex, destinationPageIndex, 'exit');
				var entryTransition = this._getTransitionType(currentPageIndex, destinationPageIndex, 'entry');
				this.neonPagesElement.set('exitAnimation', exitTransition);
				this.neonPagesElement.set('entryAnimation', entryTransition);
			} else {
				console.warn('DigAnimatedPagesBehavior.setTransitionType for ' + destinationPage + '. No neonPagesElement defined!');
			}
		}

		/**
		 * Actually do the transition to the destination page
		 * @param  {String} destinationPage The destination page name
		 */
		transitionToPage(destinationPage) {
			if (destinationPage && this.neonPagesElement) {
				this.setTransitionType(destinationPage);
				this.neonPagesElement.select(destinationPage);
			} else {
				var msg = 'DigAnimatedPagesBehavior.transition.';
				if (!destinationPage && !this.neonPagesElement) {
					msg += ' No destination page or neonPagesElement are defined!';
				} else if (!destinationPage) {
					msg += ' No destination page is defined!';
				} else if (!this.neonPagesElement) {
					msg += ' No neonPagesElement is defined!';
				}
				console.warn(msg);
			}
		}

		/**
		 * Transition to the next page
		 */
		transitionNext() {
			var currentPageIdx = this._getPageIndex(this._getCurrentPage());
			var nextPage = this._getAvailablePages()[currentPageIdx + 1];
			var nextPageName = nextPage[this.selectedPageProperty].toString();
			this.transitionToPage(nextPageName);
		}

		/**
		 * Transition to the previous page
		 */
		transitionPrev() {
			var currentPageIdx = this._getPageIndex(this._getCurrentPage());
			var nextPage = this._getAvailablePages()[currentPageIdx - 1];
			var nextPageName = nextPage[this.selectedPageProperty].toString();
			this.transitionToPage(nextPageName);
		}
	};
}

