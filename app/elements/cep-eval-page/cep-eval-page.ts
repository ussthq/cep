/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../now-validation-behavior/now-validation-behavior.ts" />
/// <reference path="../now-scroll-behavior/now-scroll-behavior.ts" />
/// <reference path="../../bower_components/DefinitelyTyped/es6-promise/es6-promise.d.ts" />

@component('cep-eval-page')
@behavior(NowBehaviors.ValidationBehavior)
@behavior(NowBehaviors.ScrollBehavior)
class CepEvalPage extends polymer.Base implements NowBehaviors.ValidationBehavior,
	NowBehaviors.ScrollBehavior {
	/**
	 * The actual review
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	evaluation:any;
	/**
	 * Loading boolean for the spinner
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;
	/**
	 * Loading boolean for the evaluation
	 * @type {Boolean}
	 */
	@property({
		type: Boolean
	})
	evalLoading:boolean;
	/**
	 * The array of year objects for the user's locationid
	 */
	@property({
		type: Array,
		observer: '_onYearsFetched'
	})
	locationYears:any;
	/**
	 * The selected year
	 * @type {Number}
	 */
	@property({
		type: Number,
		notify: true,
		observer: '_onSelectedYearChange'
	})
	selectedYear:number;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object,
		observer: '_onUserChange'
	})
	user:any;
	/**
	 * The route object from app-location
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	route:any;
	/**
	 * The sub route. Determines the selected tab and page
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	tabRoute:any;
	/**
	 * The review id for the selected year
	 * @type {Number}
	 */
	@property({
		type: Number
	})
	reviewId:number;
	/**
	 * True if the eval route is active
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	tabRouteActive:boolean;
	/**
	 * The data from the tabRoute
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	tabData:any;
	/**
	 * Holder property populated when the review is fetched. This allows
	 * the selectedYear to be selected once the locationYears are fetched
	 * @type {Number}
	 */
	@property({
		type: Number
	})
	_reviewYear:number;
	/**
	 * The active tab
	 * @type {String}
	 */
	@property({
		type: String
	})
	tab:string;
	/**
	 * The settings object
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {},
		notify: true
	})
	settings:any;
	/**
	 * True if the planning tab is disabled
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		value: false
	})
	isPlanningDisabled:boolean;

	attached() {
		document.addEventListener('open-print-page', function(evt) {
			var url = 'print.html#/' + this.evaluation.review.revid;
			window.open(url, '_blank');
		}.bind(this));
	}

	@listen('reviewPages.neon-animation-finish')
	private _onAnimationComplete() {
		var content = document.querySelector('#content');
		this.smoothScroll(content, 0, 250);
	}

	@observe('evaluation.review.*')
	_isPlanningDisabled(newVal) {
		// console.log(this.is, '_isPlanningDisabled', arguments);
		if (newVal) {
			this.async(function() {
				this.set('isPlanningDisabled', !this.isAllEmsValid());
			}.bind(this), 500);
		}
	}
	/**
	 * Observer of mainLoading. The issue is that the eval continues to load
	 * even after mainLoading is set to false by the first request. We need to
	 * ensure mainLoading keeps the spinner going as long as a request is in flight
	 * @param {Boolean} newVal
	 */
	@observe('mainLoading')
	private _onMainLoadingChange(newVal) {
		if (this.$.evalAjax.loading && !newVal) {
			this.set('mainLoading', this.$.evalAjax.loading);
		}
	}
	/**
	 * Observer for evalLoading, also to maintain mainLoading
	 * @param {Boolean} newVal
	 */
	@observe('evalLoading')
	private _onEvalLoadingChange(newVal) {
		this.set('mainLoading', newVal);
	}
	/**
	 * Fired when a tab is changed
	 * @param {Object} 'tabData.tab' The tabData.tab property from app-route
	 */
	@observe('tab')
	_onTabChanged(newVal) {
		// console.log(this.is, '_onTabChanged', arguments);
		if (newVal) {
			var reviewId = this.reviewId ? '/' + this.reviewId : '';
			this.set('route.path', '/eval/' + newVal + reviewId);
		}
	}
	/**
	 * Look for a change in the route to set the selected tab
	 * @param {String} 'tabData.tab' The tab to select
	 */
	@observe('tabData.tab')
	private _onTabDataTabChange(newVal) {
		// console.log(this.is, '_onTabDataTabChange', arguments);
		this.set('tab', newVal);
	}
	/**
	 * Fired when the review id in the route changes
	 * @param {[type]} newVal [description]
	 * @param {[type]} oldVal [description]
	 */
	@observe('tabData.reviewId, user')
	private _onTabDataReviewIdChange(newVal, user) {
		// console.log(this.is, '_onTabDataReviewIdChange', arguments);
		if (newVal && user) {
			var roles = user.roles;
			if (roles.indexOf('[Admin]') === -1 && roles.indexOf('[DHQ]') === -1) { // A User
				var yearObjs = this.locationYears;
				if (yearObjs) {
					var yearObj = yearObjs.find(function(year) {
						return year.revid == newVal;
					});
					if (!yearObj) {
						// if a user, shouldn't be requesting anything that isn't in the locationYears
						// console.log(this.is, '_onTabDataReviewIdChange, throwing not auth');
						this.$.notAuthDialog.open();
					}else {
						// console.log(this.is, '_onTabDataReviewIdChange, changing reviewId to ', newVal);
						this.reviewId = newVal;
					}
				}else {
					// console.log(this.is, '_onTabDataReviewIdChange, changing reviewId to ', newVal);
					this.reviewId = newVal;
				}
			}else {
				// console.log(this.is, '_onTabDataReviewIdChange, changing reviewId to ', newVal);
				this.reviewId = newVal;
			}
		}
	}
	/**
	 * Listens for an update to the Essential Ministry and does a POST to sync with server
	 * @param {Event} 'evt' The event object
	 * @param {Object} detail The item that was updated
	 * @event em-item-updated
	 */
	@listen('em-item-updated')
	private _onEmItemUpdated(evt, detail) {
		//console.log(this.is, '_onEmItemUpdated', arguments);
		if (detail) {
			var id = this._getId(detail);
			var ajax = null;
			if (id) {
				ajax = this.$.evalPutAjax;
			}else {
				ajax = this.$.evalPostAjax;
			}
			ajax.params = {
				id: id || 'undefined',
				javatype: detail.javatype
			};
			ajax.body = detail;
			ajax.generateRequest();
		}
	}
	/**
	 * Fired from the onResponse from the PUT and POST iron-ajax elements whose requests
	 * are generated by an em-item-updated event
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 * @event em-item-update-complete
	 */
	private _emItemUpdated(evt, detail) {
		var detailObj = detail.response;
		this.fire('em-item-update-complete', detailObj);
	}
	/**
	 * Fired when the user clicks the notAuthDialog ok button. Peforms a back or
	 * closes the window
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onAuthConfirm(evt, detail) {
		var roles = this.user.roles;
		if (roles.indexOf('[Admin]]') === -1 && roles.indexOf('[DHQ]') === -1) {
			window.history.back();
			this.$.notAuthDialog.close();
		}else if (roles.indexOf('[Admin]') === -1 && roles.indexOf('[DHQ]') > -1) {
			window.close();
		}else {
			this.$.notAuthDialog.close();
		}
	}
	/**
	 * Fired when the reviewId changes and will trigger a request for the review
	 * @param {Number} newVal The new review id
	 * @param {Number} oldVal The old review id
	 */
	@observe('reviewId, user')
	private _onReviewIdChange(newVal, user) {
		// console.log(this.is, '_onReviewIdChange', arguments);
		var review = this.evaluation;
		if (!review || review && review.review.revid !== newVal) {
			var reviewAjax = this.$.evalAjax;
			if (!reviewAjax.loading) {
				if (newVal && this.tabRoute.prefix === '/eval') {
					var params = {
						reviewid: newVal
					};
					this._fetchReview(params);
					this.tab = this.tab || 'introandlinks';
					this.set('route.path', '/eval/' + this.tab + '/' + newVal);
				}
			}else {
				if (review) {
					this.set('selectedYear', review.review.year);
				}
			}
		}
	}
	/**
	 * Fetch the review
	 * @param {params} params The ajax parameters for the fetch
	 */
	private _fetchReview(params) {
		var ajax = this.$.evalAjax;
		if (params && !ajax.loading) {
			ajax.params = params;
			ajax.generateRequest();
		}
	}
	/**
	 * Fired once the review has been fetched. If the fetch was due to a route
	 * then set the selected year, otherwise do nothing
	 * @param {Event} evt The event object
	 */
	private _onEvaluationFetch(evt) {
		// console.log(this.is, '_onEvaluationFetch', arguments);
		if (this.tabRouteActive) {
			var response = evt.detail.response;
			if (response) {
				var review = response.review;
				if (this.user && this.user.roles.indexOf('[DHQ]') > -1 && this.user.roles.indexOf('[Admin]') === -1) {
					if (review.divid !== this.user.divid) {
						// console.log(this.is, '_onEvaluationFetch, throwing not auth dialog');
						this.$.notAuthDialog.open();
					}
				}
				this.fire('now-app-update-header', this.settings.DEFAULT_HEADER + ': ' + response.review.corpname);
				this._reviewYear = response.review.year;
				if (this.selectedYear != this._reviewYear) {
					this.set('selectedYear', this._reviewYear.toString());
				}
				if (!this.locationYears || (response.review.locationid !== this.user.locationid)) {
					this._fetchLocationYears(response.review.locationid);
				}else if (!this.selectedYear) {
					this.selectedYear = response.review.year;
				}
			}
		}
	}
	/**
	 * Fired when the user is fetched. Fires an ajax request to get the year list
	 * for the locationid in the suer object
	 * @param {Object} newVal The new user object
	 * @param {Object} oldVal The old user object
	 */
	private _onUserChange(newVal, oldVal) {
		if (newVal && this.tabRoute && this.tabRoute.prefix === '/eval') {
			var evalAjax = this.$.evalAjax;
			if (!evalAjax.loading && (!this.tabData || !this.tabData.reviewId)) {
				this._fetchLocationYears(newVal.locationid);
			}
		}
	}
	/**
	 * Fetch the years for the passed in locationId
	 * @param {Number} locationId The location id to fetch the years for
	 */
	private _fetchLocationYears(locationId) {
		if (locationId) {
			var ajax = this.$.yearAjax;
			ajax.params = {
				method: 'getLocationyearList',
				locationid: locationId
			};
			ajax.generateRequest().completes
				.then((ironRequest) => {
					/**
					 * We want to check to ensure we got years. If we didn't this usually indicates
					 * that the currently logged in user has a location ID that was not found in the
					 * review table. This could be because the user has the wrong locationSID in their
					 * person document in the domino directory OR that a review as never initiated for
					 * their location.
					 */
					let response = ironRequest.response;
					let isDhqOrAdmin = this.user.roles.indexOf('[DHQ]') > -1 || this.user.roles.indexOf('[Admin]') > -1 || this.user.roles.indexOf('[THQ]') > -1;
					if (response && response.length === 0 && isDhqOrAdmin) {
						let detailObj = {
							dialogTitle: 'No reviews for your Location ID!',
							dialogText: 'Your location ID (' + this.user.locationid + ') was not found which prevents us from locating your review. However, since you have permission to see other reviews please use the Admin site to navigate to a review.'
						}
						this.fire('open-confirm-dialog', detailObj);
					} else if (response && response.length === 0 && !isDhqOrAdmin) {
						let detailObj = {
							dialogTitle: 'No review for your Location ID!',
							dialogText: 'No reviews found for your location ID (' + this.user.locationid + ') which prevents us from locating your review. Please contact THQ IT.'
						}
						this.fire('open-confirm-dialog', detailObj);
					}
				});
		}
	}
	/**
	 * Once the years for a specific location have been fetched, set the selectedYear
	 * to the proper year
	 * @param {Array} response Array of years from server
	 */
	private _onYearsFetched(response) {
		// console.log(this.is, '_onYearsFetched', arguments);
		if (!this.selectedYear && !this.tabRouteActive) {
			var reviewYr = this._getReviewYear();
			for (let i = 0; i < response.length; i++) {
				var locYear = response[i];
				if (locYear.year == reviewYr) {
					this.set('selectedYear', locYear.year);
					break;
				}
			}
			if (!this.selectedYear) {
				var showLocYear = response.find(function(yrObj) {
					return yrObj.year === (reviewYr - 1);
				});
				this.set('selectedYear', showLocYear && showLocYear.year ? showLocYear.year : null);
			}
		}else if (!this.selectedYear && this.tabRouteActive) {
			this.set('selectedYear', this._reviewYear);
		}
	}
	/**
	 * Fired when the selectedYear changes and does a fetch for the review
	 * @param {Number} newVal The selected year
	 * @param {Number} oldVal The old selected year
	 */
	private _onSelectedYearChange(newVal, oldVal) {
		// console.log(this.is, '_onSelectedYearChange', arguments);
		if (newVal && this.locationYears && this.locationYears.length > 0) {
			var revId = this._getReviewIdByYear(newVal);
			if (revId !== this.reviewId) {
				// console.log(this.is, '_onSelectedYearChange, changing reviewId to', revId);
				this.reviewId = revId;
			}
		}
	}
	/**
	 * Get the review id based on the year passed
	 * @param {[type]} year [description]
	 */
	private _getReviewIdByYear(year) {
		var returnVal = null;
		var locYears = this.locationYears;
		for (let i = 0; i < locYears.length; i++) {
			var locYear = locYears[i];
			if (locYear.year == year) {
				returnVal = locYear.revid;
				break;
			}
		}
		return returnVal;
	}
	/**
	 * Get the current review year based on the month and current year
	 */
	private _getReviewYear() {
		var dte = new Date();
		var yr = dte.getFullYear();
		if (!this.selectedYear) {
			var month = dte.getMonth();
			if (month >= 9) {
				yr = yr + 1;
			}
		}else {
			yr = this.selectedYear;
		}
		return yr;
	}
	/**
	 * Helper function for getting the ID for a PUT to the server.
	 * @param {Object} detail The item being updated
	 */
	private _getId(detail) {
		var javaType = detail.javatype;
		if (javaType.indexOf('EMSubCategory') > -1) {
			return detail.emscid;
		}else if (javaType.indexOf('EssentialMinistry') > -1) {
			return detail.emid;
		}else if (javaType.indexOf('ImpactQuestion') > -1) {
			return detail.iqid;
		}else if (javaType.indexOf('Objective') > -1) {
			return detail.objid;
		}else if (javaType.indexOf('Goal') > -1) {
			return detail.goalid;
		}else if (javaType.indexOf('Action') > -1) {
			return detail.actid;
		}else if (javaType.indexOf('Review') > -1) {
			return detail.revid;
		}
	}
	/**
	 * Force a sign out
	 */
	private _signOut() {
		location.href = '/names.nsf?logout&redirectto=' + location.href;
	}
	/**
	 * Stand-in method for ValidationBehavior
	 */
	_getAllEmPages: () => any;
	getInvalidEms: () => any;
	getAllEmValidationItems: () => any;
	isAllEmsValid: () => boolean;
	getAllValidationItems: (review:any) => any;
	isEmValid: (essentialMinistry:any, locked:any) => any;
	isReviewValid: (evaluation:any) => boolean;
	_getInvalidCount: (validationItemsArr:any) => number;
	smoothScroll: (element:any, target:number, duration:number) => Promise<any>;
}

CepEvalPage.register();
