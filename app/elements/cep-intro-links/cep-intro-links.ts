/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-intro-links')
class CepIntroLinks extends polymer.Base {
	@property({
		type: Object,
		notify: true
	})
	eval:any;

	@property({
		type: Object
	})
	intro:any;

	@property({
		type: Number,
		observer: '_onSelectedYearChange'
	})
	selectedYear:number;

	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;

	private _onSelectedYearChange(newVal, oldVal) {
		if (newVal) {
			var ajax = this.$.introAjax;
			ajax.params = {
				id: newVal,
				javatype: 'org.salvationarmy.cep.model.IntroAndLinks'
			};
			ajax.generateRequest();
		}
	}

	private _onEvalChange(newVal, oldVal) {
		console.log(this.is, '_onEvalChange', arguments);
	}
}

CepIntroLinks.register();