/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

declare var saveAs;

@component('cep-planning-action')
class CepPlanningAction extends polymer.Base {

	@property({
		type: Object,
		notify: true
	})
	goal:any;

	@property({
		type: Object,
		notify: true
	})
	objective:any;

	@property({
		type: Object,
		notify: true
	})
	action:any;

	@property({
		type: Object,
		notify: true
	})
	essentialMinistry:any;

	@property({
		type: Boolean
	})
	locked:boolean;

	@property({
		type: Boolean,
		value: true,
		notify: true
	})
	hidePersonReset:boolean;

	@property({
		type: Boolean,
		value: true,
		notify: true
	})
	hideStepReset:boolean;

	@property({
		type: Boolean,
		value: true,
		notify: true
	})
	hideDueDateReset:boolean;

	@property({
		type: Object,
		notify: true
	})
	evaluation:any;

	@observe('action.personresponsible')
	private _checkPersonResponsible(personresponsible) {
		if (!personresponsible) {
			this.hidePersonReset = true;
		} else {
			this.hidePersonReset = false;
		}
	}

	@observe('action.step')
	private _checkStep(step) {
		if (!step) {
			this.hideStepReset = true;
		} else {
			this.hideStepReset = false;
		}
	}

	@observe('action.duedate')
	private _checkDueDate(duedate) {
		if (!duedate) {
			this.hideDueDateReset = true;
		} else {
			this.hideDueDateReset = false;
		}
	}

	private _resetInput(e) {
		e.target.parentNode.previousElementSibling.value = '';
		this.action.personresponsible = this.$.personresponsible.value;
		this.action.step = this.$.step.value;
		this.action.due = this.$.duedate.value;
		this._onActionChange();
	}

	private _iCal() {
		if (this.$.duedate.value != '') {
			let desc = this.objective.objectivename;
			let duedate = moment(new Date(this.$.duedate.value).toISOString()).format('YYYYMMDDTHHmmss');
			let title = this.$.step.value;
			let datestamp = moment(new Date().toISOString()).format('YYYYMMDDTHHmmss');
			let todo =
`BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Salvation Army//CEP//EN
BEGIN:VTODO
DTSTAMP;VALUE=DATE:${datestamp}
UID:${datestamp}-${duedate}@cep
DUE;VALUE=DATE:${duedate}
SUMMARY: ${title}
DESCRIPTION: ${desc}
CATEGORIES:CEP
STATUS:NEEDS-ACTION
END:VTODO
END:VCALENDAR`;
			let filename = 'todo-'+(this.goal.idx + 1)+'-objective-'+(this.objective.idx + 1);
			let blob = new Blob([todo], {type: "text/calendar;charset=utf-8"});
			saveAs(blob, filename+".ics");
		} else {
			this.$.noDueDateDialog.open();
		}

	}

	/**
	 * Fired when a value in this action changes and triggers
	 * the server update
	 */
	private _onActionChange() {
		//console.log(this.is, '_onActionChange', arguments);
		this.fire('em-item-updated', this.action);
	}
	/**
	 * Open the date picker dialog
	 * @param {Event} evt    The event object
	 */
	private _openDialog(evt) {
		this.$.datePicker.date = new Date(this.action.duedate);
		this.$.datePickerDialog.open();
	}
	/**
	 * Fired when the dialog closes. Update the duedate if the dialog
	 * @param {[type]} evt    [description]
	 * @param {[type]} detail [description]
	 */
	private _dismissDialog(evt, detail) {
		if (evt.detail.confirmed) {
			var selDate = this.$.datePicker.date;
			var formattedDate = this.$.datePicker.dateFormat(selDate, 'MMM DD, YYYY');
			if (formattedDate !== this.action.duedate) {
				this.set('action.duedate', formattedDate);
				this._onActionChange();
			}
		}
	}
	/**
	* Patch to fix backdrop z-index issue with paper dialog modal
	*/
	private _patchOverlay(e) {
	  if (e.target.withBackdrop) {
	    e.target.parentNode.insertBefore(e.target.backdropElement, e.target);
	  }
	}
	/**
	 * Check if this action is valid
	 * @param {Object} action The action object
	 */
	isValid(personResp, step, dueDate) {
		var returnVal = false;
		if (this.action.personresponsible && this.action.duedate && this.action.step) {
			returnVal = true;
		}
		return returnVal;
	}
}

CepPlanningAction.register();
