/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-print-em')
class CepPrintEm extends polymer.Base {
	@property({
		type: Object
	})
	em:any;

	private _getGrowthScore(subCat) {
		return subCat.GrowthScore || subCat.GrowthScore === 0 ? subCat.GrowthScore : 0;
	}

	private _getOverallScore(em) {
		var overallScore = 0;
		var subCats = em.subCategories;
		var count = 0;
		for (var i = 0; i < subCats.length; i++) {
			count++;
			var ministryScore = this._getMinistryScore(subCats[i]);
			overallScore = overallScore + parseFloat(ministryScore);
		}
		overallScore = overallScore / count;
		return overallScore.toFixed(2);
	}

	private _getMinistryScore(subCat) {
		var scores = [];
		var memScore = subCat.MembershipScore;
		var standScore = subCat.StandardScore;
		var partScore = subCat.ParticipationScore;
		var growthScore = subCat.GrowthScore;
		var leadScore = parseInt(subCat.score, 10);
		if (memScore > 0) {
			scores.push(memScore);
		}
		if (standScore > 0) {
			scores.push(standScore);
		}
		if (partScore > 0) {
			scores.push(partScore);
		}
		if (growthScore > 0) {
			scores.push(growthScore);
		}
		if (leadScore > 0) {
			scores.push(leadScore);
		}
		var ministryScore = 0;
		for (var i = 0; i < scores.length; i++) {
			var scoreItem = scores[i];
			ministryScore = ministryScore + scoreItem;
		}
		if (scores.length === 0) {
			ministryScore = 0.00;
		}else {
			ministryScore = (ministryScore / scores.length);
		}
		if (ministryScore !== subCat.MinistryScore) {
			subCat.MinistryScore = ministryScore;
		}
		return ministryScore.toFixed(2);
	}
}

CepPrintEm.register();
