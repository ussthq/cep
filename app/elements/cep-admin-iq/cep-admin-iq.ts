/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-iq')
class CepAdminIq extends polymer.Base {

	@property({
		type: Array
	})
	essentialMinistries:any;

	@property({
		type: Object
	})
	user:any;

	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;

	@listen('save-admin-item')
	_onSaveItem(evt, detail) {
		// console.log(this.is, '_onSaveItem', arguments);
		if (detail && detail.item) {
			var item = detail.item;
			var ajax = this.$.iqPutAjax;
			if (!item.iqid) {
				ajax = this.$.iqPostAjax;
			}
			if (item.javatype.indexOf('ImpactQuestion') > -1) {
				ajax.params = {
					id: item.iqid || '',
					javatype: item.javatype
				};
			}
			ajax.body = item;
			ajax.generateRequest();
		}
	}

	@listen('remove-admin-item')
	private _onRemoveIqItem(evt, detail) {
		var iqItem = detail.item;
		var ajax = this.$.iqDeleteAjax;
		ajax.params = {
			id: iqItem.iqid,
			javatype: iqItem.javatype
		};
		ajax.generateRequest();
		this.removeItem(iqItem);
	}

	removeItem(iqItem) {
		var iqs = this.essentialMinistries[this.getEssentialMinistryIndex(iqItem.emid)].impactQuestions;
		for (let i = 0; i < iqs.length; i++) {
			var iq = iqs[i];
			if (iq.iqid === iqItem.iqid) {
				var splicePath = 'essentialMinistries.' + this.getEssentialMinistryIndex(iqItem.emid) + '.impactQuestions';
				this.splice(splicePath, i, 1);
				break;
			}
		}
	}

	@listen('add-admin-item')
	private _onAddIqItem(evt, detail) {
		var initiatingIqItem = detail.item;
		var newIqItem = {
			emid: initiatingIqItem.emid,
			iqid: null,
			javatype: 'org.salvationarmy.cep.model.ImpactQuestion',
			question: 'New Impact Question',
			sequence: initiatingIqItem.sequence
		};
		var splicePath = 'essentialMinistries.' + this.getEssentialMinistryIndex(initiatingIqItem.emid) + '.impactQuestions';
		this.splice(splicePath, newIqItem.sequence, 0, newIqItem);
	}

	private _onIqItemCreated(evt, detail) {
		this.fire('fetch-essential-ministries', {});
	}

	getEssentialMinistryIndex(emid) {
		var returnVal = -1;
		for (let i = 0; i < this.essentialMinistries.length; i++) {
			var em = this.essentialMinistries[i];
			if (em.emid === emid) {
				returnVal = i;
				break;
			}
		}
		return returnVal;
	}

}

CepAdminIq.register();
