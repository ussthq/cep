/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-item')
class CepAdminItem extends polymer.Base {
	@property({
		type: Boolean,
		value: false,
		reflectToAttribute: true
	})
	collapse:boolean;

	@property({
		type: Boolean,
		value: false,
		reflectToAttribute: true
	})
	draggable:boolean;

	@property({
		type: Boolean,
		value: true,
		reflectToAttribute: true
	})
	readonly:boolean;

	@property({
		type: String,
		value: 'arrow-drop-down'
	})
	toggleCollapseIcon:string;

	@property({
		type: String,
		reflectToAttribute: true
	})
	itemValue:string;

	@property({
		type: String
	})
	itemName:string;

	@property({
		type: Number
	})
	itemId:number;

	@property({
		type: Object
	})
	item:any;

	@property({
		type: Boolean
	})
	canAddRemove:boolean;

	@property({
		type: Boolean,
		value: false
	})
	noEdit:boolean;

	private _toggleCollapse(evt) {
		console.log(this.is, '_toggleCollapse', arguments);
		this.$.collapse.toggle();
		var detailObj = null;
		if (this.$.collapse.opened) {
			this.set('toggleCollapseIcon', 'arrow-drop-up');
			detailObj = {opened: true};
		}else {
			this.set('toggleCollapseIcon', 'arrow-drop-down');
			detailObj = {opened: false};
		}
		this.fire('admin-item-opened', detailObj);
	}

	private _onEdit(evt, detail) {
		if (!this.noEdit) {
			var detailObj = {
				itemValue: this.itemValue,
				itemId: this.itemId
			};
			this.readonly = false;
			this.fire('edit-admin-item', detailObj);
		}
	}

	private _onCancel(evt, detail) {
		this.readonly = true;
		this.fire('cancel-edit-admin-item');
	}

	private _onRemove(evt, detail) {
		var detailObj = {
			itemValue: this.itemValue,
			itemId: this.itemId,
			item: this.item
		};
		this.fire('remove-admin-item', detailObj);
	}

	private _onAdd(evt, detail) {
		var detailObj = {
			itemValue: this.itemValue,
			itemId: this.itemId,
			item: this.item
		};
		this.fire('add-admin-item', detailObj);
	}

	private _onSave(evt, detail) {
		var detailObj = {
			itemValue: this.itemValue,
			itemId: this.itemId,
			item: this.item
		};
		this.readonly = true;
		this.fire('save-admin-item', detailObj);
	}

}

CepAdminItem.register();