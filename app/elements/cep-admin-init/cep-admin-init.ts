/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-init')
class CepAdminInit extends polymer.Base {
	@property({
		type: Array
	})
	corpResponse:any;

	@property({
		type: Array,
		value: []
	})
	selectedCorps:any;

	@property({
		type: String
	})
	bottomTab:string;

	@property({
		type: String
	})
	selectedYear:string;

	@property({
		type: String
	})
	initializeSuccessMessage:string;

	@property({
		type: Object
	})
	user: any;

	@property({
		type: Object
	})
	settings: any;

	attached() { }

	@observe('bottomTab, user')
	private _onBottomTabChange(newVal, user) {
		if (newVal === 'initiate' && !this.corpResponse) {
			var ajax = this.$.nameAjax;
			ajax.params = {
				method: 'getCorpInfo'
			};
			ajax.generateRequest();
		}
	}

	clearSelections(evt) {
		var menu = this.$.corpMenu;
		menu.selectedValues = [];
		this.set('selectedCorps', []);
	}

	private _onAjaxResponse(evt, detail) {
		var response = detail.response;
		//var select = [];
		var menu = this.$.corpMenu;
		var items = response;
		// console.log(this.is, '_onAjaxResponse, items=', items);
		if (this.user && this.user.roles.indexOf('[Admin]') === -1 && this.user.roles.indexOf('[DHQ]') > -1) {
			items = items.filter(function(corp) {
				return (corp.div_id == this.user.divid) && corp.unit_name;
			}.bind(this));
		} else if (this.user && this.user.roles.indexOf('[Admin]') > -1) {
			items = items.filter(function (corp) {
				return corp.unit_name;
			});
		}
		// console.log(this.is, '_onAjaxResponse, filtered items=', items);
		this.corpResponse = items;
		for (let i = 0; i < items.length; i++) {
			var item = response[i];
			if (item.unit_type === 'CRP') {
				menu.select(i);
			}
		}
		//this.set('selectedCorps', select);
		this._addSelected();
	}
	/**
	 * Add the names selected in the names list, to the selected names list
	 */
	private _addSelected() {
		var selectedItems = null;
		selectedItems = this.$.corpMenu.selectedValues;
		var corpResponse = this.corpResponse;
		for (var i = 0; i < selectedItems.length; i++) {
			var pushVal = corpResponse[selectedItems[i]];
			if (this.selectedCorps.indexOf(pushVal) === -1) {
				this.push('selectedCorps', pushVal);
			}
		}
		if (this.selectedCorps.length > 0) {
			var detailObj = {
				selected: this.selectedCorps
			};
			this.fire('items-added', detailObj);
		}
	}
	/**
	 * Remove the selected items from the selected names list
	 */
	private _removeSelected() {
		var selectedRemoveMenuValues = null;
		selectedRemoveMenuValues = this.$.selectedMenu.selectedValues;
		if (selectedRemoveMenuValues && selectedRemoveMenuValues.length > 0) {
			var selectedRemoveItems = [];
			for (var i = 0; i < selectedRemoveMenuValues.length; i++) {
				var selectedItem = selectedRemoveMenuValues[i];
				selectedRemoveItems.push(this.selectedCorps[selectedItem]);
			}
			for (var j = 0; j < selectedRemoveItems.length; j++) {
				var removeItemIdx = this._findIndex('selected', selectedRemoveItems[j]);
				if (removeItemIdx > -1) {
					this.splice('selectedCorps', removeItemIdx, 1);
				}
			}
			this._deselectRemovedNames(selectedRemoveItems);
			this.$.selectedMenu.selectedValues = [];
			if (selectedRemoveItems.length > 0) {
				var detailObj = {
					selected: selectedRemoveItems
				};
				this.fire('items-removed', detailObj);
			}
		}
	}
	/**
	 * Deselect the names in the names list after they were removed from the selected names list
	 * @param  {Array} removedItems An array of the items which were removed
	 */
	private _deselectRemovedNames(removedItems) {
		for (var i = 0; i < removedItems.length; i++) {
			var removeNameIdx = this._findIndex('corps', removedItems[i]);
			this.$.corpMenu.select(removeNameIdx);
		}
	}
	/**
	 * Find the index of an item in the response list or selected names list
	 * @param  {String} menuType The menu type to look in. Acceptable values are "corps" or "selected"
	 * @param  {Object} item     The Object that is being worked with
	 * @return {Number}          The array index of the item. -1 if not found.
	 */
	private _findIndex(menuType, item) {
		var menuItems = null;
		if (menuType === 'corps') {
			menuItems = this.corpResponse;
		}else if (menuType === 'selected') {
			menuItems = this.selectedCorps;
		}
		return menuItems.indexOf(item);
	}
	/**
	 * Initiate the reviews for the selected corps
	 * @param {Event} evt The event object
	 */
	private _initiateSelectedReviews(evt) {
		let confirmDialog = this.$.confirmInit;
		confirmDialog.set('dialogTitle', 'Really Initiate Review(s)?');
		let selectedCorpsCount = this.selectedCorps && this.selectedCorps.length > 0 ? this.selectedCorps.length : 0;
		let dialogText = 'You are about to initiate ' + selectedCorpsCount + ' review(s). Are you sure you want to continue?';
		confirmDialog.set('dialogText', dialogText);
		confirmDialog.set('cancelButtonColor', 'white');
		let confirmCallback = () => {
			var ajax = this.$.initiateReviewsAjax;
			ajax.params = {
				initiate: 'true',
				year: this.selectedYear,
				unitportalid: this.settings['UNIT_PORTAL_AUTHTOKEN'],
				unitportalurl: this.settings['UNIT_PORTAL_BASE_URL']
			};
			ajax.body = this.selectedCorps;
			ajax.generateRequest();
		};
		confirmDialog.open(confirmCallback, null);
	}

	private _onCorpsInitialized(evt, detail) {
		var response = detail.response;
		this.set('initializeSuccessMessage', response.status);
		this.$.corpsReviewsInitializedDialog.open();
	}

	patchOverlay(evt, detail) {
		if (evt.target.withBackdrop) {
			evt.target.parentNode.insertBefore(evt.target.backdropElement, evt.target);
		}
	}

}

CepAdminInit.register();
