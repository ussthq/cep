//// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../now-validation-behavior/now-validation-behavior.ts" />

@component('cep-eval-em')
@behavior(NowBehaviors.ValidationBehavior)
class CepEvalEm extends polymer.Base implements NowBehaviors.ValidationBehavior {
	/**
	 * The evaluation object
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	evaluation:any;
	/**
	 * A single essential ministry from the evaluation
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true,
		observer: 'getOverallScore'
	})
	essentialMinistry:any;
	/**
	 * The overall score for the essential ministry
	 * @type: String
	 */
	@property({
		type: String
	})
	overallScore:string;
	/**
	 * The index of this essential ministry
	 * @type {Number}
	 */
	@property({
		type: Number
	})
	index:number;
	/**
	 * True if this essential ministry is valid
	 * @type {Boolean}
	 */
	@property({
		type: Boolean
	})
	isValid:boolean;
	/**
	 * Object of items that are invalid. Used by the 'Review & Submit' tab
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	invalidItems:any;
	/**
	 * The columns array for a chart. This changes as each sub category is hovered over
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	chartCols:any;
	/**
	 * The rows array for a chart. This changes as each sub category is hovered over
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	chartRows:any;
	/**
	 * The options for the chart element
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	chartOptions:any;
	/**
	 * The year selected
	 * @type {Number}
	 */
	@property({
		type: Number
	})
	selectedYear:number;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	user:any;
	/**
	 * True to show the main page spinner
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;
	/**
	 * True to show the chart loading spinner
	 * @type {Boolean}
	 */
	@property({
		type: Boolean
	})
	chartLoading:boolean;
	/**
	 * The settings object
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	settings:any;
	/**
	 * True if this essential ministry should be locked
	 * @type {Boolean}
	 */
	@computed({type: Boolean})
	locked(evaluation) {
		var returnVal = false;
		if (evaluation) {
			returnVal = evaluation.review.locked;
		}
		return returnVal;
	}
	/**
	 * Determines if the next button is hidden
	 * @type {Boolean}
	 */
	@computed()
	nexthidden(index, evaluation) {
		var returnVal = false;
		if (index && evaluation) {
			if ('review' in evaluation && 'essentialMinistries' in evaluation.review) {
				var ems = evaluation.review.essentialMinistries;
				if (index === (ems.length - 1)) {
					returnVal = true;
				}
			}
		}
		return returnVal;
	}
	/**
	 * Determines if the back button is hidden
	 * @type {Boolean}
	 */
	@computed()
	prevhidden(index) {
		var returnVal = index === 0;
		return returnVal;
	}
	/**
	 * Determines if the continue to planning page button is hidden
	 * @type {Boolean}
	 */
	@observe('evaluation.review.*')
	private _toPlanningHidden(newVal) {
		var returnVal = true;
		if (newVal) {
			if ('review' in this.evaluation && 'essentialMinistries' in this.evaluation.review) {
				this.async(function() {
					var allValid = this.isAllEmsValid();
					if (allValid) {
						var ems = this.evaluation.review.essentialMinistries;
						if (this.index === (ems.length - 1)) {
							returnVal = false;
						}
					}
					this.set('toPlanningHidden', returnVal);
				}.bind(this), 500);
			}
		}
	}
	/**
	 * Fired when a sub category is hovered on. Requests the history of the
	 * the hovered on sub category
	 * @param {Event} evt The event object
	 * @param {Object} detail The detail object
	 */
	@listen('now-popover-opened')
	private _onPopoverOpened(evt, detail) {
		// console.log(this.is, '_onPopoverOpened', arguments);
		var subCatItem = evt.target.cepItem;
		if (subCatItem) {
			var ajax = this.$.scoreHistoryAjax;
			ajax.params = {
				method: 'getScoreHistory',
				year: this.selectedYear,
				locationid: this.evaluation.review.locationid,
				emscid: subCatItem.emscid
			};
			ajax.generateRequest();
		}
	}
	/**
	 * Observer for any property in the essential ministry. Sets up the object that
	 * was updated and readies it to be submitted to the server
	 * @param {Object} The observer object
	 */
	@observe('essentialMinistry.*')
	private _onValueChange(observerObj) {
		// console.log(this.is, '_onValueChange', arguments);
		this.validate();
		var pathArr = observerObj.path.split('.');
		var updateObj = null;
		if (pathArr && pathArr[1] === 'subCategories') {
			if (pathArr[3] === 'score' || pathArr[3].indexOf('Score') > -1) {
				var arrIdx = pathArr[2];
				if (arrIdx.indexOf('#') > -1) {
					arrIdx = arrIdx.replace('#', '');
				}
				updateObj = this.essentialMinistry.subCategories[arrIdx];
				updateObj.MinistryScore = this.getMinistryScore(updateObj);
				this.set('essentialMinistry.subCategories.' + arrIdx + '.MinistryScore', updateObj.MinistryScore);
				this.getOverallScore(this.essentialMinistry);
			}
		}else if (pathArr && pathArr[1] === 'impactQuestions') {
			updateObj = this.essentialMinistry.impactQuestions[pathArr[2]];
		}
		if (pathArr && pathArr[1] === 'impactQuestions') {
			/* Do Nothing, we don't want to flood the server with updates because this method is called
			 * for every keyup in the text areas
			 */
		} else {
			if (updateObj && !this.locked && pathArr[3] !== 'MinistryScore') {
				if (updateObj.javatype.indexOf('EMSubCategory')) {
					updateObj.score = parseInt(observerObj.value, 10);
				}
				// console.log('essential-ministry-eval._valueUpdated, firing em-item-updated', updateObj);
				this.fire('em-item-updated', updateObj);
			}
		}
	}
	private _onContinuePlanning(evt) {
		location.href='#/eval/corpsplanning/' + this.evaluation.review.revid + '/0';
	}
	/**
	 * Fired when the scores are fetched
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onScoresFetched(evt, detail) {
		var response = detail.response;
		if (response) {
			var rows = [];
			var cols = [
				{label: "Year", type: "string"},
				{label: "Membership", type: "number"}, // MembershipScore
				{label: "Meetings", type: "number"}, // StandardScore
				{label: "Attendance", type: "number"}, // GrowthScore
				{label: "Giving", type: "number"}, // ParticipationScore
				{label: "Leadership", type: "number"}, // score
				{label: "Ministry", type: "number"} // MinistryScore
			];
			for (let i = 0; i < response.length; i++) {
				var item = response[i];
				var rowItem = [];
				// Order has to match the cols
				rowItem.push(
					item.year.toString(),
					item.MembershipScore,
					item.StandardScore,
					item.GrowthScore,
					item.ParticipationScore,
					item.score,
					item.MinistryScore
				);
				rows.push(rowItem);
			}
			this.chartCols = cols;
			this.chartRows = rows;
			this.chartOptions = this._getChartOptions();
		}
	}
	/**
	 * Get the options for the chart. This object must be formatted as JSON
	 */
	_getChartOptions() {
		return {
			"hAxis": {"title": "Scores", "minValue": 0, "maxValue": 4, "format": "#"},
			"vAxis": {"title": "Years"},
			"legend": {"position": "right"},
			"chartArea": {"width": "45%", "height": "75%"},
			"width": 400
		};
	}
	/**
	 * Close the charts popover
	 * @param {Event} evt The event object
	 */
	closeCharts(evt) {
		evt.stopPropagation();
		var closeButton = evt.target;
		var currentElem = closeButton;
		while (currentElem.localName !== 'now-popover') {
			var currentElem = currentElem.parentNode;
			if (currentElem.localName === 'now-popover') {
				currentElem.hide();
			}
		}
	}
	/**
	 * Fired when the Save & Continue button is clicked
	 * @param {Event} evt The event object
	 */
	private _onSaveContinue(evt) {
		this.fire('move-next-eval-page');
	}
	/**
	 * Fired when the back button is clicked
	 * @param {Event} evt The event object
	 */
	private _onBack(evt) {
		this.fire('move-prev-eval-page');
	}
	/**
	 * Fired when an impact question answer is changed
	 * @param {Event} evt The event object
	 */
	private _onImpactQuestionChange(evt) {
		// console.log(this.is, '_onImpactQuestionChange', arguments);
		var question = evt.model.question;
		if (!this.locked) {
			this.fire('em-item-updated', question);
		}
	}
	/**
	 * Get the overall score for this essential ministry
	 * @param {Object} essentialMinistry The essential ministry
	 * @return {String} - A string instead of a number because toFixed returns a string
	 */
	getOverallScore(essentialMinistry) {
		// console.log(this.is, 'getOverallScore', arguments);
		var overallScore = 0;
		if (essentialMinistry) {
			var subCats = essentialMinistry.subCategories;
			var count = 0;
			for (var i = 0; i < subCats.length; i++) {
				count++;
				var ministryScore = this.getMinistryScore(subCats[i]);
				overallScore = overallScore + ministryScore;
			}
			overallScore = overallScore / count;
		}
		this.set('overallScore', overallScore.toFixed(2));
		return this.overallScore;
	}
	/**
	 * Add up all the scores for a specific sub category
	 * @param {Object} subCat The sub category
	 */
	getMinistryScore(subCat) {
		// console.log(this.is, 'getMinistryScore', arguments);
		var scores = [];
		var memScore = subCat.MembershipScore;
		var standScore = subCat.StandardScore;
		var partScore = subCat.ParticipationScore;
		var growthScore = subCat.GrowthScore;
		var leadScore = parseInt(subCat.score, 10);
		if (memScore > 0) {
			scores.push(memScore);
		}
		if (standScore > 0) {
			scores.push(standScore);
		}
		if (partScore > 0) {
			scores.push(partScore);
		}
		if (growthScore > 0) {
			scores.push(growthScore);
		}
		if (leadScore > 0) {
			scores.push(leadScore);
		}
		var ministryScore = 0;
		for (var i = 0; i < scores.length; i++) {
			var scoreItem = scores[i];
			ministryScore = ministryScore + scoreItem;
		}
		if (scores.length === 0) {
			ministryScore = 0.00;
		}else {
			ministryScore = parseFloat((ministryScore / scores.length).toFixed(2));
		}
		if (ministryScore !== subCat.MinistryScore) {
			subCat.MinistryScore = ministryScore;
		}
		return ministryScore;
	}
	/**
	 * Validate this essential ministry to ensure all fields and scores have a value
	 * @return {Boolean} true if essential ministry passes validation
	 */
	validate() {
		//console.log(this.is, 'validate', this.essentialMinistry.name);
		var validationItem = this.isEmValid(this.essentialMinistry, this.locked);
		this.isValid = validationItem.valid;
		this.invalidItems = {
			em: this.essentialMinistry,
			invalidItems: validationItem.invalidItems,
			valid: validationItem.valid
		};
		this.fire('cep-eval-em-validated', {invalidItems: this.invalidItems, valid: this.isValid});
		return this.isValid;
	}
	/**
	 * Stand-in method for ValidationBehavior
	 */
	_getAllEmPages: () => any;
	getInvalidEms: () => any;
	getAllEmValidationItems: () => any;
	isAllEmsValid: () => boolean;
	getAllValidationItems: (review:any) => any;
	isEmValid: (essentialMinistry:any, locked:any) => any;
	isReviewValid: (evaluation:any) => boolean;
	_getInvalidCount: (validationItemsArr:any) => number;

}

CepEvalEm.register();