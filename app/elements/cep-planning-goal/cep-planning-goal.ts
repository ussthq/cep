/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-planning-goal')
class CepPlanningGoal extends polymer.Base {

	@property({
		type: Object,
		notify: true
	})
	evaluation:any;

	@property({
		type: Object,
		notify: true
	})
	goal:any;

	@property({
		type: Boolean,
		notify: true
	})
	locked:boolean;

	@property({
		type: Object
	})
	essentialMinistry:any;

	@observe('goal.*')
	private _onGoalChange(newVal, oldVal) {
		//console.log(this.is, '_onGoalChange', arguments);
	}
}

CepPlanningGoal.register();
