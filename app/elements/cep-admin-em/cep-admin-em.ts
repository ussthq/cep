/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-em')
class CepAdminEm extends polymer.Base {
	@property({
		type: Array
	})
	essentialMinistries:any;

	@property({
		type: Object
	})
	user:any;

	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;

	@listen('save-admin-item')
	_onSaveItem(evt, detail) {
		console.log(this.is, '_onSaveItem', arguments);
		if (detail && detail.item) {
			var item = detail.item;
			var ajax = this.$.emPutAjax;
			if (item.javatype === 'org.salvationarmy.cep.model.EMSubCategory' && !item.emscid) {
				ajax = this.$.emPostAjax;
			}else if (item.javatype === 'org.salvationarmy.cep.model.EssentialMinistry' && !item.emid) {
				ajax = this.$.emPostAjax;
			}
			if (item.javatype.indexOf('EssentialMinistry') > -1) {
				delete item.subCategories;
				delete item.impactQuestions;
				delete item.scoringYear;
				ajax.params = {
					id: item.emid || '',
					javatype: item.javatype
				};
			}else if (item.javatype.indexOf('EMSubCategory') > -1) {
				delete item.emName;
				ajax.params = {
					id: item.emscid || '',
					javatype: item.javatype
				};
			}
			ajax.body = item;
			ajax.generateRequest();
		}
	}

	@listen('remove-admin-item')
	private _onRemoveEMItem(evt, detail) {
		var emItem = detail.item;
		var ajax = this.$.emDeleteAjax;
		if (emItem.javatype === 'org.salvationarmy.cep.model.EssentialMinistry') {
			ajax.params = {
				id: emItem.emid,
				javatype: emItem.javatype
			};
		}else if (emItem.javatype === 'org.salvationarmy.cep.model.EMSubCategory') {
			ajax.params = {
				id: emItem.emscid,
				javatype: emItem.javatype
			};
		}
		ajax.generateRequest();
		this.removeItem(emItem);
	}

	removeItem(emItem) {
		var ems = this.essentialMinistries;
		if (emItem.javatype === 'org.salvationarmy.cep.model.EssentialMinistry') {
			for (let i = 0; i < ems.length; i++) {
				var em = ems[i];
				if (em.emid === emItem.emid) {
					this.splice('essentialMinistries', i, 1);
					break;
				}
			}
		}else if (emItem.javatype === 'org.salvationarmy.cep.model.EMSubCategory') {
			var subCats = ems[this.getEssentialMinistryIndex(emItem.emid)].subCategories;
			for (let i = 0; i < subCats.length; i++) {
				var subCat = subCats[i];
				if (subCat.emscid === emItem.emscid) {
					var splicePath = 'essentialMinistries.' + this.getEssentialMinistryIndex(emItem.emid) + '.subCategories';
					this.splice(splicePath, i, 1);
					break;
				}
			}
		}
	}

	@listen('add-admin-item')
	private _onAddEMItem(evt, detail) {
		var initiatingEmItem = detail.item;
		var newEmItem = null;
		if (initiatingEmItem.javatype === 'org.salvationarmy.cep.model.EssentialMinistry') {
			newEmItem = {
				name: 'New Essential Ministry',
				year: this.get('reviewYear'),
				javatype: 'org.salvationarmy.cep.model.EssentialMinistry',
				sequence: initiatingEmItem.sequence,
				emid: null,
				definition: null
			};
			this.splice('essentialMinistries', newEmItem.sequence, 0, newEmItem);
		}else if (initiatingEmItem.javatype === 'org.salvationarmy.cep.model.EMSubCategory') {
			newEmItem = {
				subcategory: 'New Sub Ministry',
				emid: initiatingEmItem.emid,
				emscid: null,
				javatype: 'org.salvationarmy.cep.model.EMSubCategory',
				duration: 'Weekly',
				sequence: initiatingEmItem.sequence
			};
			var splicePath = 'essentialMinistries.' + this.getEssentialMinistryIndex(initiatingEmItem.emid) + '.subCategories';
			this.splice(splicePath, newEmItem.sequence, 0, newEmItem);
		}
	}

	private _onEmItemCreated(evt, detail) {
		this.fire('fetch-essential-ministries', {});
	}

	private _onItemSortUpdate() {
		console.log(this.is, '_onItemSortUpdate', arguments);
	}

	getEssentialMinistryIndex(emid) {
		var returnVal = -1;
		for (let i = 0; i < this.essentialMinistries.length; i++) {
			var em = this.essentialMinistries[i];
			if (em.emid === emid) {
				returnVal = i;
				break;
			}
		}
		return returnVal;
	}
}

CepAdminEm.register();
