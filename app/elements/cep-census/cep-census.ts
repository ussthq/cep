/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../now-number-util-behavior/now-number-util-behavior.ts" />

@component('cep-census')
@behavior(NowBehaviors.NumberUtilBehavior)
class CepCensus extends polymer.Base implements NowBehaviors.NumberUtilBehavior {
	/**
	 * True if data is being fetched from the server
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading: boolean;
	/**
	 * The settings Object
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	settings: any;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object,
		observer: '_onUserData',
		notify: true
	})
	user:any;
	/**
	 * The census Data fetched from the server
	 * @type {Object}
	 */
	@property({
		type: Object,
		observer: '_onCensusData'
	})
	censusData: any;
	/**
	 * The zip code typed in by the user
	 * @type {String}
	 */
	@property({
		type: String,
		observer: '_onZipcode'
	})
	zipCode: string;
	/**
	 * The radius typed in by the user
	 * @type {Number}
	 */
	@property({
		type: Number,
		value: 0
	})
	radius: number;
	/**
	 * true for summary data. Default is false and user has no method
	 * to change it currently
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		value: false
	})
	summarize: boolean;
	/**
	 * The options object for By Median Age
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: 45, top: 0, width: '80%', height: '85%'},
			legend: 'none',
			format: '0'
		}
	})
	medianAgeOptions: any;
	/**
	 * The options object for the "By Household" chart
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: 45, top: 0, width: '80%', height: '95%'},
			hAxis: {textPosition: 'none'},
			tooltip: {isHtml: true},
			legend: 'none'
		}
	})
	householdOptions: any;
	/**
	 * The options object for all Pie charts
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {
			chartArea: {left: '15', top: '15', width: '100%', height: '100%'},
			slices: {
				0: {color: '#009e0f'},
				1: {color: '#2b78e4'},
				2: {color: '#dc3912'},
				3: {color: '#9900ff'},
				4: {color: '#ff9900'},
			}
		}
	})
	pieChartOptions: any;
	/**
	 * The options object for the "By Income" chart
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: 45, top: 0, width: '80%', height: '95%'},
			hAxis: {textPosition: 'none'},
			legend: 'none'
		}
	})
	incomeOptions: any;
	/**
	 * The data for the "Other Chart"
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	otherChartData: any;
	/**
	 * The title for the "Other Chart"
	 */
	@property({
		type: String
	})
	otherChartTitle: string;
	/**
	 * The type of chart to display for the "Other Chart"
	 * @type {String}
	 */
	@property({
		type: String
	})
	otherChartType: string;
	/**
	 * Options object for the "Other Chart"
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	otherChartOptions: any;
	/**
	 * The data for the data display dialog
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	displayData: any;
	/**
	 * The title for the data display dialog
	 * @type {String}
	 */
	@property({
		type: String
	})
	dataDisplayDialogTitle: string;
	/**
	 * Options object for the meanOfMedians income chart
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: 45, top: 0, width: '80%', height: '85%'},
			legend: 'none',
			vAxis: {
				title: 'Deviation of Income',
				format: 'decimal'
			},
			hAxis: {
				title: 'Average Income',
				format: 'decimal'
			},
			explorer: {
				actions: ['dragToZoom', 'rightClickToReset']
			}
		}
	})
	meanIncomeOptions: any;
	/**
	 * Options object for the singleMother chart
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: 45, top: 0, width: '80%', height: '85%'},
			legend: 'none',
			vAxis: {
				title: 'Deviation of Income',
				format: 'decimal'
			},
			hAxis: {
				title: 'Number of Single Mothers',
				format: 'decimal',
				direction: -1
			},
			explorer: {
				actions: ['dragToZoom', 'rightClickToReset']
			}
		}
	})
	singleMotherOptions: any;
	/**
	 * Options object for the singleMother chart
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: 45, top: 0, width: '80%', height: '85%'},
			legend: 'none',
			vAxis: {
				title: 'Deviation of Income',
				format: 'decimal'
			},
			hAxis: {
				title: 'Number of Households w/Children',
				format: 'decimal',
				direction: -1
			},
			explorer: {
				actions: ['dragToZoom', 'rightClickToReset']
			}
		}
	})
	childHouseholdOptions: any;
	/**
	 * Show multiple zip codes
	 * @param {Object} censusData The censusData
	 * @return {Boolean}
	 */
	@computed()
	showMultipleZips(censusData) {
		let returnVal = false;
		if (censusData && censusData.zipDemoInfo.length > 1) {
			returnVal = true;
		}
		return returnVal;
	}

	attached() {
		// Resize the charts to fit the space
		let zipCodeParam = this.getParameterByName('zipcode', location.href);
		if (zipCodeParam) {
			this.set('zipCode', zipCodeParam);
			this.async(() => {
				this.fetchCensusData();
			}, 500);
		}
		window.addEventListener('resize', () => {
			this.redrawCharts();
		});
	}

	/**
	 * Show the display data dialog
	 * @param {Event} evt The event object
	 */
	showData(evt) {
		// console.log(this.is, 'showData', arguments);
		let elem = evt.target;
		if (elem.tagName !== 'paper-icon-button') {
			elem = this.getParentOfType(evt.target, 'paper-icon-button');
		}
		if (elem) {
			let elemId = elem.id;
			if (elemId === 'byGender') {
				this.displayData = this._getGenderData(this.censusData);
				this.dataDisplayDialogTitle = 'By Gender Data';
			} else if (elemId === 'byRace') {
				this.displayData = this._getRaceData(this.censusData);
				this.dataDisplayDialogTitle = 'By Race Data';
			} else if (elemId === 'byHousehold') {
				this.displayData = this._getHouseholdData(this.censusData);
				this.dataDisplayDialogTitle = 'By Household Data';
			} else if (elemId === 'byAge') {
				this.displayData = this._getMedianAgeData(this.censusData);
				this.dataDisplayDialogTitle = 'By Median Age Data';
			} else if (elemId === 'byIncome') {
				this.displayData = this._getIncomeData(this.censusData);
				this.dataDisplayDialogTitle = 'By Median Income Data';
			}
			if (this.displayData && this.displayData.length > 0) {
				this.$.dataDisplayDialog.open();
			}
		}
	}
	/**
	 * Determine if the scatter charts should be hidden. True to hide the charts.
	 * These charts should be hidden if no radius was included
	 * @param {Object} censusData The census data
	 */
	hideScatterChart(censusData) {
		// console.log(this.is, 'hideScatterChart', arguments);
		let returnVal = true;
		if (censusData && censusData.zipDemoInfo.length > 1) {
			returnVal = false;
		}
		return returnVal;
	}
	/**
	 * Add selection listeners to all the pie charts
	 */
	addSelectListeners() {
		let pies = document.querySelectorAll('google-chart[type="pie"]');
		// console.log(this.is, 'addSelectListeners, pies=', pies);
		for (let i = 0; i < pies.length; i++) {
			let pie = pies[i];
			pie.addEventListener('google-chart-select', this._selectPie.bind(this));
		}

		let scatters = document.querySelectorAll('google-chart[type="scatter"]');
		for (let i =0; i < scatters.length; i++) {
			let scatter = scatters[i];
			scatter.addEventListener('google-chart-select', this._selectScatter.bind(this));
		}
	}
	/**
	 * Fired when a point on a scatter chart is clicked. Opens a new tab
	 * to Google Maps to include the zip code
	 * @param {Event} evt The event object
	 */
	_selectScatter(evt) {
		// console.log(this.is, '_selectScatter', arguments);
		let chart = evt.detail.chart;
		let sel = chart.getSelection()[0];
		let dataIdx = sel.row + 1;
		let chartId = evt.currentTarget.id;
		let data = null;
		if (chartId === 'childHouseholdsChart') {
			data = this._getChildHouseholdData(this.censusData);
		}else if (chartId === 'singleMotherChart') {
			data = this._getSingleMomData(this.censusData);
		}else if (chartId === 'meanOfMediansChart') {
			data = this._getMeanOfMedianData(this.censusData);
		}
		let zip = data[dataIdx][2];
		// console.log(this.is, '_selectScatter, zip=', zip);
		window.open(
			'https://www.google.com/maps/place/' + zip + ',USA',
			'_blank'
		);
	}
	/**
	 * Get the zip codes in the data set
	 * @param {Object} censusData The censusData
	 * @return {String}
	 */
	getZipCodes(censusData) {
		let zipArr = [];
		let zips = '';
		if (censusData) {
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				zipArr.push(censusData.zipDemoInfo[i].zip);
			}
			zips = zipArr.join(', ');
		}
		return zips;
	}
	/**
	 * Get the total population of all the included zip codes
	 * @param {Object} censusData The census data
	 */
	getTotalPopulation(censusData) {
		let total = 0;
		if (censusData) {
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let popInfo = censusData.zipDemoInfo[i].demographicInfo.population;
				total = total + parseInt(popInfo['total']);
			}
		}
		return this.toPrettyNumber(total);
	}
	/**
	 * Handle a google-chart-select event. Currently only for selecting
	 * "Other Race" in the "By Race" chart
	 * @param {Event} evt The event object
	 * @listens google-chart-select
	 */
	private _selectPie(evt) {
		// console.log(this.is, '_selectPie', arguments, this);
		let chart = evt.detail.chart;
		let chartElem = evt.target;
		if (chartElem.tagName !== 'google-chart') {
			chartElem = this.getParentOfType(evt.target, 'google-chart');
		}
		let otherChartData = null;
		let otherChartTitle = null;
		let otherChartType = null;
		let otherChartOptions = null;
		let chartSel = chart.getSelection();
		if (chartSel && chartSel.length > 0) {
			let idx = chartSel[0].row + 1;
			let sel = null;
			if (chartElem && chartElem.id === 'raceChart') {
				sel = this._getRaceData(this.censusData)[idx];
				if (sel && sel[0] && sel[0] === 'Other Races') {
					otherChartData = this._getOtherRaceData(this.censusData);
					otherChartTitle = 'Other Races';
					otherChartType = 'pie';
					otherChartOptions = JSON.parse(JSON.stringify(this.pieChartOptions));
					otherChartOptions.chartArea = {left: 0, top: 0, width: '100%', height: '90%'};
					otherChartOptions.width = '100%';
					otherChartOptions.height = '100%';
				}
			}else if (chartElem && chartElem.id === 'genderChart') {
				sel = this._getGenderData(this.censusData)[idx];
				if (sel && sel[0]) {
					otherChartData = this._getOtherGenderData(this.censusData, sel[0].split(' ')[0]);
					otherChartTitle = sel[0] + ' Age Data';
					otherChartType = 'pie';
					otherChartOptions = JSON.parse(JSON.stringify(this.pieChartOptions));
					otherChartOptions.chartArea = {left: 0, top: 0, width: '100%', height: '90%'};
					otherChartOptions.width = '100%';
					otherChartOptions.height = '100%';
				}
			}
		}
		if (otherChartData && otherChartData.length > 0 && otherChartTitle) {
			this.otherChartType = otherChartType;
			this.otherChartOptions = otherChartOptions;
			this.otherChartData = otherChartData;
			this.otherChartTitle = otherChartTitle;
			this.$.otherChartDialog.open();
			this.redrawCharts();
			chart.setSelection();
		}
	}
	/**
	 * Get the data array for the "Other Gender Data" chart in the dialog
	 * @param {Array} genderData The gender data object
	 * @param {String} gender The gender we're wanting
	 * @return {Array}
	 */
	private _getOtherGenderData(censusData, gender) {
		let data = [];
		if (censusData) {
			data.push([gender,'']);
			let tempObj = {};
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let genderData = censusData.zipDemoInfo[i].demographicInfo.population;
				for (let key in genderData) {
					if (key.toLowerCase().indexOf('total') === -1) {
						let repKey = key.replace(/TO/g, ' To ');
						repKey = this.toProperCase(repKey.replace(/_/g, ' '));
						if (repKey.split(' ')[0].toLowerCase() === gender.toLowerCase()) {
							if (repKey in tempObj) {
								tempObj[repKey] = tempObj[repKey] + parseInt(genderData[key]);
							}else {
								tempObj[repKey] = parseInt(genderData[key]);
							}
						}
					}
				}
			}
			for (let key in tempObj) {
				data.push([key, tempObj[key]]);
			}
		}
		return data;
	}
	/**
	 * Get the other race data for the "Other Chart"
	 * @param {Object} raceData The race data from the censusData object
	 * @return {Array}
	 */
	private _getOtherRaceData(censusData) {
		let data = [];
		if (censusData) {
			let includeVals = [
				'american_Indian_Or_Alaskan_Native',
				'asian_Indian',
				'chinese',
				'filipino',
				'japanese',
				'korean',
				'vietnamese',
				'other_Asian',
				'native_Hawaiian_And_Other_Pacific_Islander',
				'native_Hawaiian',
				'guamanian_Or_Chamorro',
				'samoan',
				'other_Pacific_Islander',
				'vietnamese',
				'some_Other_Race'
			];
			data.push(['Race', '']);
			let tempObj = {};
			for (let j = 0; j < censusData.zipDemoInfo.length; j++) {
				let raceData = censusData.zipDemoInfo[j].demographicInfo.race;
				for (let i = 0; i < includeVals.length; i++) {
					let key = includeVals[i];
					let newKey = this.toProperCase(key.replace(/_/g, ' '));
					if (newKey in tempObj) {
						tempObj[newKey] = tempObj[newKey] + parseInt(raceData[key]);
					}else {
						tempObj[newKey] = parseInt(raceData[key]);
					}
				}
			}
			for (let key in tempObj) {
				data.push([key, tempObj[key]]);
			}
		}
		return data;
	}
	/**
	 * Get the parent with a particular tag name
	 * @param {Element} node The node to get the parent of
	 * @param {String} parentTagName The tag name to look for
	 * @return {Element}
	 */
	getParentOfType(node, parentTagName) {
		let returnVal = null;
		if (node && parentTagName) {
			let parent = node.parentNode;
			while (parent !== null && parent.tagName !== 'body') {
				if (parent.tagName && (parent.tagName.toLowerCase() === parentTagName.toLowerCase())) {
					returnVal = parent;
					break;
				} else if (parent.tagName === 'body') {
					break;
				}
				parent = parent.parentNode;
			}
		}
		return returnVal;
	}
	/**
	 * Fired when the zip code changes. If the value is cleared out
	 * also clear the censusData and radius
	 * @param {String} newVal the new zip code
	 * @param {String} oldVal the old zip code
	 */
	private _onZipcode(newVal, oldVal) {
		if (!newVal) {
			this.censusData = null;
			this.radius = 0;
		}
	}
	/**
	 * Fired when the censusData value changes. Sets up all the chart data objects
	 * @param {Object} newVal The new censusData
	 * @param {Object} oldVal the old censusData
	 */
	private _onCensusData(newVal, oldVal) {
		// console.log(this.is, '_onCensusData', arguments);
		if (newVal && newVal.zipDemoInfo.length > 0) {
			this.async(function() {
				this.redrawCharts();
				this.addSelectListeners();
			}.bind(this), 400);
		}
		if (newVal && newVal.zipDemoInfo && newVal.zipDemoInfo.length === 0) {
			this.censusData = null;
		}
	}
	/**
	 * Fired when the user object changes.
	 * @param {Object} newVal The new censusData
	 * @param {Object} oldVal the old censusData
	 */
	private _onUserData(newVal, oldVal) {
		if (newVal && newVal.corpZip) {
			this.zipCode = newVal.corpZip;
		}
	}
	/**
	 * Parses the data for gender and returns the chart data array
	 * @param {Object} censusData the census data
	 * @return {Array}
	 */
	private _getGenderData(censusData) {
		let data = [];
		if (censusData) {
			data.push(['Gender', '']);
			let includeVals = [
				'male_Total',
				'female_Total'
			];
			let tempObj = {};
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let genderData = censusData.zipDemoInfo[i].demographicInfo.population;
				for (let key in genderData) {
					if (includeVals.indexOf(key) > -1) {
						let tempKey = this.toProperCase(key.replace(/_/g, ' '));
						if (tempKey in tempObj) {
							tempObj[tempKey] = tempObj[tempKey] + parseInt(genderData[key]);
						}else {
							tempObj[tempKey] = parseInt(genderData[key]);
						}
					}
				}
			}
			for (let key in tempObj) {
				data.push([key, tempObj[key]]);
			}
		}
		return data;
	}
	/**
	 * Parses the data for income and returns the chart data array
	 * @param {Object} censusData the census data
	 * @return {Array}
	 */
	private _getIncomeData(censusData) {
		// console.log(this.is, '_getIncomeData', arguments);
		let data = [];
		if (censusData) {
			data.push(['Income Datapoint', '', {role: 'annotation'}]);
			let tempObj = {};
			let medianArr = [];
			let meanArr = [];
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let incomeData = censusData.zipDemoInfo[i].demographicInfo.medianIncome;
				medianArr.push(parseInt(incomeData['household_Income_Median']));
				meanArr.push(parseInt(incomeData['household_Income_Mean']));
			}
			let medianTotal = Math.round(this.getAverage(medianArr));
			let meanTotal = Math.round(this.getAverage(meanArr));
			data.push(['Median Household Income', medianTotal, '$' + this.toPrettyNumber(medianTotal)]);
			data.push(['Average Household Income', meanTotal, '$' + this.toPrettyNumber(meanTotal)]);
		}
		return data;
	}
	/**
	 * Parses the data for income and returns the chart data array
	 * @param {Object} censusData the census data
	 * @return {Array}
	 */
	private _getMeanOfMedianData(censusData) {
		let data = [];
		if (censusData) {
			let medianAvg = this.getMedianIncomeAvg(censusData);
			data.push([
				{label: '', type: 'number'},
				{label: 'Average / Median Income Deviation', type: 'number'},
				{role: 'annotation'},
				{role: 'tooltip'}
			]);
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let incomeData = censusData.zipDemoInfo[i].demographicInfo.medianIncome;
				let dev = parseInt(incomeData['household_Income_Median']) - medianAvg;
				dev = Math.round(dev);
				let num = parseInt(incomeData['household_Income_Mean']);
				data.push([
					num,
					dev,
					censusData.zipDemoInfo[i].zip,
					censusData.zipDemoInfo[i].zip + '\nAverage Income / Median Income Deviation\n $' + this.toPrettyNumber(num) + ' / $' + this.toPrettyNumber(dev)
				]);
			}
		}
		return data;
	}
	/**
	 * Get the median income average from the censusData
	 * @param {Object} censusData The census data
	 * @return {Number}
	 */
	getMedianIncomeAvg(censusData) {
		let medianAvgArr = [];
		for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
			let incomeData = censusData.zipDemoInfo[i].demographicInfo.medianIncome;
			medianAvgArr.push(parseInt(incomeData['household_Income_Median']));
		}
		return this.getAverage(medianAvgArr);
	}
	/**
	 * Parses the data for income and returns the chart data array
	 * @param {Object} censusData the census data
	 * @return {Array}
	 */
	private _getSingleMomData(censusData) {
		let data = [];
		if (censusData) {
			data.push([
				{label: '', type: 'number'},
				{label: 'Single Mother households / Median Income', type: 'number'},
				{role: 'annotation'},
				{role: 'tooltip'}
			]);
			let medianAvg = this.getMedianIncomeAvg(censusData);
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let householdData = censusData.zipDemoInfo[i].demographicInfo.households;
				let incomeData = censusData.zipDemoInfo[i].demographicInfo.medianIncome;
				let dev = parseInt(incomeData['household_Income_Median']) - medianAvg;
				dev = Math.round(dev);
				let num = parseInt(householdData['family_Household_Female_Only_With_Own_Children_Under_18']);
				if (num > 0) {
					data.push([
						num,
						dev,
						censusData.zipDemoInfo[i].zip,
						censusData.zipDemoInfo[i].zip + '\nSingle Mother households / Median Income\n' + this.toPrettyNumber(num) + ' / $' + this.toPrettyNumber(dev)
					]);
				}
			}
		}
		return data;
	}
	/**
	 * Parses the data for w/Children income disparity and returns the chart data array
	 * @param {Object} censusData the census data
	 * @return {Array}
	 */
	private _getChildHouseholdData(censusData) {
		let data = [];
		if (censusData) {
			data.push([
				{label: '', type: 'number'},
				{label: 'Households with Children / Median Income', type: 'number'},
				{role: 'annotation'},
				{role: 'tooltip'}
			]);
			let medianAvg = this.getMedianIncomeAvg(censusData);
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let householdData = censusData.zipDemoInfo[i].demographicInfo.households;
				let incomeData = censusData.zipDemoInfo[i].demographicInfo.medianIncome;
				let dev = parseInt(incomeData['household_Income_Median']) - medianAvg;
				dev = Math.round(dev);
				let num = parseInt(householdData['family_Household_With_Own_Children_Under_18']);
				if (num > 0) {
					data.push([
						num,
						dev,
						censusData.zipDemoInfo[i].zip,
						censusData.zipDemoInfo[i].zip + '\nHouseholds w/Children / Median Income\n' + this.toPrettyNumber(num) + ' / $' + this.toPrettyNumber(dev)
					]);
				}
			}
		}
		return data;
	}
	/**
	 * Parses the data for household and returns the chart data array
	 * @param {Object} censusData the census data
	 * @return {Array}
	 */
	private _getHouseholdData(censusData) {
		let data = [];
		if (censusData) {
			data.push(['Household Datapoint','', {role: 'annotation'}]);
			var skipVals = [
				'average_Family_Size',
				'average_Household_Size'
			];
			let tempObj = {};
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let houseData = censusData.zipDemoInfo[i].demographicInfo.households;
				tempObj['Total'] = tempObj['Total'] ? tempObj['Total'] + parseInt(houseData.total) : parseInt(houseData.total);
				for (let key in houseData) {
					let tempKey = this.toProperCase(key.replace(/_/g, ' '));
					if (tempKey in tempObj) {
						tempObj[tempKey] = tempObj[tempKey] + parseInt(houseData[key]);
					}else {
						tempObj[tempKey] = parseInt(houseData[key]);
					}
				}
			}
			for (let key in tempObj) {
				data.push([key, tempObj[key], this.toPrettyNumber(tempObj[key])]);
			}
		}
		return data;
	}
	/**
	 * Parses the data for Median Age and returns the chart data array
	 * @param {Object} censusData the census data
	 * @return {Array}
	 */
	private _getMedianAgeData(censusData) {
		let data = [];
		if (censusData) {
			data.push(['Gender', '', {role: 'annotation'}]);
			let maleAge = [];
			let femaleAge = [];
			//let totalAge = [];
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let ageData = censusData.zipDemoInfo[i].demographicInfo.medianAge;
				maleAge.push(parseInt(ageData['male']));
				femaleAge.push(parseInt(ageData['female']));
				//totalAge.push(parseInt(ageData['total']));
			}
			let male = Math.round(this.getAverage(maleAge));
			let female = Math.round(this.getAverage(femaleAge));
			//let total = Math.round(this.getAverage(totalAge));
			data.push(['Male', male, male.toString()]);
			data.push(['Female', female, female.toString()]);
			//data.push(['Both', total, total.toString()]);
		}
		return data;
	}
	/**
	 * Parses the data for race and returns the chart data array. We clean up the
	 * labels and only surface a few of the properties. All other properties are
	 * consolidated into 'some_Other_Race'
	 * @param {Object} censusData the census data
	 * @return {Array}
	 */
	private _getRaceData(censusData) {
		let data = [];
		if (censusData) {
			data.push(['Race', '']);
			let skipVals = [
				'total',
				'total_Of_One_Race',
				'american_Indian_Or_Alaskan_Native',
				'asian_Indian',
				'chinese',
				'filipino',
				'japanese',
				'korean',
				'vietnamese',
				'other_Asian',
				'native_Hawaiian_And_Other_Pacific_Islander',
				'native_Hawaiian',
				'guamanian_Or_Chamorro',
				'samoan',
				'other_Pacific_Islander',
				'vietnamese',
				'some_Other_Race',
				'total',
				'total_Of_One_Race'
			];
			let tempObj = {};
			let someOtherTotal = 0;
			for (let i = 0; i < censusData.zipDemoInfo.length; i++) {
				let raceData = censusData.zipDemoInfo[i].demographicInfo.race;
				for (let key in raceData) {
					if (skipVals.indexOf(key) > -1 && key !== 'total' && key !== 'total_Of_One_Race') {
						someOtherTotal = someOtherTotal + parseInt(raceData[key]);
					}else if (key !== 'total' && key !== 'total_Of_One_Race') {
						let tempKey = this.toProperCase(key.replace(/_/g, ' '));
						if (tempKey in tempObj) {
							tempObj[tempKey] = tempObj[tempKey] + parseInt(raceData[key]);
						}else {
							tempObj[tempKey] = parseInt(raceData[key]);
						}
					}
				}
				if ('Hispanic Or Latino' in tempObj) {
					tempObj['Hispanic Or Latino'] = tempObj['Hispanic Or Latino'] + parseInt(censusData.zipDemoInfo[i].demographicInfo.race_HispanicLatino.hispanic_or_Latino);
				}else {
					tempObj['Hispanic Or Latino'] = parseInt(censusData.zipDemoInfo[i].demographicInfo.race_HispanicLatino.hispanic_or_Latino);
				}
			}

			for (let key in tempObj) {
				data.push([key, tempObj[key]])
			}
			data.push(['Other Races', someOtherTotal]);
		}
		return data;
	}
	/**
	 * Takes a string and converts it to use proper case. For example:
	 * 'some string' will be converted to 'Some String'
	 * @param {String} str The string to convert to proper case
	 * @return {String}
	 */
	toProperCase(str) {
		return str.replace(/\w\S*/g, function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
	}
	/**
	 * Get the URL for the settings object
	 * @return {String}
	 */
	getSettingsUrl() {
		// console.log(this.is, 'settingsUrl', arguments);
		var url = '/thq/cep2.nsf/scripts/settings.json';
		var port = location.port;
		// Setup for running local
		if (port && port !== '80' && port !== '443') {
            url = '../../scripts/settings.json';
        }
		return url;
	}
	/**
	 * Get the census data url from the settings object
	 * @param {Object} settings The settings object
	 * @return {String}
	 */
	getUrl(settings) {
		return settings.CENSUS_URL;
	}
	/**
	 * Perform a fetch to get the census data
	 */
	fetchCensusData() {
		if (this.zipCode) {
			let ajax = this.$.censusAjax;
			ajax.params = {
				zipcode: this.zipCode,
				AuthId: this.settings.CENSUS_AUTHID,
				AuthToken: this.settings.CENSUS_AUTHTOKEN,
				radius: this.radius,
				summarize: this.summarize
			};
			ajax.generateRequest();
		}
	}
	/**
	 * Redraw the charts to fit their containers
	 */
	redrawCharts() {
		let charts = document.querySelectorAll('google-chart');
		[].forEach.call(charts, (el) => {
			el.redraw();
		});
	}

	getParameterByName(name, url) {
		if (!url) {
			url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
		let results = regex.exec(url);
		if (!results) {
			return null;
		}
		if (!results[2]) {
			return '';
		}
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	/**
	 * Stand-in methods for NowNumberUtilBehavior
	 */
	getDeviationArr: (numberArr: number[]) => number[];
	toPrettyNumber: (num: number) => string;
	getMedian: (numberArr: number[]) => number;
	getAverage: (numberArr: number[]) => number;
	toFixed: (value: any, precision: any) => string;
}

CepCensus.register();
