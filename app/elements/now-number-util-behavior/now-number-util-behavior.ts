/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../../bower_components/DefinitelyTyped/es6-promise/es6-promise.d.ts" />

/**
 * This behavior is for scrolling
 */
namespace NowBehaviors {
	export class NumberUtilBehavior extends polymer.Base {
		/**
		 * Get the average of an array of numbers
		 * @param {Array} numberArr Array of numbers
		 * @return {Number}
		 */
		getAverage(numberArr) {
			let total = 0;
			for(let i = 0; i < numberArr.length; i++) {
				total += numberArr[i];
			}
			return total / numberArr.length;
		}
		/**
		 * Get the median of an array of numbers
		 * @param {Array} numberArr Array of numbers to get the median for
		 * @return {Number}
		 */
		getMedian(numberArr) {
			numberArr.sort( function(a,b) {
				return a - b;
			});
			let half = Math.floor(numberArr.length/2);
			if(numberArr.length % 2) {
				return numberArr[half];
			}else {
				return (numberArr[half-1] + numberArr[half]) / 2.0;
			}
		}
		/**
		 * Format a number to include commas on the thousands
		 * @param {Number} num The number to format
		 * @return {String}
		 */
		toPrettyNumber(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}
		/**
		 * Given a number array and an avg. Will return the deviations
		 * between each number in the array and the average
		 * @param {Array} numberArr Array of numbers
		 * @return {Array}
		 */
		getDeviationArr(numberArr) {
			let returnVal = [];
			if (numberArr && numberArr.length > 0) {
				let avg = this.getAverage(numberArr);
				for (let i = 0; i < numberArr.length; i++) {
					returnVal.push(numberArr[i] - avg);
				}
			}
			return returnVal;
		}
		toFixed(value, precision) {
			var precision = precision || 0,
				power = Math.pow(10, precision),
				absValue = Math.abs(Math.round(value * power)),
				result = (value < 0 ? '-' : '') + String(Math.floor(absValue / power));

			if (precision > 0) {
				var fraction = String(absValue % power),
					padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
				result += '.' + padding + fraction;
			}
			return result;
		}
	}
}

