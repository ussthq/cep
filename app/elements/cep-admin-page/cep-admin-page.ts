/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-page')
class CepAdminPage extends polymer.Base {
	/**
	 * The route object from app-location
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	route:any;
	/**
	 * The sub route. Determines the selected tab and page
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	topTabRoute:any;
	/**
	 * Loading boolean for the spinner
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object,
		observer: '_onUserChange',
		notify: true
	})
	user:any;
	/**
	 * The route for the bottom setupTab
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	bottomSetupTabRoute:any;
	/**
	 * The route for the bottom manageTab
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	bottomManageTabRoute:any;
	/**
	 * The attribute for the selected neon-animated-pages page
	 * @type {String}
	 */
	@property({
		type: String,
		value: 'evalpage'
	})
	selectedPageProperty:string;
	/**
	 * The years which have reviews defined. Drives the version dropdown
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	reviewYears:any;
	/**
	 * The selected review year
	 * @type {Number}
	 */
	@property({
		type: Number
	})
	selectedYear:number;
	/**
	 * The selected top tab
	 * @type {String}
	 */
	@property({
		type: String
	})
	topTab: string;

	attached() { }

	isAdmin(user) {
		return this.user && this.user.roles && this.user.roles.indexOf('[Admin]') > -1;
	}

	_isCurrentPage(pageName, topTabRoute) {
		// console.log(this.is, '_isCurrentPage', arguments);
		var returnVal = false;
		var topRoute = topTabRoute ? topTabRoute : null;
		if (topRoute && topRoute.indexOf(pageName) > -1) {
			returnVal = true;
		}
		return returnVal;
	}
	/**
	 * Observer for the route prefix. Selects the proper tab if just the /admin route is used
	 * @param {String} topTabRoutePrefix' The prefix from app-route
	 */
	@observe('topTabRoute.prefix,settings')
	private _onTopTabRoutePrefix(topTabRoutePrefix, settings) {
		if (topTabRoutePrefix && topTabRoutePrefix.indexOf('admin') > -1) {
			let entryAnim = this.$.adminPages.entryAnimation;
			let exitAnim = this.$.adminPages.exitAnimation;
			if (!this.topTabRoute.path) {
				this.set('topTabRoute.path', '/manage');
				this.$.adminPages.entryAnimation = null;
				this.$.adminPages.exitAnimation = null;
				this.set('topTab', 'manage');
				this.$.adminPages.entryAnimation = entryAnim;
				this.$.adminPages.exitAnimation = exitAnim;
			}else if (this.topTabRoute.path.indexOf('setup') > -1) {
				this.set('topTab', 'setup');
			}else if (this.topTabRoute.path.indexOf('manage') > -1) {
				this.$.adminPages.entryAnimation = null;
				this.$.adminPages.exitAnimation = null;
				this.set('topTab', 'manage');
				this.$.adminPages.entryAnimation = entryAnim;
				this.$.adminPages.exitAnimation = exitAnim;
			}else if (this.topTabRoute.path.indexOf('car') > -1) {
				this.set('topTab', 'car');
			}
			this.fire('now-app-update-header', settings.DEFAULT_HEADER + ': Admin Site');
			this._fetchReviewYears();
		}
	}
	/**
	 * Fired when the top tab changes
	 * @param {String} topTab The tab to navigate to
	 */
	@observe('topTab, user')
	private _onTopTabChange(topTab, user) {
		// console.log(this.is, '_onTopTabChange', arguments);
		if (!this.route || (this.route && this.route.path.indexOf(topTab) === -1)) {
			this.set('topTabRoute.path', '/' + topTab);
			// console.log(this.is, '_onTopTabChange, tabroute=', this.topTabRoute);
		}
		var authorized = this.isUserAuthorized(topTab, user);
		if (!authorized) {
			this.$.notAuthDialog.open();
		}
	}
	/**
	 * Fired when the selected year changes.
	 * @param {String} newVal the selected year
	 */
	@observe('selectedYear')
	private _onSelectedYearChanged(newVal) {
		//console.log(this.is, '_onSelectedYearChanged', arguments);
		if (newVal && typeof newVal === 'string') {
			this.fire('admin-year-changed', newVal);
		}
	}
	/**
	 * Fired when the user changes
	 * @param {Object} newVal The new user
	 * @param {Object} oldVal The old user
	 */
	private _onUserChange(newVal, oldVal) {
		// console.log(this.is, '_onUserChange', arguments);
	}
	/**
	 * Fetch the review years
	 */
	private _fetchReviewYears() {
		if (!this.reviewYears || this.reviewYears.length === 0) {
			var ajax = this.$.reviewYearsAjax;
			ajax.params = {
				method: 'getYearList'
			};
			ajax.generateRequest();
		}
	}
	/**
	 * Fired when a response is received from fetching the review years
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onReviewYearsResponse(evt, detail) {
		var response = detail.response;
		if (response) {
			this.reviewYears = response.reverse();
			var yr = this._getReviewYear();
			this.$.reviewYearCombo.value = yr;
		}
	}
	/**
	 * Get the current review year based on the month and current year
	 */
	private _getReviewYear() {
		var dte = new Date();
		var yr = dte.getFullYear();
		if (!this.selectedYear) {
			var month = dte.getMonth();
			if (month >= 8) {
				yr = yr + 1;
			}
		}else {
			yr = this.selectedYear;
		}
		return yr;
	}
	/**
	 * Fired when the 'Create New from Current' link is clicked
	 * @param {Event} evt The event object
	 */
	private _onNewFromCurrent(evt) {
		this.$.newVersionDialog.open();
	}
	/**
	 * Fired when the 'OK' button is clicked in the 'Create New from Current' new year dialog.
	 * Fires the ajax request to create the new year's review setup
	 * @param {Event} evt The event object
	 */
	private _onAcceptNewYear(evt) {
		// console.log(this.is, '_onAcceptNewYear', arguments);
		var fromYr = this.selectedYear;
		var newYr = this.get('newEvalYear');
		var ajax = this.$.createNewEvalAjax;
		ajax.params = {
			method: 'createNewEvalFromCurrent',
			oldYear: fromYr,
			newYear: newYr
		};
		ajax.generateRequest();
	}
	/**
	 * Fired once a response is received from the server after creating a new review setup
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onNewEvalResponse(evt, detail) {
		location.reload();
	}
	/**
	 * True if the user is authorized to access the topTab
	 * @param {String} topTab The selected top tab
	 */
	private isUserAuthorized(topTab, user) {
		// console.log(this.is, 'isUserAuthorized', arguments);
		// console.log(this.is, 'isUserAuthorized, user=', this.user);
		var returnVal = false;
		if (this.user) {
			var roles = this.user.roles;
			if (roles && roles.indexOf('[Admin]') > -1) {
				returnVal = true;
			}else if (roles) {
				if (topTab === 'setup') {
					if (roles.indexOf('[Admin]') > -1) {
						returnVal = true;
					}
				}else if (topTab === 'manage' || topTab === 'car') {
					if (roles.indexOf('[DHQ]') > -1 || roles.indexOf('[THQ]') > -1) {
						returnVal = true;
					}
				}
			}
		}
		return returnVal;
	}
	/**
	 * Fired when the confirmation button on the notAuthDialog is clicked. Will redirect
	 * to the manage page if the user has the DHQ or THQ role, else it'll redirect to the home
	 * page
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onNotAuthConfirm(evt, detail) {
		if (this.user) {
			var roles = this.user.roles;
			if (roles.indexOf('[DHQ]') > -1 || roles.indexOf('[THQ]') > -1) {
				this.set('route.path', '/admin/manage/view');
			}else {
				this.set('route.path', '/eval/introandlinks');
			}
		}
	}
	/**
	 * Force a sign out
	 */
	private _signOut() {
		location.href = '/names.nsf?logout&redirectto=' + location.href;
	}

	patchOverlay(evt, detail) {
		console.log(this.is, 'patchOverlay', arguments);
		if (evt.target.withBackdrop) {
			evt.target.parentNode.insertBefore(evt.target.backdropElement, evt.target);
		}
	}
}

CepAdminPage.register();
