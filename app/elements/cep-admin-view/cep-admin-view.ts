/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-view')
class CepAdminView extends polymer.Base {
	/**
	 * The selected bottom tab
	 * @type {String}
	 */
	@property({
		type: String
	})
	bottomTab:string;
	/**
	 * The reviews either for the current user's divid (DHQ Role) or all the reviews (Admin Role)
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	reviews:any;
	/**
	 * The selected review year
	 * @type {String}
	 */
	@property({
		type: String
	})
	reviewYear:string;
	/**
	 * A place holder for the review year
	 * @type {String}
	 */
	@property({
		type: String
	})
	_reviewYear:string;
	/**
	 * True if the spinner should be shown
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;
	/**
	 * The different review states. This is for the state filter dropdown
	 * @type {Array}
	 */
	@property({
		type: Array,
		value: ['Initialized', 'Confirmed', 'Scored', 'Opened', 'Submitted', 'Reviewed', 'Completed']
	})
	reviewStates:any;
	/**
	 * The current user
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	user:any;

	/**
	 * Setup the grid
	 */
	attached() {
		var grid = this.$.reviewsTable;
		grid.columns = [
			{name: 'revid', title: 'ID', flex: 1},
			{name: 'corpname', title: 'Corps Name', flex: 2},
			{name: 'locationid', title: 'Location ID', flex: 1},
			{name: 'lasteditedby', title: 'Last Edited By', flex: 2},
			{name: 'state', title: 'Status', flex: 1},
			{name: 'locked', title: 'Unlock', renderer: this._unlockColumnRenderer.bind(this), flex: 1},
			{name: 'locked', title: 'View', renderer: this._viewColumnRenderer.bind(this), flex: 1},
			{name: 'locked', title: 'Delete', renderer: this._deleteColumnRenderer.bind(this), flex: 1}
		]
		this._addGridHeaders(grid);
	}
	/**
	 * Fired when a state is selected from the combo box. Filters the grid by the selected state
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onGridFiltered(evt, detail) {
		var grid = this.$.reviewsTable;
		var val = detail.value;
		if (val) {
			val = val.toUpperCase();
			grid.items = this.reviews.filter(function(review) {
				return review.state === val;
			});
		}else {
			grid.items = this.reviews;
		}
	}
	/**
	 * Fired when a search is performed. Filters the grid by the search on each keyup
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onSearchGrid(evt, detail) {
		var grid = this.$.reviewsTable;
		var val = this.$.searchField.value;
		if (val) {
			val = val.toLowerCase();
			grid.items = this.reviews.filter(function(review) {
				if (review.corpname) {
					var corp = review.corpname.toLowerCase();
					return corp.indexOf(val) > -1;
				}
			});
		}else {
			grid.items = this.reviews;
		}
	}
	/**
	 * Add the headers to setup the column titles
	 * @param {Object} grid The vaadin-grid
	 */
	private _addGridHeaders(grid) {
		if (grid && grid.columns && grid.columns.length > 0) {
			for (var i = 0; i < grid.columns.length; i++) {
				var col = grid.columns[i];
				var cell = grid.header.getCell(0, i);
				cell.content = col.title;
			}
		}
	}
	/**
	 * observe the active tab and reviewyear to facilitate fetching the proper reviews
	 * @param {String} bottomTab The tab that should be selected
	 * @param {String} reviewYear the selected year
	 */
	@observe('bottomTab, reviewYear')
	private _onActiveTabChange(bottomTab, reviewYear) {
		if (bottomTab === 'view' && reviewYear !== this._reviewYear) {
			this._reviewYear = reviewYear;
			if (this.reviewYear && !this.mainLoading) {
				this.fetchReviews();
			}
		}
	}
	/**
	 * Fired when the reviews change
	 * @param {Array} 'reviews' The array of reviews
	 */
	@observe('reviews')
	private _onReviewsChange(reviews) {
		var grid = this.$.reviewsTable;
		if (!reviews || reviews.length === 0) {
			reviews = [{
				divid: this.user.divid,
				corpname: 'No Reviews Found!',
				revid: 0,
				lasteditedby: null,
				locationid: this.user.locationid,
				state: null,
				locked: false
			}]
		}
		grid.items = reviews;
		grid.refreshItems();
	}
	/**
	 * Fetch the reviews
	 */
	fetchReviews() {
		var ajax = this.$.reviewsAjax;
		ajax.params = {
			method: 'getReviews',
			year: this.reviewYear
		};
		ajax.generateRequest();
	}
	/**
	 * Fired once a response is received from fetching the reviews. If the user has only the
	 * DHQ role, only show the reviews whose divid matches the user's divid
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onReviewsFetchResponse(evt, detail) {
		var response = detail.response;
		var items = response;
		if (this.user) {
			var roles = this.user.roles;
			if (roles.indexOf('[Admin]') === -1 && roles.indexOf('[DHQ]') > -1) {
				items = response.filter(function(review) {
					return review.divid === this.user.divid;
				}.bind(this));
			}
		}
		this.reviews = items;
	}
	/**
	 * Cell Renderer for the lock/unlock column cell
	 * @param {Object} cell The vaadin-grid cell
	 */
	private _unlockColumnRenderer(cell) {
		// console.log(this.is, '_unlockColumnRenderer');
		var val = cell.data;
		var rowData = cell.row.data;
		var icon = val ? 'lock-outline' : 'lock-open';
		var className = val ? 'lockedReviewButton cep-admin-view' : 'unlockedReviewButton cep-admin-view';
		var methodName = val ? '_unlockReview' : '_lockReview';
		var button = null;
		if (!cell.element.childNodes || cell.element.childNodes.length === 0) {
			button = this._createLockUnlockButton(icon, rowData.revid, methodName, className, rowData);
			cell.element.appendChild(button);
		}else {
			button = cell.element.querySelector('paper-icon-button');
			if (button) {
				cell.element.removeChild(button);
			}
			button = this._createLockUnlockButton(icon, rowData.revid, methodName, className, rowData);
			cell.element.appendChild(button);
		}
	}
	/**
	 * Create the unlock button to add to a column
	 * @param {String} icon       The icon to use
	 * @param {Number} revid      The review ID
	 * @param {String} methodName The method name to bind to the click listener
	 * @param {String} className  The style class name
	 * @param {Object} rowData    The review object being processed
	 * @return {HTMLElement}
	 */
	private _createLockUnlockButton(icon, revid, methodName, className, rowData) {
		var button = null;
		button = document.createElement('paper-icon-button');
		button.setAttribute('icon', icon);
		button.setAttribute('revid', rowData.revid);
		button.className = className;
		button.setAttribute('revid', rowData.revid);
		if (rowData.revid === 0) {
			button.setAttribute('disabled', true);
		}
		button.addEventListener('click', this[methodName].bind(this));
		return button;
	}
	/**
	 * Fired when the unlock button is clicked
	 * @param {Event} evt The event object
	 */
	private _unlockReview(evt) {
		// console.log(this.is, '_unlockReview', arguments);
		var reviewId = null;
		if (evt.target.localName === 'iron-icon') {
			var paperButton = evt.target.parentNode;
			reviewId = paperButton.getAttribute('revid');
		}
		if (reviewId) {
			this._lockUnlockReview(reviewId, false)
		}
	}
	/**
	 * Fired when the lock button is clicked
	 * @param {Event} evt The event object
	 */
	private _lockReview(evt) {
		// console.log(this.is, '_lockReview', arguments);
		var reviewId = null;
		if (evt.target.localName === 'iron-icon') {
			var paperButton = evt.target.parentNode;
			reviewId = paperButton.getAttribute('revid');
		}
		if (reviewId) {
			this._lockUnlockReview(reviewId, true)
		}
	}
	/**
	 * Toggles the locked status
	 * @param {Number} revid The review id to toggle the locked status on
	 * @param {Boolean} lock  What to set the locked property to
	 */
	private _lockUnlockReview(revid, lock) {
		var review = this.getReview(revid);
		delete review['$H'];
		if (review) {
			review.locked = lock;
			if (!lock) {
				review.state = 'OPENED';
			}
			var ajax = this.$.lockUnlockAjax;
			ajax.params = {
				id: revid,
				javatype: 'org.salvationarmy.cep.model.Review'
			};
			ajax.body = review;
			ajax.generateRequest();
		}
	}
	/**
	 * Fired when a response is received from locking/unlocking a review. This will
	 * then update the grid with the updated item
	 * @param {Event} evt    The event object
	 * @param {Object} detail The detail object
	 */
	private _onLockUnlockResponse(evt, detail) {
		// console.log(this.is, '_onLockUnlock', arguments);
		var response = detail.response;
		var grid = this.$.reviewsTable;
		var changedItemIndex = this.getReviewIndex(response.revid);
		grid.items.splice(changedItemIndex, 1);
		grid.items.splice(changedItemIndex, 0, response);
		grid.then(function() {
			grid.refreshItems();
		}.bind(this));
	}
	/**
	 * The renderer for the "view button" column
	 * @param {Object} cell The vaadin-grid cell object
	 */
	private _viewColumnRenderer(cell) {
		var row = cell.row;
		var revid = row.data.revid;
		var viewColumnHtml = null;
		if (revid === 0) {
			viewColumnHtml = '<a href="#/eval/introandlinks/' + revid + '" target="_blank" class="cep-admin-view viewReviewIcon disabled">';
			viewColumnHtml += '<paper-icon-button icon="visibility" disabled></paper-icon-button></a>';
		}else {
			viewColumnHtml = '<a href="#/eval/introandlinks/' + revid + '" target="_blank" class="cep-admin-view viewReviewIcon">';
			viewColumnHtml += '<paper-icon-button icon="visibility"></paper-icon-button></a>';
		}
		cell.element.innerHTML = viewColumnHtml;
	}
	/**
	 * The renderer for the "delete button" column
	 * @param {Object} cell The vaadin-grid cell object
	 */
	private _deleteColumnRenderer(cell) {
		var rowData = cell.row.data;
		var button = null;
		if (!cell.element.childNodes || cell.element.childNodes.length === 0) {
			button = this._createDeleteButton(rowData.revid, rowData);
			cell.element.appendChild(button);
		}else {
			button = cell.element.querySelector('paper-icon-button');
			if (button) {
				cell.element.removeChild(button);
			}
			button = this._createDeleteButton(rowData.revid, rowData);
			cell.element.appendChild(button);
		}
	}
	private _createDeleteButton(revid, rowData) {
		var button = null;
		button = document.createElement('paper-icon-button');
		button.setAttribute('icon', 'delete');
		button.setAttribute('revid', rowData.revid);
		if (rowData.revid === 0) {
			button.setAttribute('disabled', true);
		}
		button.addEventListener('click', this._deleteReview.bind(this));
		return button;
	}
	/**
	 * Fired when the delete button is clicked. Will throw a confirmation dialog
	 * @param {Event} evt The event object
	 */
	private _deleteReview(evt) {
		var reviewId = null;
		if (evt.target.localName === 'iron-icon') {
			var paperButton = evt.target.parentNode;
			reviewId = paperButton.getAttribute('revid');
		}
		var confirmDialog = this.$.confirmDelete;
		confirmDialog.set('dialogTitle', 'Really Delete Review?');
		var dialogText = 'You are about to delete a review with ID ';
		dialogText += reviewId + '. ';
		dialogText += 'This will delete all scores, answers and planning data for this review.'
		dialogText += 'This action CAN NOT BE UNDONE! Are you sure you want to continue?';
		confirmDialog.set('dialogText', dialogText);
		confirmDialog.set('cancelButtonColor', 'white');
		var confirmCallback = function() {
			var ajax = this.$.deleteReviewAjax;
			ajax.params = {
				id: reviewId,
				javatype: 'org.salvationarmy.cep.model.Review'
			};
			ajax.generateRequest().completes
				.then((ironRequest) => {
					this.fetchReviews();
				});
		}.bind(this);
		confirmDialog.open(confirmCallback, null);
	}
	/**
	 * Get a review by it's ID
	 * @param {Number} reviewId The review id to find
	 */
	getReview(reviewId) {
		// console.log(this.is, 'getReview', arguments);
		var returnVal = null;
		if (this.reviews) {
			for (let i = 0; i < this.reviews.length; i++) {
				var review = this.reviews[i];
				if (review.revid == reviewId) {
					returnVal = review;
					break;
				}
			}
		}
		// console.log(this.is, 'getReview, returning=', returnVal);
		return returnVal;
	}
	/**
	 * Get the grid index of a review by it's id
	 * @param {Number} reviewId The review id to find
	 */
	getReviewIndex(reviewId) {
		// console.log(this.is, 'getReviewIndex', arguments);
		var returnVal = null;
		if (this.reviews) {
			for (let i = 0; i < this.reviews.length; i++) {
				var review = this.reviews[i];
				if (review.revid == reviewId) {
					returnVal = i;
					break;
				}
			}
		}
		// console.log(this.is, 'getReviewIndex, returning=', returnVal);
		return returnVal;
	}

}

CepAdminView.register();
