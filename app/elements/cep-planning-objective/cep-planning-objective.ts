/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-planning-objective')
class CepPlanningObjective extends polymer.Base {

	@property({
		type: Object,
		notify: true
	})
	evaluation:any;

	@property({
		type: Object,
		notify: true
	})
	objective:any;

	@property({
		type: Object,
		notify: true
	})
	goal:any;

	@property({
		type: Object
	})
	essentialMinistry:any;

	@property({
		type: Boolean,
		notify: true
	})
	locked:boolean;

	/**
	 * Get the objective title
	 * @param {Object} goal The goal this objective resides in
	 * @param {Object} obj  The objective
	 */
	getObjLabelTitle(goal, obj, emscid) {
		var goalVal = goal.idx + 1;
		var objVal = obj.idx + 1;
		var title = 'Objective ' + goalVal + '.' + objVal;
		return title;
	}
	/**
	 * Find the objective title to display
	 * @param {Object} objective         The objective
	 * @param {Object} essentialMinistry The essential ministry
	 * @param {Number} emscid            The emscid of the sub category
	 */
	private _findEmscSubcatTitle(objective, essentialMinistry, emscid) {
		var title = null;
		if (essentialMinistry) {
			var subCats = essentialMinistry.subCategories;
			var emscId = objective.emscid;
			for (let i = 0; i < subCats.length; i++) {
				var subCat = subCats[i];
				if (subCat.emscid === emscId) {
					title = subCat.subcategory;
					break;
				}
			}
		}
		return title;
	}

	/**
	 * Fired when a value in this action changes and triggers
	 * the server update
	 */
	private _onObjectiveChange() {
		//console.log(this.is, '_onObjectiveChange', arguments);
		this.fire('em-item-updated', this.objective);
	}

}

CepPlanningObjective.register();
