/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-print-planning')
class CepPrintPlanning extends polymer.Base {
	@property({
		type: Object,
		observer: '_onEvaluationChange'
	})
	evaluation:any;

	@property({
		type: Boolean,
		notify: true
	})
	printLoading:boolean;

	@property({
		type: Object,
		notify: true
	})
	user:any;

	private _isAdmin(user) {
		if (user.roles && user.roles.indexOf('[DHQ]') > -1) {
			return true;
		}
	}

	private _onEvaluationChange(newVal, oldVal) {
		if (newVal) {
			var ajax = this.$.planningIntroAjax;
			ajax.params = {
				id: newVal.review.year,
				javatype: 'org.salvationarmy.cep.model.PlanningIntro'
			};
			ajax.generateRequest();
		}
	}

	private _getGoalEMName(evaluation, goalIdx) {
		var returnVal = '';
		var review = evaluation.review;
		var goals = review.goals;
		var goal = goals[goalIdx];
		returnVal = this._getEMName(evaluation, goal.emid);
		return returnVal;
	}

	private _getObjectiveEmscName(evaluation, goalIdx, objectiveIdx) {
		var returnVal = '';
		var review = evaluation.review;
		var goals = review.goals;
		var goal = goals[goalIdx];
		if (goal) {
			var objectives = goal.objectives;
			var obj = objectives[objectiveIdx];
			if (obj) {
				returnVal = this._getEmscName(evaluation, obj.emscid);
			}
		}
		return returnVal;
	}

	private _getGoalNotes(evaluation, goalIdx) {
		var returnVal = '';
		var review = evaluation.review;
		var goals = review.goals;
		var goal = goals[goalIdx];
		if (goal) {
			returnVal = goal.goalname;
		}
		return returnVal;
	}

	private _getEMName(evaluation, emid) {
		var returnVal = null;
		var review = evaluation.review;
		var ems = review.essentialMinistries;
		for (let i = 0; i < ems.length; i++) {
			var em = ems[i];
			if (em.emid === emid) {
				returnVal = em.name;
				break;
			}
		}
		return returnVal;
	}

	private _getEmscName(evaluation, emscid) {
		var returnVal = null;
		var review = evaluation.review;
		var ems = review.essentialMinistries;
		for (let i = 0; i < ems.length; i++) {
			var em = ems[i];
			var subCats = em.subCategories;
			for (let j = 0; j < subCats.length; j++) {
				var subCat = subCats[j];
				if (subCat.emscid === emscid) {
					returnVal = subCat.subcategory
					break;
				}
			}
			if (returnVal) {
				break;
			}
		}
		return returnVal;
	}

	private _isActionVisible(action) {
		var returnVal = false;
		if (action.step || action.personresponsible || action.duedate) {
			returnVal = true;
		}
		return returnVal;
	}

}

CepPrintPlanning.register();
