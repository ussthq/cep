/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../../bower_components/DefinitelyTyped/moment/moment.d.ts"/>
/// <reference path="../now-validation-behavior/now-validation-behavior.ts" />

@component('cep-eval-verify')
@behavior(NowBehaviors.ValidationBehavior)
class CepEvalVerify extends polymer.Base implements NowBehaviors.ValidationBehavior {
	/**
	 * The evaluation
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	evaluation:any;
	/**
	 * The main loading boolean. Drives the spinner
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;
	/**
	 * Essential Ministry validation items
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	emValidationItems:any;
	/**
	 * Goal validation items
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	goalValidationItems:any;
	/**
	 * Objective validation items
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	objValidationItems:any;
	/**
	 * Action validation items
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	actValidationItems:any;
	/**
	 * The current user
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	user: any;

	@property({
		type: Object
	})
	settings:any;
	/**
	 * The review from the evaluation object
	 * @type {Object}
	 */
	@computed()
	review(evaluation) {
		return evaluation ? evaluation.review : null;
	}
	/**
	 * Check validity of review
	 * @type {Boolean}
	 */
	@computed()
	isValid(emValidationItems, goalValidationItems, objValidationItems, actValidationItems) {
		return this.isReviewValid(this.evaluation);
	}
	/**
	 * Listen for em-item-updated event and check validation when it runs
	 */
	attached() {
		document.addEventListener('em-item-updated', this._onReviewChange.bind(this));
	}
	@observe('mainLoading')
	private _onMainLoadingChange(mainLoading) {
		if (this.$.signSubmitAjax.loading && !mainLoading) {
			this.set('mainLoading', this.$.signSubmitAjax.loading);
		}
	}

	@observe('signSubmitLoading')
	private _onSignSubmitLoadingChange(signSubmitLoading) {
		this.set('mainLoading', signSubmitLoading);
	}
	/**
	 * Review change observer fired when any property of the evaluation changes
	 * @param {Object} review the item that changed
	 */
	@observe('evaluation.*')
	private _onReviewChange(review) {
		// 	console.log(this.is, '_onReviewChange', arguments);
		// Need to wait for the DOM to be built before doing this
		this.async(function() {
			if (this.review) {
				var allValidationItems = this.getAllValidationItems(this.evaluation.review);
				this.emValidationItems = allValidationItems.ems;
				this.goalValidationItems = allValidationItems.goals;
				this.objValidationItems = allValidationItems.objectives;
				this.actValidationItems = allValidationItems.actions;
			}
		}.bind(this), 500);
	}
	/**
	 * Get the proper icon name
	 * @param {Object} validationItem The validation item
	 * @return {String}
	 */
	getEmIcon(validationItem) {
		var icon = 'check';
		if (validationItem.invalidItems.length > 0) {
			icon = 'close';
		}
		return icon;
	}
	/**
	 * Get the 'valid' attribute value
	 * @param {Object} validationItem The validation item
	 * @return {String}
	 */
	getEmIconClass(validationItem) {
		// console.log(this.is, 'getEmIconClass', arguments);
		var className = 'valid';
		if (validationItem.invalidItems.length > 0) {
			className = 'invalid';
		}
		// console.log(this.is, 'getEmIconClass, returning', className);
		return className;
	}
	/**
	 * True if the essential ministry item is valid
	 * @param {Object} validationItem The validation object
	 * @return {Boolean}
	 */
	isEssMinValid(validationItem) {
		// console.log(this.is, 'isValid', arguments);
		var valid = true;
		if (validationItem.invalidItems.length > 0) {
			valid = false;
		}
		// console.log(this.is, 'isValid, returning', valid);
		return valid;
	}
	/**
	 * Get the text for the submit button
	 * @param {Object} review The review
	 * @return {String}
	 */
	private _getSubmitButtonText(review) {
		var yr = review ? review.year : '';
		return 'Submit ' + yr + ' Evaluation and Goals for Divisional';
	}
	/**
	 * True if the review is locked or the review does not pass validation
	 * @param {Object} review The review
	 * @return {Boolean}
	 */
	private _isSubmitDisabled(review) {
		var returnVal = false;
		if (review.locked || !this.isValid) {
			returnVal = true;
		}
		return returnVal;
	}
	/**
	 * True if the DHQ Officer has completed their review and the
	 * review.state is COMPLETED
	 * @param {Object} review The review
	 * @return {Boolean}
	 */
	private _isSignDisabled(review) {
		var returnVal = false;
		if (review && review.state === 'COMPLETED') {
			returnVal = true;
		}else if (review && review.state !== 'SUBMITTED') {
			returnVal = true;
		}
		return returnVal;
	}
	/**
	 * True if the user has the DHQ or Admin roles
	 * @param {Object} user The current user
	 * @return {Boolean}
	 */
	private _isDhqAdmin(user) {
		var returnVal = false;
		if (user && user.roles && user.roles.length > 0) {
			if (user.roles.indexOf('[Admin]') > -1 || user.roles.indexOf('[DHQ]') > -1) {
				returnVal = true;
			}
		}
		return returnVal;
	}
	/**
	 * Fired when the attendees field is changed
	 * @param {Event} evt The event object
	 * @fires 'em-item-updated'
	 */
	private _onVerifyTextareaChange(evt) {
		this.fire('em-item-updated', this.review);
	}
	/**
	 * Fired when the submit button is clicked
	 * @param {Event} evt The event object
	 */
	private _onSubmit(evt) {
		this.set('review.locked', true);
		this.set('review.submitted', true);
		this.set('review.submittedby', this.user.abbreviatedName);
		this.set('review.submitteddate', moment().format('MMM DD, YYYY'));
		this.set('review.state', 'SUBMITTED');
		this.fire('em-item-updated', this.review);
		var ajax = this.$.signSubmitAjax;
		ajax.params = {
			reviewid: this.get('review.revid'),
			sendSubmittedEmail: 'true',
			division: this.user.division
		};
		ajax.generateRequest();
	}
	/**
	 * Fired when the sign button is clicked
	 * @param {Event} evt The event object
	 */
	private _onSign(evt) {
		this.set('review.state', 'COMPLETED');
		this.set('review.signedby', this.user.abbreviatedName);
		this.set('review.signed', true);
		this.set('review.signeddate', moment().format('MMM DD, YYYY'));
		this.fire('em-item-updated', this.review);
		var ajax = this.$.signSubmitAjax;
		ajax.params = {
			reviewid: this.get('review.revid'),
			sendSignedEmail: 'true',
			unitportalid: this.settings['UNIT_PORTAL_AUTHTOKEN'],
			unitportalurl: this.settings['UNIT_PORTAL_BASE_URL']
		};
		ajax.generateRequest();
	}

	private _signSubmitDone(evt) {
		location.reload();
	}
	/**
	 * Stand-in method for ValidationBehavior
	 */
	_getAllEmPages: () => any;
	getInvalidEms: () => any;
	getAllEmValidationItems: () => any;
	isAllEmsValid: () => boolean;
	getAllValidationItems: (review:any) => any;
	isEmValid: (essentialMinistry:any, locked:any) => any;
	isReviewValid: (evaluation:any) => boolean;
	_getInvalidCount: (validationItemsArr:any) => number;

}

CepEvalVerify.register();
