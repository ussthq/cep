/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../now-number-util-behavior/now-number-util-behavior.ts" />

@component('cep-admin-car')
@behavior(NowBehaviors.NumberUtilBehavior)
class CepAdminCar extends polymer.Base implements NowBehaviors.NumberUtilBehavior {
	/**
	 * Loading boolean for the spinner
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading: boolean;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	user: any;
	/**
	 * The selected review year
	 * @type {String}
	 */
	@property({
		type: String,
		observer: '_onReviewYear'
	})
	reviewYear: string;
	/**
	 * The array of year objects for the user's locationid
	 */
	@property({
		type: Array,
		value: []
	})
	selectedYears: any[];
	/**
	 * The array of year objects for the user's locationid
	 */
	@property({
		type: Array
	})
	yearsResponse:any;
	/**
	 * The settings object
	 * @type {Object}
	 */
	@property({
		type: Object
	})
	settings: any;
	/**
	 * The array of essential ministries for the dropdown box
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	essentialMinistries: any;
	/**
	 * The selected essential ministry id
	 * @type {Number}
	 */
	@property({
		type: Number,
		observer: '_onEmSelected'
	})
	selectedEm: number;
	/**
	 * The goals data array delivered from the server
	 * @type {Array}
	 */
	@property({
		type: Array,
		observer: '_onGoalResponse'
	})
	goalResponse: any;
	/**
	 * The objective data array delivered from the server
	 * @type {Array}
	 */
	@property({
		type: Array,
		observer: '_onObjResponse'
	})
	objResponse: any;
	/**
	 * The response from fetching the emChart data
	 *
	 * @type {any[]}
	 */
	@property({
		type: Array
	})
	emChartResponse: any;
	/**
	 * The data for Goal 1 for the google-chart
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	goal1Data: any;
	/**
	 * The data for Goal 2 for the google-chart
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	goal2Data: any;
	/**
	 * The data for Objective 1.1 for the google-chart
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj11Data: any;
	/**
	 * The data for Objective 1.2 for the google-chart
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj12Data: any;
	/**
	 * The data for Objective 2.1 for the google-chart
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj21Data: any;
	/**
	 * The data for Objective 2.2 for the google-chart
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	obj22Data: any;
	/**
	 * The options object for the scoring charts for the essential ministries
	 */
	@property({
		type: Object,
		value: {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: 45, top: 0, width: '80%', height: '85%'},
			legend: 'none'
		}
	})
	emChartOptions: any;
	/**
	 * The options object for the scoring charts for the sub ministries
	 */
	@property({
		type: Object,
		value: {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: 45, top: 0, width: '80%', height: '85%'},
			legend: 'none'
		}
	})
	emscChartOptions: any;
	/**
	 * Response for the emsc scores
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	emscChartResponse: any;
	/**
	 * Temp array for the emsc scores
	 * @type {Array}
	 */
	@property({
		type: Array
	})
	emscChartGroupArr: any;
	/**
	 * The selected divid
	 * @type {number}
	 */
	@property({
		type: Number
	})
	divId: number;
	/**
	 * The essential ministries fetched with the CAR graph data, NOT
	 * with the fetch of essential ministries to populate the drop down
	 * @type {any[]}
	 */
	@property({
		type: Array
	})
	carEssentialMinistries: any[];
	/**
	 * The years selected. Used for display when no goal data is retrieved
	 * @param selectedYears - The years selected in the multi-select
	 * @param reviewYear - The current reviewYear (from top right corner of site)
	 */
	@computed()
	yearsSelection(selectedYears, reviewYear) {
		//console.log(this.is, 'yearsSelection', arguments);
		if (selectedYears && selectedYears.replace) {
			return selectedYears.replace(/,/g, ', ');
		} else if (selectedYears && !selectedYears.replace) {
			return selectedYears;
		} else {
			return reviewYear;
		}
	}
	/**
	 * True to hide all of the charts
	 * @type {Boolean}
	 */
	@computed({type: Boolean})
	hideCharts(goalResponse): boolean {
		let returnVal = true;
		if (goalResponse && goalResponse.length > 0) {
			returnVal = false;
		}
		return returnVal;
	}
	/**
	 * True to hide the objective charts
	 * @type {Boolean}
	 */
	@computed({type: Boolean})
	hideObjectives(objResponse): boolean {
		let returnVal = true;
		if (objResponse && objResponse.length > 0) {
			returnVal = false;
		}
		return returnVal;
	}
	/**
	 * build the options object for the 1st goal bar chart
	 * @type {Object}
	 */
	@computed()
	goal1Options(goalResponse) {
		let opts = {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: '0', top: '0', width: '100%', height: '100%'},
			width: '100%',
			height: '60',
			isStacked: true,
			legend: {position: 'none'},
			hAxis: {baselineColor: 'none', ticks: []},
			vAxis: {baselineColor: 'none', ticks: []},
			series: {
				0: {color: '#cc0000'},
				1: {color: '#a61b1b'}
			},
			annotations: {textStyle: {fontSize: '14'}}
		};
		return opts;
	}
	/**
	 * build the options object for the 2nd goal bar chart
	 * @type {Object}
	 */
	@computed()
	goal2Options(goalResponse) {
		let opts = {
			animation: {
				duration: 400,
				easing: 'in',
				startup: true
			},
			chartArea: {left: '0', top: '0', width: '100%', height: '100%'},
			width: '100%',
			height: '60',
			isStacked: true,
			legend: {position: 'none'},
			hAxis: {baselineColor: 'none', ticks: []},
			vAxis: {baselineColor: 'none', ticks: []},
			series: {
				0: {color: '#2b78e4'},
				1: {color: '#085394'}
			},
			annotations: {textStyle: {fontSize: '14'}}
		};
		return opts;
	}
	/**
	 * The options object for the objectives pie charts
	 * @type {Object}
	 */
	@computed()
	objectiveOptions(objResponse) {
		let opts = {
			chartArea: {left: '15', top: '15', width: '100%', height: '85%'},
			slices: {
				0: {color: '#009e0f'},
				1: {color: '#2b78e4'},
				2: {color: '#dc3912'},
				3: {color: '#9900ff'},
				4: {color: '#ff9900'},
			}
		};
		return opts;
	}
	/**
	 * true if the multiple year notification for the goal charts is shown.
	 * Should be true if multiple years AND a selectedEm are present
	 * @param {any} user
	 * @param {any} selectedYears
	 * @param {any} selectedEm
	 * @returns {boolean}
	 */
	@computed()
	hideMultiYearNotification(user, selectedYears, selectedEm) {
		if (user && selectedYears && selectedYears.length > 0 && selectedEm) {
			return false;
		}
		return true;
	}
	/**
	 * true if the year label above each chart should be visible
	 * @param selectedYears the selected years from the paper-multi-select
	 */
	@computed()
	showChartRowYear(selectedYears) {
		if (selectedYears && selectedYears.length > 0) {
			return true;
		}
		return false;
	}
	attached() {
		// console.log(this.is, 'attached');
		// Resize the charts to fit the space
		window.addEventListener('resize', () => {
			this.redrawCharts();
		});
	}
	/**
	 * Fired when the user changes. If the user has a divid, set the
	 * divId property which in turn will fire off the fetch of CAR
	 * data charts
	 *
	 * @private
	 * @param {any} newUser
	 * @param {any} oldUser
	 */
	@observe('user')
	private _onUserChange(newUser, oldUser) {
		console.log(this.is, '_onUserChange', arguments);
		if (newUser && newUser.hasOwnProperty('divid')) {
			this.divId = newUser.divid;
		}
	}
	/**
	 * Fired when the selected divid changes. Currently only
	 * attached to the #filterByDivision combo box
	 * @private
	 * @param {any} divId
	 */
	@observe('divId')
	private _onDivIdChange(divId, oldDivId) {
		// console.log(this.is, '_onDivIdChange', arguments);
		// Prevent a double fetch
		if ((divId && oldDivId) || (!divId && oldDivId) || (divId && !oldDivId)) {
			this._refreshEmData();
			// Clear selected essential ministry and reset those charts
			this.goalResponse = null;
			this.objResponse = null;
			this.selectedEm = null;
		}
	}
	/**
	 * When the selectedYears change, update the charts
	 * @param selectedYears
	 * @param oldSelectedYears
	 */
	@observe('selectedYears')
	private _onSelectedYearsChange(selectedYears, oldSelectedYears) {
		// console.log(this.is, '_onSelectedYearsChange', selectedYears);
		this.goalResponse = null;
		this.objResponse = null;
		this.selectedEm = null;
		//if (this.selectedYears && this.selectedYears.length > 0) {
			this._refreshEmData();
		//}
	}
	/**
	 * Check if the [THQ] or [Admin] roles are assigned to the user
	 * @private
	 * @param {any} user
	 * @returns {boolean}
	 */
	private _isAdmin(user) {
		if (user && user.roles && (user.roles.indexOf('[THQ]') > -1 || user.roles.indexOf('[Admin]') > -1)) {
			return true;
		}
		return false;
	}
	/**
	 * Refresh the CAR graphics charts based on the selections
	 * @private
	 */
	private _refreshEmData() {
		// console.log(this.is, '_refreshEmData', arguments);
		this.set('emChartResponse', null);
		let year = this.reviewYear;
		if (this._isAdmin(this.user)) {
			let yearList = this.selectedYears;
			if (yearList && yearList.length > 0) {
				year = yearList.sort().join('|');
			}
		}
		let divid = this.getDivId();
		if (divid) {
			let emAjax = this.$.emChartDataAjax;
			emAjax.params = {
				DivId: divid,
				Year: year
			};
			emAjax.headers = {
				Authorization: this.settings.DF_AUTH_HEADER
			};
			emAjax.generateRequest().completes
				.then((ironRequest) => {
					// console.log(this.is, '_refreshEmData, emAjax.request.then, ironRequest=', ironRequest);
					let emChartResponse = ironRequest.response;
					let emscAjax = this.$.emscChartDataAjax;
					emscAjax.params = {
						DivId: divid,
						Year: year,
						EmId: null
					};
					emscAjax.headers = {
						Authorization: this.settings.DF_AUTH_HEADER
					};
					emscAjax.headers = emAjax.headers;
					emscAjax.generateRequest().completes
						.then((ironRequest) => {
							let emscChartResponse = ironRequest.response;
							// Now roll up all the results into one object and set the carEssentialMinistries property
							let carEms = this._onEmChartResponse(emChartResponse);
							carEms = this._onEmscChartResponse(emscChartResponse, carEms);
							this.set('carEssentialMinistries', carEms);
						});
				});
		}
	}
	/**
	 * Build an array for the EM chart data. Will build an array of objects structured like
	 * {
	 *	  name: 'Missional Outreach',
	 * 	  years: [
	 *		{
	 *		  year: 2016,
	 *		  em: {...}, // response object
	 *		  emsc: null
	 * 		},
	 *		{
	 *		  year: 2017,
	 *		  em: {...}, // response object
	 *		  emsc: null
	 * 		}
	 *    ]
	 * }
	 * @private
	 * @param {any} emChartResponse
	 * @return {any[]}
	 */
	private _onEmChartResponse(emChartResponse) {
		// console.log(this.is, '_onEmChartResponse', arguments);
		let carEms = null;
		if (emChartResponse && emChartResponse.length > 0) {
			let ems = [];
			// Build just the essential ministries
			for (let i = 0; i < emChartResponse.length; i++) {
				let em = emChartResponse[i];
				let emObj = {
					name: em.Title,
					years: []
				};
				let foundEms = ems.filter((emData) => {
					return emData.name === emObj.name;
				});
				if (foundEms.length === 0) {
					ems.push(emObj);
				}
			}
			carEms = ems;
			// Now we need to build year Objects
			let selYears = this.selectedYears;
			if (!selYears || selYears.length === 0) {
				selYears = [this.reviewYear];
			}
			// Loop through selected years (or just review year) and add the structure
			for (let i = 0; i < selYears.length; i++) {
				let yr = selYears[i];
				// This should filter out the emData for just the selected year (yr)
				let yrEms = emChartResponse.filter((emResponseData) => {
					return emResponseData.Year == yr;
				});
				// Now loop through the carEMs and add the year data to the appropriate em
				for (let j = 0; j < carEms.length; j++) {
					let carEm = carEms[j];
					let emYear = yrEms.filter((yrEmObj) => {
						return yrEmObj.Title === carEm.name;
					});
					let emYrData = {
						year: yr,
						em: null,
						emsc: null
					};
					if (emYear.length > 0) {
						emYrData.em = emYear[0];
					}
					carEm.years.push(emYrData);
				}
			}
		}
		return carEms;
	}
	/**
	 * Add the emsc part to the carEssentialMinistries array. Will add the emsc property
	 * to carEssentialMinistries[0].years[0]
	 * @private
	 * @param {any} emscChartResponse
	 * @param {any} carEms
	 * @return {any[]}
	 */
	private _onEmscChartResponse(emscChartResponse, carEms) {
		// console.log(this.is, '_onEmscChartResponse', arguments);
		if (carEms && carEms.length > 0) {
			for (let i = 0; i < carEms.length; i++) {
				let carEm = carEms[i];
				let carEmYears = carEm.years;
				for (let j = 0; j < carEmYears.length; j++) {
					let carEmYear = carEmYears[j];
					let carYearEmid = carEmYear.em.emid || carEmYear.em.EMID;
					let emscForEmid = emscChartResponse.filter((emscChartObj) => {
						let emscEmid = emscChartObj.emid || emscChartObj.EMID;
						return emscEmid == carYearEmid;
					});
					carEmYear.emsc = emscForEmid;
				}
			}
		}
		return carEms;
	}
	/**
	 * Get the data object for a single chart of scores by EMSCID
	 * @param {Array} emscChartResponse The response from the server
	 * @param {Object} em The essential ministry that we want the sub ministries for
	 * @return {Array}
	 */
	getEmscChartData(emYear) {
		// console.log(this.is, '_getEmscChartData', arguments);
		let data = [];
		if (emYear) {
			let emscDataArr = emYear.emsc
			// console.log(this.is, '_getEmscChartData, emscDataArr=', emscDataArr);
			data.push([
				{label: 'Label', type: 'string'},
				{label: 'Avg', type: 'number'},
				{role: 'annotation'},
				{role: 'tooltip'},
				{role: 'style'}
			]);
			let colors = [
				'#2b78e4',
				'#009e0f',
				'#dc3912',
				'#9900ff',
				'#ff9900'
			];
			for (let i = 0; i < emscDataArr.length; i++) {
				let dataItem = emscDataArr[i];
				let subCatScoreArr = [];
				subCatScoreArr.push(parseFloat(dataItem.SumGrowthScore));
				subCatScoreArr.push(parseFloat(dataItem.SumMembershipScore));
				subCatScoreArr.push(parseFloat(dataItem.SumParticipationScore));
				subCatScoreArr.push(parseFloat(dataItem.SumStandardScore));
				let scoreAvg = this.getAverage(subCatScoreArr);
				let toolTip = dataItem.Subcategory + '\n\n' +
					'Membership - ' + parseFloat(dataItem.SumMembershipScore).toFixed(2) + '\n' +
					'Meetings - ' + parseFloat(dataItem.SumStandardScore).toFixed(2) + '\n' +
					'Giving - ' + parseFloat(dataItem.SumParticipationScore).toFixed(2) + '\n' +
					'Attendance - ' + parseFloat(dataItem.SumGrowthScore).toFixed(2) + '\n';
				data.push([
					dataItem.Subcategory,
					scoreAvg,
					scoreAvg.toFixed(2).toString(),
					toolTip,
					colors[i]
				]);
			}
		}
		return data;
	}
	/**
	 * Get the EM Score data for the google chart
	 * @param {Array} emChartResponse The response from fetching the score data
	 * @param {Object} em The em object for a graph
	 * @return {Array}
	 */
	getEmChartData(emYear) {
		// console.log(this.is, 'getEmChartData', arguments);
		let data = [];
		if (emYear) {
			let row = emYear.em;
			if (row) {
				data.push(['Membership', parseFloat(row.MemberShip_Score), '#2b78e4', parseFloat(row.MemberShip_Score).toString()]);
				data.push(['Meetings', parseFloat(row.Standard_Score), '#009e0f', parseFloat(row.Standard_Score).toString()]);
				data.push(['Giving', parseFloat(row.Participation_Score), '#dc3912', parseFloat(row.Participation_Score).toString()]);
				data.push(['Attendance', parseFloat(row.Growth_Score), '#9900ff', parseFloat(row.Growth_Score).toString()]);
				data.push(['Ministry', parseFloat(row.Ministry_Score), '#ff9900', parseFloat(row.Ministry_Score).toString()]);
				data.splice(0,0,['Leadership Assessment Category', '', {role: 'style'}, {role: 'annotation'}]);
				this.redrawCharts();
			}
		}else {
			//this.set('emChartResponse', null);
		}
		// console.log(this.is, 'getEmChartData, returning', data);
		return data;
	}
	/**
	 * Get the name of the selected essential ministry
	 * @param {Number} selectedEm The selected essential ministry emid
	 * @return {String}
	 */
	getEmTitle(selectedEm) {
		// console.log(this.is, 'getEmTitle', arguments);
		let ems = this.essentialMinistries;
		let returnVal = 'No Essential Ministry';
		for (let i = 0; i < ems.length; i++) {
			var em = ems[i];
			if (em.emid === selectedEm) {
				returnVal = em.name;
			}
		}
		return returnVal;
	}
	/**
	 * Redraw the charts to fit their containers
	 */
	redrawCharts() {
		let charts = document.querySelectorAll('google-chart');
		[].forEach.call(charts, (el) => {
			el.redraw();
		});
	}
	/**
	 * True if there is no objective data for the selected essential ministry
	 * @param {Array} objResponse The response from fetching objective data
	 * @return {Boolean}
	 */
	isNoObjData(objResponse) {
		// console.log(this.is, 'isNoObjData', arguments);
		let returnVal = true;
		if (objResponse && objResponse.length === 0) {
			returnVal = false;
		}
		// console.log(this.is, 'isNoObjData, returning', returnVal);
		return returnVal;
	}
	/**
	 * Fired when the objective data changes. This will resize the charts to
	 * fit the space
	 * @param {Array} newVal the new objResponse value
	 * @param {Array} oldVal the old objResponse value
	 */
	private _onObjResponse(newVal, oldVal) {
		// console.log(this.is, '_onObjResponse', arguments);
		if (newVal) {
			let objData11 = [
				['Objective', 'Goal']
			];
			let objData12 = [
				['Objective', 'Goal']
			];
			let objData21 = [
				['Objective', 'Goal']
			];
			let objData22 = [
				['Objective', 'Goal']
			];
			for (let i = 0; i < newVal.length; i++) {
				var obj = newVal[i];
				objData11.push(this._parseObjective(obj, '1.1'));
				objData12.push(this._parseObjective(obj, '1.2'));
				objData21.push(this._parseObjective(obj, '2.1'));
				objData22.push(this._parseObjective(obj, '2.2'));
			}
			this.obj11Data = objData11;
			this.obj12Data = objData12;
			this.obj21Data = objData21;
			this.obj22Data = objData22;
		}
		this.redrawCharts();
	}
	/**
	 * Parse the passed in objective to return the array that google-charts
	 * requires to display the pie charts
	 * @param {Object} objective The objective from the objResponse array
	 * @param {String} position The position of the objective
	 * @return {Array}
	 */
	private _parseObjective(objective, position): any {
		let pos = null;
		// TODO: Need to change these to match the stored procedure
		if (position === "1.1") {
			pos = "Total_Objective11";
		}else if (position === "1.2") {
			pos = "Total_Objective12";
		}else if (position === "2.1") {
			pos = "Total_Objective21";
		}else if (position === "2.2") {
			pos = "Total_Objective22";
		}
		let obj = [
			objective.Subcategory,
			parseInt(objective[pos])
		];
		return obj;
	}
	/**
	 * Fired when goalResponse changes. Will build the data arrays for the bar
	 * charts and set the goal1Data and goal2Data properties
	 * @param {Array} newVal The new goal data for the selected essential ministry
	 * @param {Array} oldVal The old goal data for the selected essential ministry
	 */
	private _onGoalResponse(newVal, oldVal): void {
		// console.log(this.is, '_onGoalResponse', arguments);
		if (newVal && newVal.length > 0) {
			let goal1Data = [
				[
					"",
					"Goal",
					{"role": "annotation"},
					"Total",
					{"role": "annotation"}
				],
				[
					"",
					parseInt(newVal[0].Total_1stGoal),
					newVal[0].Total_1stGoal,
					parseInt(newVal[0].Total_State),
					newVal[0].Total_State + ' Total Corps'
				]
			];
			this.goal1Data = goal1Data;
			let goal2Data = [
				[
					"",
					"Goal",
					{"role": "annotation"},
					"Total",
					{"role": "annotation"}
				],
				[
					"",
					parseInt(newVal[0].Total_2ndGoal),
					newVal[0].Total_2ndGoal,
					parseInt(newVal[0].Total_State),
					newVal[0].Total_State + ' Total Corps'
				]
			];
			this.goal2Data = goal2Data;
			this._fetchObjData();
		}
	}
	/**
	 * Fetch the objectives data
	 */
	private _fetchObjData(): void {
		console.log(this.is, '_fetchObjData', arguments);
		let ajax = this.$.carObjAjax;
		//let year = this.$.compareByYear.selectedItems ? [this.$.compareByYear.selectedItems, this.reviewYear].sort().join(',') : this.reviewYear;
		let year = this.reviewYear;
		let divid = this.getDivId();
		ajax.params = {
			DivId: divid,
			Emid: this.selectedEm,
			Year: year
		};
		ajax.headers = {
			Authorization: this.settings.DF_AUTH_HEADER
		};
		ajax.generateRequest();
	}
	/**
	 * Fired when the review year changes. Will fetch the essential ministries and set
	 * the combo box value to null
	 * @param {String} newVal The new reviewYear
	 * @param {String} oldVal The old reviewYear
	 */
	private _onReviewYear(newVal, oldVal) {
		// console.log(this.is, '_onReviewYear', arguments);
		// console.log(this.is, '_onReviewYear, settings=', this.settings);
		if (newVal && newVal != oldVal) {
			this.$.filterByMinistry.value = null;
			this._fetchEms();
		}
	}
	/**
	 * Fired when a response is received from fetching the years. Should
	 * include all the years including current year
	 * @private
	 * @param {any} e
	 */
	private _yearsResponse(e) {
		// console.log(this.is, '_yearsResponse', arguments);
		let response = e.detail.response;
		this.yearsResponse = response;
	}
	/**
	 * Fired when an Essential Ministry is selected and the selectedEm property is set.
	 * will fetch the totals for the selectedEm
	 * @param {Number} newVal The newly selected emid
	 * @param {Number} oldVal the old emid
	 */
	private _onEmSelected(newVal, oldVal) {
		// console.log(this.is, '_onEmSelected', arguments);
		if (newVal > 0) {
			let goalAjax = this.$.carGoalAjax;
			//let year = this.$.compareByYear.selectedItems ? [this.$.compareByYear.selectedItems, this.reviewYear].sort().join(',') : this.reviewYear;
			let year = this.reviewYear;
			let divid = this.getDivId();
			goalAjax.params = {
				DivId: divid,
				Emid: newVal,
				Year: year
			};
			goalAjax.headers = {
				Authorization: this.settings.DF_AUTH_HEADER
			};
			goalAjax.generateRequest();
		}
	}
	/**
	 * Fired when the value in the combo box changes. If a value is selected
	 * will set the selectedEm property
	 * @param {Event} evt the event object
	 * @param {Object} detail the detail object
	 */
	private _onMinistryFiltered(evt, detail) {
		// console.log(this.is, '_onMinistryFiltered', arguments);
		this.goalResponse = null;
		this.objResponse = null;
		if (detail.value) {
			this.selectedEm = detail.value.emid;
		}else {
			this.selectedEm = null;
		}
	}
	/**
	 * Fetch the essential ministries based on the selected reviewYear
	 */
	private _fetchEms() {
		// console.log(this.is, '_fetchEms', arguments);
		let ajax = this.$.essentialMinistryAjax;
		let year = this.reviewYear;
		ajax.params = {
			method: 'getEssentialMinistries',
			year: year
		};
		ajax.generateRequest();

		let yearsAjax = this.$.yearAjax;
		yearsAjax.generateRequest();

		let divisionsAjax = this.$.divisionsAjax;
		divisionsAjax.generateRequest();

	}
	/**
	 * Get the appropriate divid to use in requests for charting data
	 * If a value is selected in the division drop down, use that. Else
	 * use the user.divid value
	 * @private
	 * @returns
	 */
	private getDivId() {
		let divid = this.user ? this.user.divid : null;
		if (this._isAdmin(this.user)) {
			let selectedDiv = this.divId;
			divid = selectedDiv ? selectedDiv : this.user.divid;
		}
		return divid;
	}

	/**
	 * Stand-in methods for NowNumberUtilBehavior
	 */
	getDeviationArr: (numberArr: number[]) => number[];
	toPrettyNumber: (num: number) => string;
	getMedian: (numberArr: number[]) => number;
	getAverage: (numberArr: number[]) => number;
	toFixed: (value: any, precision: any) => string;
}

CepAdminCar.register();
