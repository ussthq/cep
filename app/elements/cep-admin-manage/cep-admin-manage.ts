/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('cep-admin-manage')
class CepAdminManage extends polymer.Base {
	/**
	 * Loading boolean for the spinner
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object,
		observer: '_onUserChange'
	})
	user:any;
	/**
	 * Route for the bottom tab
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	bottomTabRoute:any;

	@property({
		type: String
	})
	reviewYear: string;

	attached() { }

	@observe('bottomTabRoute.prefix')
	private _onBottomTabRouteChange(newVal) {
		// console.log(this.is, '_onBottomTabRouteChange', arguments);
		if (newVal && newVal.indexOf('/admin/manage') > -1 && !this.bottomTabRoute.path) {
			this.set('bottomTabRoute.path', '/view');
		}
	}

	private _onUserChange(newVal, oldVal) {
	}
}

CepAdminManage.register();
