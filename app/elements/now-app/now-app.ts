/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../now-animated-behavior/now-animated-behavior.ts"/>

@component('now-app')
class NowApp extends polymer.Base {
	/**
	 * The current page to show
	 * @type {String}
	 */
	@property({
		type: String,
		reflectToAttribute: true,
		value: 'evalPage'
	})
	page:string;
	/**
	 * True if content in the main content area is currently loading. All elements
	 * going down the tree should get this property and it should be added as the loading
	 * property of any iron-ajax components
	 * @type {Boolean}
	 */
	@property({
		type: Boolean,
		notify: true
	})
	mainLoading:boolean;
	/**
	 * now-animated-pages-behavior property - Defines the animation type
	 * @type {String}
	 */
	@property({
		type: String,
		value: 'scale',
		readOnly: true
	})
	animationType:string;
	/**
	 * now-animated-pages-behavior property - Defines the neon-animated-pages attrForSelected property to determine the current page
	 * @type {String}
	 */
	@property({
		type: String,
		value: 'id',
		readOnly: true
	})
	selectedPageProperty:string;
	/**
	 * now-animated-pages-behavior property - Defines the neon-animated-pages element
	 * @type {String}
	 */
	@computed()
	neonPagesElement() {
		return this.$.appPages;
	}
	/**
	 * The content title
	 * @type {String}
	 */
	@property({
		type: String,
		notify: true
	})
	contentTitle:string;
	/**
	 * The user object
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	user:any;
	/**
	 * The route for app-location
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	route:any;
	/**
	 * The route data which contains the tab and review id
	 * @type {Object}
	 */
	@property({
		type: Object,
		value: {}
	})
	routeData:any;
	/**
	 * True if the admin route is active
	 * @type {Boolean}
	 */
	@property({
		type: Boolean
	})
	adminRouteActive:boolean;
	/**
	 * True if the eval route is active
	 * @type {Boolean}
	 */
	@property({
		type: Boolean
	})
	evalRouteActive:boolean;
	/**
	 * The settings object
	 * @type {Object}
	 */
	@property({
		type: Object,
		notify: true
	})
	settings:any;

	@computed()
	settingsUrl(route) {
		var url = '/thq/cep2.nsf/scripts/settings.json';
		var port = location.port;
		// Setup for running local
		if (port && port !== '80' && port !== '443') {
            url = '../../scripts/settings.json';
        }
		return url;
	}
	/**
	 * Ready callback
	 */
	ready() {
		console.info(this.is, "is ready to rock...");
		document.addEventListener('now-app-update-header', this.updateHeader.bind(this));
	}
	@observe('settings')
	private _onSettings(settings) {
		// console.log(this.is, '_onSettings', arguments);
		if (settings && 'BASE_REST_URL' in settings) {
			var ajax = this.$.userAjax;
			ajax.params = {
				"unitportalurl": settings.UNIT_PORTAL_BASE_URL,
				"unitportalid": settings.UNIT_PORTAL_AUTHTOKEN
			};
			ajax.generateRequest().completes
				.catch((err) => {
					var dialog = this.$.noLocationIdDialog;
					if (ajax && ajax.lastError) {
						console.error(ajax.lastError.response.message);
						dialog.dialogTitle = 'No AD Location SID';
						dialog.dialogText = 'No AD Location SID is associated with your user account which prevents us from finding your review. Please contact THQ IT to correct this issue.';
					} else {
						dialog.dialogTitle = 'Unable to fetch user';
						dialog.dialogText = 'An error occurred attempting to fetch your user account. This prevents us from finding your review. This is most likely because you have no location SID associated with your account. Please contact THQ IT to correct this issue.';
					}
					dialog.open();
				});
			// Setup the keep alive request
			var keepAlive = this.$.keepAliveAjax;
			keepAlive.params = {
				method: 'keepAlive'
			};
			// Generate the request every 10 minutes
			setInterval(function() {
				keepAlive.generateRequest();
			}, 600000);
		}
	}
	@observe('user')
	private _onUserChange(user) {
		if (user && !user.locationid) {
			if (!user.roles || (user.roles.indexOf('[DHQ]') === -1 && user.roles.indexOf('[Admin]') === -1)) {
				var dialog = this.$.noLocationIdDialog;
				dialog.dialogTitle = 'No Location ID';
				dialog.dialogText = 'No Location ID is associated with your user account which prevents us from finding your review. Please contact THQ IT to correct this issue.';
				dialog.open();
			}
		}
	}
	/**
	 * Fired when the active status of the 2 different routes fire
	 * @param {Boolean} adminRouteActive true if the admin route is active
	 * @param {Boolean} evalRouteActive true if the eval route is active
	 */
	@observe('adminRouteActive, evalRouteActive, user')
	private _onRouteActiveChange(adminRouteActive, evalRouteActive, user) {
		// console.log(this.is, '_onRouteActiveChange', arguments);
		// console.log(this.is, '_onRouteActiveChange, route.path=', this.route.path);
		var page = 'eval';
		var dialog = this.$.noLocationIdDialog;
		if (this.route.path === '/eval' && !user.locationid) {
			if (user.roles.indexOf('[DHQ]') > -1 || user.roles.indexOf('[Admin]') > -1) {
				dialog.dialogTitle = 'No Location ID';
				var dialogText = 'No Location ID is associated with your user account which prevents us from finding your review. ';
				dialogText += 'Since you have permission to see other reviews,';
				dialogText += ' please use the Admin site to navigate to a review.';
				dialog.dialogText = dialogText;
				dialog.open();
			}
		}
		if (adminRouteActive || evalRouteActive) {
			page = adminRouteActive ? 'admin' : 'eval';
			if (adminRouteActive && dialog.opened) {
				dialog.close();
			}
		} else if (!this.route.path) {
			this.set('route.path', '/' + page);
		}
		this.set('page', page);
		// console.log(this.is, '_onRouteActiveChange page', page);
	}

	@listen('open-confirm-dialog')
	private _onOpenConfirmDialog(evt, detail) {
		let dialog = this.$.noLocationIdDialog;
		dialog.dialogTitle = detail.dialogTitle;
		dialog.dialogText = detail.dialogText;
		dialog.open();
	}

	@listen('em-item-update-complete')
	private _onItemUpdate(evt, detail) {
		// console.log(this.is, '_onItemUpdate', arguments);
		this.$.saveNotification.show({
			text: 'Changes Saved.',
			duration: 4000
		});
		this.$.saveNotification.resetFit();
	}

	private _openPrint() {
		this.fire('open-print-page');
	}
	/**
	 * Update the header title
	 * @param {String} header The title to set
	 */
	updateHeader(evt) {
		// console.log(this.is, 'updateHeader', arguments);
		var header = evt.detail;
		this.set('contentTitle', header);
	}
}

NowApp.register();
