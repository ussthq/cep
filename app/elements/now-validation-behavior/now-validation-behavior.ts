/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * This behavior is for validation of various items in CEP
 */
namespace NowBehaviors {
	export class ValidationBehavior extends polymer.Base {
		/**
		 * Get all the cep-eval-em components
		 * @return {Array}
		 */
		_getAllEmPages() {
			var cepEvalEms = document.querySelectorAll('cep-eval-em');
			return cepEvalEms || [];
		}
		/**
		 * Determine if all the cep-eval-em components are valid
		 * @return {Boolean}
		 */
		isAllEmsValid() {
			var returnVal = false;
			var cepEvalEms = this._getAllEmPages();
			if (cepEvalEms.length > 0) {
				var validationStats = [];
				for (let i = 0; i < cepEvalEms.length; i++) {
					var cepEvalEm = cepEvalEms[i] as CepEvalEm;
					var emValid = cepEvalEm.validate();
					validationStats.push(emValid);
				}
				if (validationStats.indexOf(false) === -1) {
					returnVal = true;
				}
			}
			return returnVal;
		}
		/**
		 * Get only the invalid cep-eval-em validation items
		 * @return {Array}
		 */
		getInvalidEms() {
			var returnVal = [];
			var cepEvalEms = this._getAllEmPages();
			if (cepEvalEms.length > 0) {
				for (let i = 0; i < cepEvalEms.length; i++) {
					var cepEvalEm = cepEvalEms[i] as CepEvalEm;
					var emValid = cepEvalEm.validate();
					if (!emValid) {
						returnVal.push(cepEvalEm.invalidItems);
					}
				}
				if (returnVal.length > 0) {
					returnVal = returnVal.sort(function(a, b) {
						return a.em.sequence - b.em.sequence;
					});
				}
			}
			return returnVal;
		}
		/**
		 * Get all of the cep-eval-em validation items
		 * @return {Array}
		 */
		getAllEmValidationItems() {
			var evals = this._getAllEmPages();
			var invalidItems = [];
			for (let i = 0; i < evals.length; i++) {
				var cepEvalEm = evals[i] as CepEvalEm;
				var valid = cepEvalEm.validate();
				var insertIdx = invalidItems.length === 0 ? 0 : invalidItems.length - 1;
				invalidItems.splice(insertIdx, 0, cepEvalEm.invalidItems);
			}
			invalidItems = invalidItems.sort(function(a, b) {
				return a.em.sequence - b.em.sequence;
			});
			return invalidItems;
		}
		/**
		 * Get the validation items for the entire review
		 * @param {Object} review The review (evaluation.review)
		 * @return {Object} An object containing arrays of the validation items
		 */
		getAllValidationItems(review) {
			var emValidationItems = this.getAllEmValidationItems();
			var goalValidationItems = [];
			var objValidationItems = [];
			var actValidationItems = [];
			var allEms = this._getAllEmPages();

			var goals = review.goals;
			for (let i = 0; i < goals.length; i++) {
				var goal = goals[i];
				var goalValObj = {
					validTxt: goal.emid ? 'valid' : 'invalid',
					valid: goal.emid ? true : false,
					icon: goal.emid ? 'check' : 'close',
					identifier: 'Goal ' + (goal.idx + 1)
				};
				goalValidationItems.push(goalValObj);
				var objs = goal.objectives;
				for (let j = 0; j < objs.length; j++) {
					var obj = objs[j];
					var objName = 'Objective ' + (goal.idx + 1) + '.' + (obj.idx + 1);
					var objValObj = {
						validTxt: obj.emscid ? 'valid' : 'invalid',
						valid: obj.emscid ? true : false,
						icon: obj.emscid ? 'check' : 'close',
						identifier: objName
					};
					objValidationItems.push(objValObj);
					var acts = obj.actions;
					var validActionCount = 0;
					for (let k = 0; k < acts.length; k++) {
						var act = acts[k];
						if ('step' in act && act.step) {
							validActionCount++;
						}
					}
					var actObj = {
						validTxt: validActionCount > 0 ? 'valid' : 'invalid',
						valid: validActionCount > 0 ? true : false,
						icon: validActionCount > 0 ? 'check' : 'close',
						identifier: objName + ' - Actions'
					};
					actValidationItems.push(actObj);
				}
			}
			return {
				ems: emValidationItems,
				goals: goalValidationItems,
				objectives: objValidationItems,
				actions: actValidationItems
			};
		}
		/**
		 * Determine if an individual cep-eval-em is valid
		 * @param {Object} essentialMinistry The essential ministry object to validate
		 * @param {Boolean} locked            true if the review is locked
		 */
		isEmValid(essentialMinistry, locked) {
			// console.log(this.is, 'isEmValid', arguments);
			var valid = true;
			var invalidItems = [];
			if (!locked) {
				if (essentialMinistry) {
					var iqs = essentialMinistry.impactQuestions;
					var subCats = essentialMinistry.subCategories;
					var validIqs = true;
					var validSubCats = true;
					for (var i = 0; i < iqs.length; i++) {
						if (!iqs[i].answer) {
							validIqs = false;
							valid = false;
							invalidItems.push(iqs[i]);
						}
					}
					for (var j = 0; j < subCats.length; j++) {
						if (!subCats[j].score) {
							validSubCats = false;
							valid = false;
							invalidItems.push(subCats[j]);
						}
					}
				}
			}
			return {
				valid: valid,
				invalidItems: invalidItems
			};
		}
		/**
		 * Check the validity of the entire review
		 * @param {Object} evaluation The evaluation
		 * @return {Boolean}
		 */
		isReviewValid(evaluation) {
			var returnVal = false;
			var invalidItemCount = 0;
			var allValidationItems = this.getAllValidationItems(evaluation.review);
			var types = ['ems', 'goals', 'objectives', 'actions'];
			for (let i = 0; i < types.length; i++) {
				var type = types[i];
				invalidItemCount = invalidItemCount + this._getInvalidCount(allValidationItems[type]);
			}
			if (invalidItemCount === 0) {
				returnVal = true;
			}
			return returnVal;
		}
		/**
		 * Take an array of validation items and find how many are invalid
		 * @param {Array} validationItemsArr Array of validation items
		 * @return {Number}
		 */
		_getInvalidCount(validationItemsArr) {
			var invalidItemCount = 0;
			for (let i = 0; i < validationItemsArr.length; i++) {
				var item = validationItemsArr[i];
				if (!item.valid) {
					invalidItemCount++;
				}
			}
			return invalidItemCount;
		}

	};
}
