/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

// We use UAParser-JS library as a User-Agent string parser (https://github.com/faisalman/ua-parser-js)
declare var UAParser;

@component('now-browser-check')
class NowBrowserCheck extends polymer.Base {
	@property({
		type: Array
	})
	filter: any;

	@property({
		type: Boolean,
		notify: true,
		value: true
	})
	isHidden: boolean;

	attached() {
		let parser = new UAParser();
		let browser = parser.getBrowser();
		//console.log(browser);
		for (let i of this.filter) {
			for (var key in i) {
				if (i.hasOwnProperty(key)) {
					//console.log(key);
					//console.log(parseInt(i[key]));
					if (key.toLowerCase() == browser.name.toLowerCase() && (parseInt(browser.major) < parseInt(i[key]))   ) {
						this.isHidden = false;
						break;
					}
				}
			}
			if (!this.isHidden) {
				break;
			}
		}
	}

}

NowBrowserCheck.register();
